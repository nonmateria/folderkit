#!/bin/sh

directory=${PWD##*/}

if [ "$directory" = "folderkit" ]
then

    echo "folderkit single header libraries update:"

	git clone https://github.com/RandyGaul/cute_headers.git
	git clone https://github.com/mackron/dr_libs.git
	git clone https://github.com/mattiasgustavsson/libs.git rnd
	git clone https://github.com/floooh/sokol.git
    
    echo "moving files..."

    mv cute_headers/cute_files.h src/3rdp/cute_files.h
    mv dr_libs/dr_wav.h src/3rdp/dr_wav.h
    mv rnd/rnd.h src/3rdp/rnd.h 
    mv sokol/sokol_time.h src/3rdp/sokol_time.h

    echo "deleting repos..."
    
    rm -rf cute_headers/
    rm -rf dr_libs/
    rm -rf rnd/
    rm -rf sokol/

    echo "...done"

else
    echo "use this script from the folderkit directory"
fi


exit
