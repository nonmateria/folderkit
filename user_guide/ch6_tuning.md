
# Timing

The `/w` will set a delay to all the successive messages, the delay will be in 1/32th of each orca frame, so 1/32th of 1/16th at the current set tempo. Sequence this value to create shuffle timings, or randomize it with low values to humanize timing.  

Try this at a low tempo:
```
...C2...
.D112T0g
.*=w1g..
........
.D1.....
.*=a211.
```
( change `=a211` with a message to trigger your samples ).

As orca send the OSC messages from the top to the bottom, you can change the `w` timing multiple times in the same orca patch to using different delays for different groups of instruments. 

# Tuning

Folderkit uses the `/x` `/y` and `/z` addresses for special messages that will change the behavior of the transpose arguments. After one of those message is triggered all the successive transpose values will be affected, so you usually trigger the tuning messages rarely (maybe once for each orca file).

This chapter include a bit of math about frequency ratios, but for more informations about intonation and tuning i strongly advice the third chapter of [Musimathics](http://musimathics.com/). For even more resources, check out the [Xenharmonic Wiki](https://en.xen.wiki/).

If you are not interested in microtunings, just read "Split", "Base Note" and "Default Tuning" for things that are still relevant if you are using standard western tempered tuning.

## Split 

The first argument of the all the tuning message will set the "split point" value for the Transpose. If you don't have to use any special microtuning, you use the `/x` message.  After the tuning message, all the transpose values after the split point will traspose the pitch lower, giving you the possibility to tune a sample down. The default value for the split point is `o` (24).

This sample will be played an two octave higher:

```
*=x2p.....
..........
.D........
.*=a4s1.o.
..........
```

## Base Note 

The second argument of all the tuning messages will set the base note for all the oscillators and effects, the note that is played when just the octave is used with no transpose. 

| orca | 0 | 1  | 2 | 3  | 4 | 5 | 6  | 7 | 8  | 9 | a  | b | c |
|------|---|----|---|----|---|---|----|---|----|---|----|---|---|
| note | C | C# | D | D# | E | F | F# | G | G# | A | B# | B | C |  

The tuning frequency for those note can be changed in [settings.conf](ch5_settings.md).

If there is a waveform in the `w/q` subfolder, it will be played as a F, fourth octave:

```
*=x2.5.......
.............
.D...........
.*=a7wq4..18.
.............
```

## Default Tuning

This message will reset the tuning to the default tempered tuning with `o` as split point and a base note of C:

```
    *=x2o.
```

## More Info on Tuning

When you use the transpose value, what you are actually doing is generating another frequency for the note or sample, relative to the base note or to the normal sample playback speed. By using tuning messages you define the ratios of those multiplications, defining new sets of pitches to be used in various ways.

By default folderkit uses the standard western 12-degree tempered tuning. If `tr` is the transpose value (0-35), and `freq` the generated frequency, and `base` the base frequency, then : 

```
    freq = base * 2^(tr/12)
```
    
( ^ is the simbol for "elevated to so " 2^4 means two elevated to the fourth ).

So the base frequency is multiplied to `2^(tr/12)` to get the frequency of our played note or sample playback.


## Tempered Tunings with Different Degrees

By using the third `/x` argument you can change the divisor in the previous expression, so we will call this argument `den` as denominator, so 

```
    f = base * 2^(tr/den)
```
   
So you can get a tempered scale with any number of degrees. For example:


```
    *=x3..n
```
   
will generate a 23 degree tempered scale.

When the third argument is set to `0` or left empty then 36 degrees are used.


## Pure Tunings 

The `/y` will work as the `/x` message, but the resulting frequencies will be generated with ratios between integer numbers, so 

```
    freq = base * (ind+den)/den 
```
    
With `ind` incrementing from `0` to `den-1`. After a number `den` of degrees is generated the same ratios will be used for the next octave, and so on and so on. The first two arguments for `/y` are still split point and base note. So for example 

```
    *=y3.5a
```
    
will create a scale with frequency ratios with 10 as denominator and 10, 11, 12, 13 ... 19 as numerator, and base note F.

When the third argument is set to `0` or left empty then the denominator will be 36.


## Pure Tuning with Degree Indices

If you use more than 3 arguments for the `/y` message, then all the arguments for the fourth onwards will be the offset of the numerator from the denominator. Writing as `off` the offset you are choosing, the formula for the ratio of each degree is:

```
    freq = base * (off+den)/den 
```
    
For example:

```
    *=y9..a.12579
```
    
will generate a scale of 6 degrees, with ratios 10/10, 11/10, 12/10, 15/10, 17/10 and 19/10 (the `.` after `a` is sent by orca-c as 0, and it's useful to separate the denominator from the degree and to rapresent the unison ratio).


## Tempered Tuning with Degree Indices

An `/x` message with more than 3 degrees can be used in the same way of `/y` for generating diatonic temperated scales. Writing as `off` the offset you are choosing, the formula for the ratio of each degree is:

```
    freq = base * 2^(off/den)
```
    
For example 
```
    *=x8.4c.357a
```
    
is a E minor pentatonic scale.

## Pure Tuning with Ratio Couples

The `/z` message let you define ratios as `/y`, but with indipendent control for offset and denominator for each degree of the scale. The first two messages are still split point and base note, then you have to give a couple of numbers for each degree of the scale (usually you leave the first blank for the unison, any empty denominator will default to 36 do avoid division by 0). The formula is still:
```
    freq = base * (off+den)/den 
```
    
but you are choosing `off` and `den` for each degree.
   
For example:
```
    *=zeo...2a4a12168a
```
    
will generate a 6 degrees scale with the ratios 12/12 (unison), 12/10, 14/10, 3/2 (perfect fifth), 7/6, 18/10. The split point is set to orca value `o`.
        
next chapter : [Hints and Tricks](ch7_hints.md)
