
# Setup

## Building

At the moment i'm working on debian, but you should be able to build it on any *nix that have the required libraries (`jack`, `liblo` and a single precision `fftw3`).

to get the prerequisite packages on debian:
```console
sudo apt-get install liblo7 liblo-dev jackd1 libjack-dev libfftw3-dev libfftw3-single3
```   
    
to build `folderkit`

```console
git clone https://git.sr.ht/~npisanti/folderkit/
cd folderkit 
make
``` 
    
the `folderkit` executable will be created in the `folderkit/bin` folder. 

To install it globally (still debian): 
```console
sudo mv bin/folderkit /usr/bin/folderkit
```
    
or just move `folderkit` to whatever global bin path you are using.


## JACK

Folderkit is running connecting itself to a running JACK server (or launching one), so you need a working JACK installed on your computer. 

Usually you just need to install JACK from your package manager and then do some config, for linux setupping realtime limits is described [here](https://jackaudio.org/faq/linux_rt_config.html). 

Folderkit can start the JACK server by itself, but usually you want to launch it by yourself by command line or using a graphical tool like `qjackctl`.


## Managing your samples

When you start folderkit you give it a folder path for your sample. We will call this path "root" folder.

Folderkit searche for `.wav` files in the root folder, those files need to be nested in folders (with 0-z) as name and subfolders (again with 0-z) as names. Folder names should be lowercase. 

An example folder could look like this:

```
.
├── a
│   └── 0
│       ├── hit_a_01.wav
│       ├── hit_a_02.wav
│       ├── hit_a_03.wav
│       └── hit_a_04.wav
└── b
    ├── x
    │   └── frog_guiro_B_01.wav
    ├── y
    │   └── frog_hit_A_01.wav
    └── z
        └── hi_01.wav   
```
    
more on this in [chapter 3](ch3_files.md).
    
    
## Starting folderkit 

from the command line:
```console
folderkit [folder] [port]
```

`folderkit` will search for wavs in the given `[folder]`
    
`folderkit` will receive OSC messages form `orca` at the given `[port]`. 

When starting correctly, `folderkit` will print to console a point `.` for each mono sample loaded and a color `:` for each stereo sample loaded, both after the message "loading samples" and the message "loading /ir folder". 


## Hello World 

Put a `/s` folder into your samples path, then a `/1` folder into the `/s` folder, and a sample into it, like this:
```
.
└── s
    └── 1
        └── yoursample.wav
```
    
start folderkit like this:
    
```console
folderkit path/to/your/samples 49162 
```
    
folderkit should be now connected to JACK and waiting for OSC messages.

now start `orca`, activate the OSC output (address 49162 should be the default), and bang to /a
```
..D4.....
..*=a2s1.
......... 
```
folderkit should now play your sample each 4 frames.

ATTENTION! all the code examples are formatted for orca-c, for other versions you may need to ignore the second parameter (which sets the number of arguments). For example the previous example would be:
 ```
..D4.....
..*=as1..
......... 
```   

next chapter: [OSC basics](ch2_osc_basics.md)
