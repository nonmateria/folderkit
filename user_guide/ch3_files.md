
# Files

the `folderkit` binary takes a path for the files library as argument, we will call this given path the "root" directory.

Files in the root directory should be grouped into "folders" and "subfolders" using the orca letters ( lowercase ).

An example folder, with some of the files explained layer:

```console
.
├── a
│   └── 1
│       ├── -12.dB
│       ├── hit_a_01.wav
│       ├── hit_a_02.wav
│       ├── hit_a_03.wav
│       └── hit_a_04.wav
├── b
│   ├── a
│   │   └── loop_amen.wav
│   ├── p
│   │   └── loop_apache.wav
│   └── t
│       └── loop_think.wav
├── ir
│   ├── -36.dB
│   ├── kingtubby-fl1a.wav
│   └── kingtubby-fl1b.wav
├── w
│   ├── s
│   │   ├── -24.dB
│   │   └── saw.wav
│   ├── q
│   │   ├── -27.dB
│   │   └── square.wav
│   └── t
│       ├── -24.dB
│       └── triangle.wav
└── settings.conf
```

## Folder Basics

If there are more than a file in a subfolder, a random file is played, avoiding triggering the same sample twice in a row ( in the case of just two sample, they are just triggered one after the other ). If in the folder is present a file with a .dB extension, the name of that file is used as numeric value for scaling the volume of all the sample in the folder.

## Loop files 

If the filename starts with `loop` than the loaded sample will be played back looped. You probably want to control looped samples with envelopes or to choke them by playing back another nonexistent subfolder.

## Single cycle waveforms 

If the loaded sample buffer is lower than 1024 audio samples, the file will be loaded as a single cycle waveform. The third argument of the OSC message for a single cycle waveform will set the waveform octave, the fourth will set the transpose as usual. The waves will be played as a C note by default, you can set this base note with a [tuning message](ch6_tuning.md). When triggered a single cycle wavefor will loop, so you probably need to trigger them together with an amp envelope fx, more on this in the [effects](ch4_effects.md) chapter.

A squarish synth playing a C note, 4th octave (presuming the folder tree above), with a very short attack and a medium release:
```
.D...........
.*=a7wq4..19.
.............
```
   
If you use waveforms with a bigger size or samples with very short sizes you may want to change the default threshold for waveform/sample playback to something different than 1024 in the `settings.conf` file (more on this in [chapter 5](ch5_settings.md) ). 

Stereo files loaded as waveforms will load as mono, the right channel being ignored.

## Forcing a mode 

You can prepend the file name with one of those keywords to force that file into a specific mode, for example to make very short files play as one-shot instead of being considered as waveforms or to be sure that some files are always loaded as waveforms or loops:

* sample : will force to one-shot playback
* wave : will force to single cycle waveform
* loop : will playback the file as a loop 

## IR Folder 

If you want to use an FFT IR reverb with your samples, you can put a folder named `/ir` into the root folder, with a single mono or stereo `.wav` file for the impulse response.

You can control the FFT reverb send with a `.dB` file in the /ir folder. You can get more option for sends with a `settings.conf` file (more on this in [chapter 5](ch5_settings.md) ).

next chapter: [Effects](ch4_effects.md)
