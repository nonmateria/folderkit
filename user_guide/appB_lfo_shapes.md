
# LFO Shapes

The first value of `u` and `w` can be used to select different lfo shapes, in looping or one-shot mode.

| orca | shape                    
|------|-----------------------------------|
| m    | sine looping                      | 
| n    | sine one-shot                     | 
| w    | saw looping                       | 
| v    | saw one-shot                      | 
| x    | exponential saw looping           | 
| l    | exponential saw one-shot          | 
| r    | ramp looping                      | 
| f    | ramp one-shot                     | 
| q    | square looping                    | 
| p    | square one-shot                   | 
| a    | triangle looping                  | 
| t    | triangle one-shot                 | 
| y    | exponential triangle one-shot     | 
| i    | exponential triangle one-shot     | 
