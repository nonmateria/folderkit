
# Hints and Tricks

The most important advice i give you on using folderkit is just to keep you messages as simple as possible. Organize your folders with ready to use instruments already mixed at the right volumes, keeping the arguments count for your messages to 5 or less (just folder, subfolder, pitch and pan should be enough).

When you add FX, do not clutter their arguments together, leave an empty space between then so you can quickly recognize the parameters for each FX. Many times you can leave the last argument in the fx to `.`, gaining a space to separate it from the next FX.


## Sustain / Choke 

This structure will trigger the sample (with a `a0x` env) and fade it out when the `E` operator reaches the second message:

```
.D..............
H.=a7gr..a0x....
Ey...*=a7....a14
```

## 1-voice subtractive synth

By using the single cycle waveforms together with the various kind of filters, you can get a message that forms a basic subtractive synth. Assuming 'w/s' is a saw wave:
```
D4....................
.=aiws3..n05.m4004.a28
```
is a simple synth with an oscillator ( `aiws3` )  going into a 24dB/oct lowpass filter ( `m4004` ) modulated by its own envelope ( `n05` ), going into an AR amp envelope ( `a28` ). 


## 1-voice FM synth

Folderkit features a FM operator FX, which uses the wave/sample as modulator, so if you have some single cycles waves for the modulator you can get a basic one voce FM synth. Assuming `w/i` is a sine wave:
```
..pV3.....................
..........................
.D.....Kp......Kp.........
.*=1hw243.a8a.o4340x.a4d..
..........................
```
As the last argument of the operator is `x` the output won't be modulated by the first envelope, so the first envelope will act on the sampled wave ( the modulator ) and the second envelope will shape the output of the `o` operator ( the carrier ).


## Karplus synthesis with the k comb filter 

You can use an amp envelope to create an impulse to feed the into a `k` comb filter, creating a karplus-strong synthesizer. Assuming `x/p` is a noise (for example pink noise) sample:
```
.D...............
..=acxp..a02.k37d
```
The `k23d` is connotating the `k` comb filter, octave `3`, transpose `7` and `d` feedback. The amp envelope as a release of `2`, for creating the short impulse. Different release times can create louder or softer notes (or you can use the third argument of the amp, left to `.` here ).

## Copypaste presets

Yyou can do is write down a list of parameters to manually copy together, some kind of copy-paste presets.

Here i memorize two different envelopes + dynamics combinations:
```
.D1....Ks..2dg.
.*=29s437.a2dg.
...........6xc.
```
I copy `2dg` or `6xc` manually to change the sound from a full volume with a percussive envelope to lower volume with a flat envelope used to smoothly change volume.

## Grainclouds

```
.D1.....1Ra...
.*i8.#p#.1....
.j0A1.4AhJ....
.*=19sal.1.a78
```
This structure will trigger the voices 1-9 one after each other, `a78` is a smooth short envelope used as grain window, lower value generate a more stuttering sound, the number on the bottom of `#p#` is used to control the (jittered) grain position. The grains will be panned at random with `1Ra`. The samples in `/s/a` will be used. Many variation on this structure are possible, for example adding FX like lowpass or comb filters, or by duplicating the structure to make more grainclouds (remember to change the number that is added to the voice address, in this case `1`, to avoid the use of the same voices in different clouds).

## Clicks 

If you hear non-intended clicky sounds there are some things in folderkit that could be the cause:
- retriggering a playing voice but with a change in pan or in the number of used FX (those are meant to be added while the voice is silent)
- retriggering a waveform playing a long release with an attack equal to 0 (set the attack to 1) 
- triggering a sample with position different than 0 and `fade_on_offset` set to 0
- triggering two long samples one after the other with `chock_fadeout` set to 0
- very short envelope (`0`-`3`) are usually of less than 15ms and can still cause clicks in some waveforms 
