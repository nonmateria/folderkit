
# `settings.conf`

If you want to change some of the default settings for the sampler engine, you can put a `settings.conf` file in your samples root folder. This is totally optional. 

You can assign a value in each line, with a `parameter = value` style, 

This is an example `settings.conf` :
```
# anything can be a comment 
env_attack_max_ms = 1000
env_release_max_ms = 2000

reference_freq = 440

reverb_low_cut = 100

```
   
parameter names should begin at the line start, without any whitespace before :

```
# this would have no effect 
env_attack_max_ms = 1000
```

## Parameters list

| value                  | default | note                                                                                      |
|------------------------|---------|-------------------------------------------------------------------------------------------|
| env_attack_max_ms      | 1250    | a, e, n envelope FXs max attack time in ms                                                |
| env_hold_max_ms        | 2500    | e envelope FX max hold time in ms                                                         |
| env_release_max_ms     | 2500    | a, e, n  envelope FXs max release time in ms                                              |
| env_db_min             | -30     | a amp envelope dynamics control minimum in db, for the value 1 in the 1-g range           |
| volume_db_min          | -30     | v volume control minimum value in dB, for the value 1 in the 1-g range                    |
| silence_threshold_db   | -72     | voices quieter than this will be considered silent and stopped after some seconds         |
| delay_time_max_ms      | 3000    | max time for the delay effects (more on ch.4)                                             |
| delay_lfo_rate_hz      | 0.5     | delay modulation LFO rate in hz                                                           |
| slew_octave_ms         | 500     | ms time for the gliding an interval of an octave (more on ch.4)                           |
| default_slew_ms        | 1       | default voice slew time in ms, used when s slew is not set                                |
| lfo_slew_freq          | 1000    | frequency of lfo smoothing in hz, lower the frequency to reduce lfo clicks                |
| max_wave_length        | 1024    | as explained in chapter 3                                                                 |
| reference_freq         | 440     | tuning frequency in hertz, default to A = 440hz                                           |
| choke_fadeout_ms *     | 20      | when a new sample is triggered for each voice, the old will have a short fade             |
| fade_on_offset_ms *    | 5       | when a sample is not played from the start, this sample will have a short fade            |
| reverb_low_cut         | 20      | low cut frequency for the IR Reverb, in hertz                                             |
| drift_amount           | 0.0     | enable drifting of the samplers frequency, value in semitones                             |
| sine_resolution        | 1024    | FM operator sine wavetable size, in audio samples                                         |
| fm_operators_gain      | -24.0   | base volume in dB for the FM synth operators ( more on FM in ch.4 )                       |
| master_gain            | 0       | gain control in dB for the master output                                                  |
| master_clipping        | -2.0    | threshold in dB of a soft clip saturation applied to the main output, 0.0 deactivates it  |
| reverb_jack            | 0       | if not 0 creates extra JACK channels for the reverb or as extra auxs when no IR is given  |
| jack_autoconnect       | 1       | when set to 0 it will not automatically connect folderkit to system L/R outputs           |

* those values are also used to crossfade between waves, but only when there is a wave change.

## Groups

You can group some sampler voices and have some gain and send control for those groups, by using those parameters:

| value          | default | note                                                                 |
|----------------|---------|----------------------------------------------------------------------|
| group_voices   | ...     | creates a new group with the given voices                            |
| group_gain     | 0       | group gain before the soft clip in dB, unused if group_clipping is 0 |
| group_clipping | 0       | threshold for a soft clipper in dB, 0 for no clipping                |
| group_makeup   | 0       | group gain after the soft clip                                       |
| group_send     | 0       | group send to reverb (or to the send jack channels)                  |
| group_jack     | 0       | if not 0 creates extra JACK channels for this group (dry signal)     |

When you use a group_voices parameter, a new group is created with the given voices. The successive parameters will apply just to this group. For example

```
group_voices = all
group_makeup = 0
group_send = -24

group_voices = abcd
group_makeup = -12
group_send = -14

group_voices = nmjkl
group_gain = 6
group_clipping = -2
group_makeup = -12
group_send = -24

```

will create two groups, one with the voice with address `/a /b /c /d` and one with the voices `/n /m /j /k /l`, with different parameters. 

A voice assigned by one group can be reassigned to successive `group_voices` assignation, and by default all the voices goes to the first group, so `group_voices = all` or `group_voices = default` or anything similar can be used at the top to define a default group for all the voices that are not in the others.
      
next chapter: [Microtuning and Timings](ch6_tuning.md)
