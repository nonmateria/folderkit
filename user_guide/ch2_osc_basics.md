
# OSC Basics

Folderkit will receive OSC messages to its 32 voices, addresses from `/0` to `/v` are used. `/w` is used to shuffle timing and `/x`, `/y`, `/z` are used for [microtuning](ch6_tuning.md). 

Each sampler voice from `/0` to `/v` plays one sample at time, if a playing voice receives a new message the old sample will be chocked.

Remember that the c version of `orca` sends a value of 0 for a unsetted OSC argument, so 0 is sometimes changed by folderkit into another default values, as stated into each argument description. 


## 1st/2nd Arguments : Folder, Subfolder

The first two arguments of an OSC message for folderkit are the folder and subfolder of the sample to play. For example with
```
.D.......
.*=a2s1..
.........
```
the sampler voice `/a` will play the sample in the folder `s/1`. If there are more samples in the same folder a random sample will be played (avoiding the repetition of the same sample two time in a row).

## 3rd Argument: Position / Wave Octave

The third OSC argument will set the starting position of playback or the wave octave if a single cycle waveform has been loaded (more on wave files in [chapter 3](ch3_files.md).

The start position of the sample is set in 16th of the sample length, from `0` to `g`, values `h`-`x` will still set the starting point in 16th of a sample but with an additional position jitter equal to 1/16th of the length, so 
```
.D.......
.*=a3s1h.
.........
```
will start the playback from a random position from 0 to 6.25 % of the sample.

Summinng `h` with the `A` operator can be useful to change the `0`-`g` values to `h`-`x` jittered values:
```
.D....hA2
.*=a3s1j.
.........
```

If a single cicle waveform is loaded this same argument will set the wave octave, values `0`-`g` or `h`-`w` will set the octave. Values `h`-`w` are still mapped to octaves 0-15. `x` `y` and `z` will set sub-audio octaves. The oscillator phase will be reset each time a new waveform is chosen. More on how to load single cycle waveforms in the [chapter 3](ch3_files.md).

## 4th Argument : Transpose 

The fourth OSC argument will transpose the playback of the sample or will set the pitch of the wave. By default the playback will be transpose upwards in tempered semitones, up to the letter `n` (+23 semitones). Letters from to `o` to `z` will traspose down the sample from -12 to -1 semitones. The behavior of this parameter can be changed with [tuning messages](ch6_tuning.md). 

By default:

```
.D.......
.*=a4s1.c
.........
```
   
will playback the sample an octave higher (c=12).

Use this code to quickly listen all the available pitches:

```
.......D1.
.D1.....i.
.*=a4s1...
```

## No Retriggering 

Selecting `0` as folder will change all the sampler parameters but it will not retrigger the sample, you can use it to change the parameter of an already triggered voice, for example this code

```
.D........
..=a2s1...
.h........
.D1.....R.
.*=a4...o.
```

will trigger a sample each 8 frames and randomly change its pitch the other frames. Not that the subfolder and the start position arguments will be ignored when there is no retriggering, so it won't matter if you set it them 0 or not.

## 5th Argument : Pan 

The fifth OSC argument will set the pan of the sample from left to right, with the number 1-9. Any other value (0 included) will default to 5 (center). 

| orca | 1    | 2     | 3    | 4     | 5   | 6    | 7   | 8    | 9   |
|------|------|-------|------|-------|-----|------|-----|------|-----|
| pan  | -1.0 | -0.75 | -0.5 | -0.25 | 0.0 | 0.25 | 0.5 | 0.75 | 1.0 |
    
This will play our sample only in the right channel:

```
.D........
.*=a5s1..9
..........
```
    
next chapter: [managing files](ch3_files.md)
