
# FX Arguments 

After the first 4 arguments, you can use a letter in the 'a'-'z' range to select an effect processor to apply to the sample. Each effect takes up a number of the next arguments for its parameters, after that you can use another `a`-`z` letter again to add another effect, and so on for up to 6 available effects. Some of this effects (the envelopes for now) generate a secondary signal that can also be used to modulate a parameter of the next selected effect. 

If an fx is selected as the 5 argument (that is the pan control by default), the pan control won't be set and will default to 0.0f;

# List of Processors 

| processor                 | code | 1th       | 2th       | 3th        | 4th        | 5th     |
|---------------------------|------|-----------|-----------|------------|------------|---------|
| AR Amp Envelope           | a    | attack    | release   | gain       |            |         |
| 12dB/oct Band Pass Filter | b    | octave    | transpose | resonance  | mod amount |         |
| 24dB/oct Band Pass Filter | c    | octave    | transpose | resonance  | mod amount |         |
| Clocked Delay             | d    | time      | feedback  | time mod   | send       |         |
| AHR Amp Envelope          | e    | attack    | hold      | release    | gain       |         |
| WaveFolder                | f    | in gain   | shape     | feedback   | out gain   |         |
| Gritty Delay              | g    | time      | feedback  | waveloss   | send       |         |
| 12dB/oct High Pass Filter | h    | octave    | transpose | resonance  | mod amount |         |
| 24dB/oct High Pass Filter | i    | octave    | transpose | resonance  | mod amount |         |
| Formant Filter            | j    | part      | vowel     | slew       | high cut   |         |
| Comb Filter               | k    | octave    | transpose | feedback   | mod amount |         |
| 12dB/oct Low Pass Filter  | l    | octave    | transpose | resonance  | mod amount |         |
| 24db/oct Low Pass Filter  | m    | octave    | transpose | resonance  | mod amount |         |
| AR Mod Envelope           | n    | attack    | release   | multiply   |            |         |
| FM Operator               | o    | octave    | transpose | fm amount  | feedback   | routing |
| 4 Pole Phaser             | p    | octave    | transpose | phaser fb  | spread     |         |
| Modal Bank                | q    | octave    | transpose | ratio mode | bank q     |         |
| Ring Modulator            | r    | octave    | transpose | mix        | mod amount |         |
| Slew                      | s    | slew amt  |           |            |            |         |
| Tanh Saturator            | t    | drive     | octave    | transpose  | trim       |         |
| Clocked LFO Modulator     | u    | shape     | time      | slew oct   |            |         |
| Volume Control            | v    | volume    | smooth    |            |            |         |
| Clocked LFO Tremolo       | w    | shape     | time      | amp mod    |            |         |
| Decimator                 | x    | octave    | transpose | bits       |            |         |
| Ducker                    | y    | voice     | attack    | release    | gain redux |         |
| Waveloss                  | z    | control   |           |            |            |         |

octave + transpose: many processor operate at a certain frequency, this frequency is operated and tuned in the same identical way of the single cycle waveforms, so you can play with a filter or even with the decimator frequency. The tahn saturator has a 12dB/oct lowpass non-resonant filter after the saturation you can use to darken the harmonics.

# Processor Guide 

## `a` `e` `n` envelopes 

Values of `0`-`g` into the first argument will control the attack timing of the envelope. The time value is controlled exponentially, with a max default value of 1250 ms (you can change this value in [settings.conf](ch5_settings.md)).

Values of `0`-`g` into the second argument will control the release timing of the envelope. The time value is controlled exponentially, with a max default value of 2500 ms (you can change this value in [settings.conf](ch5_settings.md)). 

A value of  `x`, `y` or `z` for the second argument will set the release time to infinite (actually to one day, but let's say infinite). You could use this feature to simulate an envelope with sustain, you will need to trigger another message with a shorter envelope later to trigger the release stage ( use `0` as folder to avoid retriggering the sample, if you use a waveform it won't be retriggered by default. ) 

In the case of the `a` envelope the third argument controls the the gain of envelope peak value in dB, using the `1`-`g` range, from a settable minumum (default to -30.0 dB) to 0dB. A value of `0` is also defaulted to 0 dB, so you can avoid setting this argument and have no change in gain. The minimum can be setted in the `settings.conf` file.
For the n envelope the dynamics are linearly scaled from 0.0625 to 1.0 in increments of 0.0625, using the `1`-`g` range for control (`0` also default to 1.0 as `g`, for having full envelope multiplication when you don't set this value).
A value of `x`, `y` or `z` to the third arguments will just mute the envelope, for both `a` end `n` envelopes ( generating a flat envelope signal ). 
The this third value is the target toward what the attack stage will raise, if the actual envelope output is greater than this value the first stage won't be an attack but a decay stage, using the same time values.

The `a` amp envelope will multiply the input signal for the envelope signal, the `n` envelope won't change the sound but it is used for only modulating the successive effects.

The `e` envelope works as the `a` envelope but with an additional hold stage controlled with the second parameter, the third argument controls the release and the fourth argument controls the dynamics as described in the `a` envelope parameters.

## `b` `c` `h` `i` `l` `m` filters  

`b` `c` will select a bandpass with 12 or 24 db/oct slope, `h` `i` will select highpass filters and `l` `m` will select a lowpass filter. 

The first argument will select the octave of the cutoff frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument select the resonance amount with the `0`-`g` range, the filters will resonate with high resonance and can be played in tune with the octave and transpose control. 

The fourth argument will control the amount of modulation from the last processed modulator ( for example the `a` amp envelope).

## `d` clocked delay `g` gritty delay

The first argument controls timing the delay time measured in orca frames. Some values will set it to fractional amounts: `0` for half frame, `z` for a quarter of a frame, `y` for 1/8th and `x` for 1/16th. This value is smoothed out to avoid clicks on tempo change, the slew can generate some slight pitch shifting on value or tempo changes.

The second argument controls the delay feedback, `0`-`g` is mapped from zero to endless feedback. THe feedback value for the Comb Filter is still in the 0-g range but mapped with a different curve.

For the clocked delay `d` the third argument controls the amount of modulation applied to the delay timing, uses values from `0` to `g`. `0` (no modulation) by default. A triangle LFO is used for modulation, with a 0.5 hz rate (this value can be changed in the `settings.conf` file).
For the gritty delay `g` the third argument controls a waveloss FX inside the feedback path, using the same values as `z` control argument.

The fourth arguments controls the delay send gain reduction. The value is in negative decibels so for example `9` will mean -9 dB. This value will trim the signal before entering the dalay line. A value of `x`, `y` or `z` will mute the delay send without chocking the current feedbacks. A value of `w` will set the send to 0dB and deactivate the dry signal. Defaults to 0 dB. 

## `f` wavefolder

The first argument controls the gain for the wavefolder, twice the value given, for example `8` will increase of 16 dB. Defaults to 0.

The second arguments controls the wavefolder shape, value of `0`-`g` will use a sine shape and value of `h`-`x` will set a triangle folder. Defaults to `8`.

The third argument controls the amount of a saturated feedback path after the wavefolding, value `0`-`g` are mapped to a 0-4 range. Defaults to 0.

The fourth argument reduces the output of the wavefolder of twice the value given, for example a value of `a` will reduce the gain of 20 dB. Defaults to 0 dB.

## `j` formant filter

This is a formant filter that can be used to give a vocal quality to sounds. Beware: the combination of some part, vowels and input frequencies can generate loud feedbacks.

The first argument is the kind of singer to emulate, the value used to select it can be: `0` or `b` for the Bass part, `1` or `t` for the Tenor part, `2` or `c` for the Countertenor part, `3` or `a` for the Alto part, `4` or `s` for the Soprano part.

The second argument is for the vowel: `0` or `a` for A, `1` or `e` for E, `2` or `i` for I, `3` or `o` for O, `4` or `u` for U.

The third argument will set the paramenter for slewing between vocals, the change will be slower as the value increase.

The fourth value will control a 12dB/oct lowpass filter to darken the overall sound, by default the filter is fully open, use a value greater than `0` to activate it.

## `k` comb filter 

The comb filter can be used for flanging or to resonate some frequencies, at high resonance it can be used for karplus-strong synthesis.

The first argument will select the octave of the cutoff frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument select the resonance amount with the `0`-`g` range, the filters will resonate with high resonance and can be played in tune with the octave and transpose control. 

The fourth argument will control the amount of modulation from the last processed modulator ( for example the `a` amp envelope).

## `o` FM operator 

The FM operator is the basic block for FM synthesis, it takes the audio input and uses it to modulate a sine wave with a settable frequency.

The first argument will select the octave of the sine frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument will select the amount of modulation from the audio input to the carrier wave. At 0 no modulation will be applied so just a sine wave with the given frequency will be heard. 

The fourth argument will control the self-modulation from the operator output to the frequency ( fm feedback ). The output is taken after the amp modulation and before the gain control ( configurable in `settings.conf` ).

The fifth argument will define the routing for the amp and the input. It can have four values: 
- `m` is the default and sets the operator output to be multiplied for the last modulation (`a` `e` or `n` envelope or `w` lfo). 
- `x` deactivates the amp modulation and let the operator signal pass unmultiplied. 
- `a` multiplies the operator signal like `m` but adds the unmultiplied input to the output (usuall the input is already multiplied by the same envelope). 
- `b` let both the input signal and operator output pass unmultiplied. 

So for a 2op FM synth, you need a string like this (`p` is the pitch, sequenced elsewhere, `fw` is for folder and wave):

```
.D.....Kp......Kp.....
.*=1hfw40.a3b.o4080...
......................
```

## `p` 4 pole phaser

The first argument will select the octave of the cutoff frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument controls the phaser feedback, acting a bit like a filter resonance.

The fourth argument controls the spread of the phaser's poles frequencies. Defaults to 4. Formula is freq[n] = basefreq * (1.0 + (n*spread/4), where n=0,3.

## `q` modal bank

The `q` code will select a modal bank, with various selectable modes.

The first argument will select the base octave of the resonator frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument selects a set of ratios that are multiple of the fundamental frequency, see [Appendix A](appA_modal_bank.md) for more info on the various modes.

The fourth argument selects the Q value of the modal bank resonators, an higher value will create longer tails for the percussions.

## `r` ring modulator 

The ring modulator takes the audio input and multiply it for a sine wave with a settable frequency.

The first argument will select the octave of the sine frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument controls the mix of the ring modulated signal with the original, `0`-`g` is mapped to the 0% - 100% range. 

The fourth argument will apply the last processed modulation to the pitch of the sine wave.

## `s` slew control

The `s` code won't actually process the signal but it will set the slew (or "glide", or "portamento") amount to the transpose parameters of all the FX and samplers BEFORE the 's' code. It only works on the transpose parameter, it won't slew octave changes. it can range from `1` to `g` and `0` deactivates it. Using `s` does not count as using an FX so you won't waste an FX slot. Use it more than once to set different slew times, or put it as the last code for controlling the slew paramenters of all the FX and samplers. If not set, the slew defaults to 1ms/octave, this value is settable in the `settings.conf` file. 

## `t` tanh saturator 

The `t` code will select a saturator based on a tanh curve. 

The first argument gain increase in decibel before the saturation DSP, `6` for example would mean +6dB and `c` would mean +12dB.

The second argument will select the octave of the cutoff frequency, and the third argument will select the transpose (as with the waveforms), both for an integrated 12dB/oct nonresonant lowpass filter placed after the saturator. 

The fourth argument controls the saturator trim gain. The value is in negative decibels so for example `9` will mean -9 dB. The gain reduction is applied to the whole signal after low pass filtering.

## `u` and `w` LFOs

The LFO can be used as modulations instead of the envelopes. The `w` LFO will also modulate the amp for tremolo effect or it can be used as timed envelope when a single cycle shape is selected. The LFO shape is smoothed with a 12dB/oct lowpass filter to avoid clicky transitions; this value is fixed for `w` but can be changed for `u`. 

The first argument controls the LFO shape. Different values will select different looping or single-cycle shapes, the codes are reported in the [appendix B](appB_lfo_shapes.md). The waveform is reset each time the voice is triggered, with the exception of when `0` (or nothing) is selected as wave shape. When `0` is used as argument the last wave selected will be used, without being retriggered (the new time parameter will be used). 

The second argument controls timing the delay time measured in orca frames, clocked to the tempo. Some values will set it to fractional amounts: `0` for half frame, `z` for a quarter of a frame, `y` for 1/8th and `x` for 1/16th. 

The third value is different for `u` and `w`: 
- For `u` it will control the octave of the smoothing filter, when nothing is selected the value is the same as when `6` is selected.
- For `w` it will control the amount of modulation applied to amp, ranging from `1` to `g`, when `0` is selected the same value of `g` will be used (100% modulation of amp value).

## `v` volume

This is just a gain stage, with a settable slew to the given gain.

The first argument controls the the desired gain using the `1`-`g` range, from a settable minumum (default to -30.0 dB) to 0dB. A value of `x`, `y`, `z` or `0` will set target amplitude to zero, muting the voice. The minimum can be setted in the `settings.conf` file.

Values of `0`-`g` into the second argument will smooth out the volume changes, using the same time controls as the envelope attack, the default value of `0` will just set the gain to the desired volume, generating clicks.

## `x` decimator

The `x` code will select a decimator/bitcruncher.

The first argument will select the octave of the sample rate frequency, and the second argument will select the transpose (as with the waveforms). 

The third argument controls the number of bits for the signal, for example `8` would reduce the audio to an 8 bit signal. 0 deactivates bit reduction in the decimator.

## `y` ducker

The `y` code will select the "ducker".

The ducker is the only effects acting on another voice, it is an AR envelope that reduces the gain of a target voice, usually to simulate side-chain compression.

The first argument will select the target voice, all the voice are valid, incuding the calling. 

The second argument is the attack value, the time it will take to reach maximum gain reduction.

The third argument is the release value, the time it will take to go back to no gain reduction.

All those two values are influenced by the settings for attack and release envelope timings, for more info check the [settings](ch5_settings.md) manual page.

The fourth argument is the gain reduction in dB, ranging from `0` (no gain reduction) to `w` (32dB gain reduction). `x` `y` and `z` will also set 0 gain reduction.

## `z` waveloss

The `z` code select a "waveloss" effect, an effect that will ciclically mute parts of the waveform.

The only argument gradually shift many parameters, from `1` to `g` a proportional mode is used for the waveloss, from `i` to `z` a random mode is used instead. `0` and `h` produces clean signals.

# No arguments: Clearing Voices 

Many FX have internal buffer variables that are not cleared even if the FX is successively being deactivated, that could lead to strange behavior (expecially with delays, as some old audio could be still inside the delay). You can trigger a voice with no arguments to clear all its buffers and immediately stop all the sample playing. 

This will clear and stop the voice `a`:
```
......
*=a...
......
```
    
next chapter : [settings](ch5_settings.md)
