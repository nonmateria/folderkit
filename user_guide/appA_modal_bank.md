
# Modal Bank Ratios

the `q` and `w` FX are used for a modal bank of tuned resonators that simulates the behavior of some percussion bodies when struck. The third argument of the fx is used to select the ratios of the resonators in relationship to the frequency given by the first two arguments. Many of those ratios come from [this page](https://csound.com/docs/manual/MiscModalFreq.html) of the csound reference. The letters from `o` to `z` will use different methods to generate the ratios from the setted microtonal [tuning](ch7_tuning.md).

| orca | info                     | ratios                                                       |
|------|--------------------------|--------------------------------------------------------------|
| 1    | one                      | 1.0                                                          |
| 2    | two                      | 1.0, 2.0                                                     |
| 3    | four (default)           | 1.0, 2.0, 3.0, 4.0                                           |
| 4    | six                      | 1.0, 2.0, 3.0, 4.0, 5.0, 6.0                                 |
| 5    | eight                    | 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0                       |
| 6    | primes                   | 1.0, 2.0, 3.0, 5.0, 7.0, 11.0                                |
| 7    | powers of two            | 1.0, 2.0, 4.0, 8.0, 16.0                                     |
| 8    | powers of three          | 1.0, 3.0, 9.0, 27.0                                          |
| 9    | odds                     | 1.0, 3.0, 5.0, 7.0, 9.0, 11.0                                |
| a    | even                     | 1.0, 2.0, 4.0, 6.0, 8.0, 10.0                                |
| b    | pseudo xylophone         | 1.0, 4.0, 9.0, 16.0, 24.0, 31.0                              |
| c    | pseudo vibraphone 1      | 1.0, 4.0, 11.0, 18.0, 23.0, 34.0                             |
| d    | pseudo vibraphone 2      | 1.0, 4.0, 9.5, 16.0, 21.0, 29.0                              |
| e    | pseudo chalandi plates   | 1.0, 1.5, 6.0, 7.5, 14.0                                     |
| f    | pseudo wine glass        | 1.0, 2.5, 4.0, 6.5, 9.0                                      |
| g    | pseudo wooden bar        | 1.0, 2.5, 5.0, 7.0, 10.0, 12.0                               |
| h    | red cedar wood plate     | 1.0, 1.47, 2.09, 2.56                                        |
| i    | douglas fir wood plate   | 1.0, 1.42, 2.11, 2.47                                        |
| j    | dahina tabla             | 1.0, 2.89, 4.95, 6.99, 8.01, 9.02                            |
| k    | bayan tabla              | 1.0, 2.0, 3.01, 4.01, 4.69, 5.63                             |
| l    | singing bowl 180 mm      | 1.0, 2.77828, 5.18099, 8.16289, 11.66063, 15.63801, 19.99    |
| m    | singing bowl 152 mm      | 1.0, 2.66242, 4.83757, 7.51592, 10.64012, 14.21019, 18.14027 |
| n    | singing bowl 140 mm      | 1.0, 2.76515, 5.12121, 7.80681, 10.78409                     |
| o    | stepped, size-1          | from tuning                                                  |
| p    | stepped, size+1          | from tuning                                                  |
| q    | stretch 0.0              | from tuning                                                  |
| r    | stretch 0.5, simple rev. | from tuning                                                  |
| s    | stretch 0.5, simple      | from tuning                                                  |
| t    | stretch 0.5, from center | from tuning                                                  |
| u    | stretch 1.0, simple rev. | from tuning                                                  |
| v    | stretch 1.0, simple      | from tuning                                                  |
| w    | stretch 1.0, from center | from tuning                                                  |
| x    | stretch 2.0, simple rev. | from tuning                                                  |
| y    | stretch 2.0, simple      | from tuning                                                  |
| z    | stretch 2.0, from center | from tuning                                                  |
