
#pragma once

#define ORCA_GLYPHS_SIZE 36

#define NUMVOICES 32

#define FKIT_MAXLINE 1024
#define FKIT_MAXVALUECHARS 128

#define FK_WAIT_BEFORE_OSC 250

#ifndef M_TWO_PI
#define M_TWO_PI 6.2831853071795864769252867665590f
#endif

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795f
#endif

#define orca_0 0
#define orca_1 1
#define orca_2 2
#define orca_3 3
#define orca_4 4
#define orca_5 5
#define orca_6 6
#define orca_7 7
#define orca_8 8
#define orca_9 9
#define orca_a 10
#define orca_b 11
#define orca_c 12
#define orca_d 13
#define orca_e 14
#define orca_f 15
#define orca_g 16
#define orca_h 17
#define orca_i 18
#define orca_j 19
#define orca_k 20
#define orca_l 21
#define orca_m 22
#define orca_n 23
#define orca_o 24
#define orca_p 25
#define orca_q 26
#define orca_r 27
#define orca_s 28
#define orca_t 29
#define orca_u 30
#define orca_v 31
#define orca_w 32
#define orca_x 33
#define orca_y 34
#define orca_z 35

#define orca_tempo_change 42

#define FKIT_WAVELOSS_MODE_PULSE 1
#define FKIT_WAVELOSS_MODE_RANDOM 2
