#pragma once

#include "dsp/fft_reverb.h"
#include "dsp/softclip.h"
#include "osc_buffer.h"
#include "voice.h"

#include <stdlib.h>

typedef struct fk_message_buffer_t {
	fk_osc_message_t ** data;
	unsigned size;
	unsigned max_size;
	double step_samples; // amount of a single orca timed step in audio samples
	unsigned offset;     // w message offset
} fk_message_buffer_t;

typedef struct fk_audio_inputs_t {
	unsigned channels;
	long ci;             // circular index
	long max;            // max size of buffers, max for input_ci
	double step_samples; // to multiply with start pos, recalculate on tempo change
	int glyph;

	float * buffers_ref[orca_z + 1]; // reference to buffers
} fk_audio_inputs_t;

typedef struct fk_synth_t {
	float ** buffers; // group buffers
	float ** outputs; // audio device outputs
	unsigned num_outputs;
	fk_parameters_t * parameters;
	fk_osc_buffer_t * osc_link;
	fk_tuning_t * tuning_link;
	fk_voice_t ** voices;
	dsp_fft_reverb_t * rev[2];

	fk_audio_inputs_t indata;
	fk_message_buffer_t messages;
} fk_synth_t;

int fk_synth_prepare_to_play(fk_synth_t * synthesizer, fk_parameters_t * config, fk_sample_folder_t ** library, fk_sample_t * impulses, fk_osc_buffer_t * osc_buffer);

void fk_synth_release(fk_synth_t * syn);

void fk_synth_process(fk_synth_t * syn, unsigned buffersize);
