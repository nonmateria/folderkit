#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "dsp/buffer.h"
#include "dsp/functions.h"
#include "dsp/rand.h"
#include "flags.h"
#include "voice.h"

#define FKIT_ZRELEASE -1

fk_voice_t * fk_voice_create(fk_parameters_t * config)
{
	fk_voice_t * voice = malloc(sizeof(fk_voice_t));
	if (voice == NULL)
		return NULL;

	for (unsigned i = 0; i < 2; ++i) {
		voice->samplers[i] = dsp_sampler_create();
		voice->outputs[i] = dsp_create_buffer(config->buffersize);
		dsp_set_zero(voice->outputs[i], config->buffersize);

		for (int f = 0; f < FK_NUM_FX; ++f) {
			voice->fxchain[f].vaf[i] = dsp_filter_create(config->samplerate);
			voice->fxchain[f].decimator[i] = dsp_decimator_create(config->samplerate);
			voice->fxchain[f].sat[i] = dsp_saturator_create(config->samplerate);
			voice->fxchain[f].op[i] = dsp_operator_create(config->sine_wave, config->sine_size,
			                                              config->samplerate);
			voice->fxchain[f].pha[i] = dsp_phaser_create(config->samplerate);
			voice->fxchain[f].vow[i] = dsp_formant_create(config->samplerate);
			voice->fxchain[f].rm[i] = dsp_ringmod_create(config->sine_wave, config->sine_size,
			                                             config->samplerate);
			voice->fxchain[f].delay[i] = dsp_delay_create(config->samplerate, config->delay_max_ms,
			                                              config->delay_lfo_rate_hz,
			                                              config->drift_amount);
			voice->fxchain[f].mb[i] = dsp_modal_create(config->samplerate);
			voice->fxchain[f].wf[i] = dsp_wavefolder_create();
			voice->fxchain[f].wl[i] = dsp_waveloss_create();
		}
	}

	float max_sil = config->env_attack_max_ms + config->env_hold_max_ms + config->env_release_max_ms;
	voice->sil = dsp_silence_create(config->samplerate, max_sil, config->silence_threshold_db);

	voice->freq_buffer = dsp_create_buffer(config->buffersize);
	voice->dummy_mod = dsp_create_buffer(config->buffersize);
	dsp_set_zero(voice->dummy_mod, config->buffersize);
	// 0.2 hz drift
	voice->drift = dsp_drift_create(0.2, config->samplerate, config->buffersize);

	voice->slew_smp = dsp_slew_create(config->samplerate);

	for (int f = 0; f < FK_NUM_FX; ++f) {
		voice->fxchain[f].slew = dsp_slew_create(config->samplerate);
		voice->fxchain[f].env = dsp_envelope_create(config->buffersize, config->samplerate,
		                                            config->env_attack_max_ms,
		                                            config->env_hold_max_ms,
		                                            config->env_release_max_ms);
		voice->fxchain[f].lfo = dsp_lfo_create(config->buffersize, config->samplerate,
		                                       config->sine_wave, config->sine_size);
	}
	// TODO add memory checks for all allocated resources

	dsp_set_zero(voice->freq_buffer, config->buffersize);

	voice->enabled = 0;
	voice->current_sampler = 0;
	voice->pans[0] = 1.0f;
	voice->pans[1] = 1.0f;

	for (int f = 0; f < FK_NUM_FX; ++f) {
		voice->fxchain[f].fx_mode = 0;
		voice->fxchain[f].duck_voice = NO_DUCKING;
		voice->fxchain[f].mod_amt = 0.0f;
		voice->fxchain[f].max_freq = (float)(config->samplerate * 0.25);
		if (voice->fxchain[f].max_freq > 20000.0) {
			voice->fxchain[f].max_freq = 20000.0;
		}
		// TODO: set oct and xpose to values that flags errors
	}
	voice->used_fx = 0;
	voice->default_slew = config->default_slew_ms;
	voice->default_lfo_slew = config->lfo_slew_freq;

	voice->tuning_link = NULL;
	return voice;
}

void fk_voice_destroy(fk_voice_t * voice)
{
	if (voice != NULL) {
		// put deallocations here
		for (unsigned i = 0; i < 2; ++i) {
			dsp_sampler_destroy(voice->samplers[i]);
			dsp_destroy_buffer(voice->outputs[i]);
			for (int f = 0; f < FK_NUM_FX; ++f) {
				dsp_phaser_destroy(voice->fxchain[f].pha[i]);
				dsp_formant_destroy(voice->fxchain[f].vow[i]);
				dsp_filter_destroy(voice->fxchain[f].vaf[i]);
				dsp_decimator_destroy(voice->fxchain[f].decimator[i]);
				dsp_saturator_destroy(voice->fxchain[f].sat[i]);
				dsp_operator_destroy(voice->fxchain[f].op[i]);
				dsp_delay_destroy(voice->fxchain[f].delay[i]);
				dsp_ringmod_destroy(voice->fxchain[f].rm[i]);
				dsp_modal_destroy(voice->fxchain[f].mb[i]);
				dsp_waveloss_destroy(voice->fxchain[f].wl[i]);
			}
		}
		for (int f = 0; f < FK_NUM_FX; ++f) {
			dsp_slew_destroy(voice->fxchain[f].slew);
			dsp_envelope_destroy(voice->fxchain[f].env);
			dsp_lfo_destroy(voice->fxchain[f].lfo);
		}
		dsp_destroy_buffer(voice->freq_buffer);
		dsp_destroy_buffer(voice->dummy_mod);

		dsp_drift_destroy(voice->drift);
		dsp_slew_destroy(voice->slew_smp);
		dsp_silence_destroy(voice->sil);

		free(voice);
	}
}

void fk_voice_clear(fk_voice_t * voice)
{
	voice->samplers[0]->sample = NULL;
	voice->samplers[1]->sample = NULL;

	voice->used_fx = 0;
	for (int f = 0; f < FK_NUM_FX; ++f) {
		voice->fxchain[f].fx_mode = 0; // deactivate all fx

		for (unsigned i = 0; i < 2; ++i) {
			dsp_delay_clear(voice->fxchain[f].delay[i]);
			dsp_modal_clear(voice->fxchain[f].mb[i]);
			dsp_phaser_clear(voice->fxchain[f].pha[i]);
			dsp_saturator_clear(voice->fxchain[f].sat[i]);
			dsp_filter_clear(voice->fxchain[f].vaf[i]);
			dsp_waveloss_clear(voice->fxchain[f].wl[i]);
			// decimator, operator and ringmod don't need to be cleared
		}
	}

	dsp_slew_reset(voice->slew_smp, 1.0);
	for (int f = 0; f < FK_NUM_FX; ++f) {
		dsp_slew_reset(voice->fxchain[f].slew, 1.0);
		dsp_env_clear(voice->fxchain[f].env);
		dsp_lfo_clear(voice->fxchain[f].lfo);
	}
}

void fk_fxs_trigger(fk_voice_t * voice, unsigned argc, int * argv);

void fk_voice_trigger(fk_voice_t * voice, unsigned argc, int * argv, long input_ci, double input_step)
{
	// TODO: make voices more robust to instantaneous retriggering
	if (argc < 1) {
		fk_voice_clear(voice);
		voice->enabled = 0;
		return;
	}

	voice->enabled = 1;
	dsp_silence_retrigger(voice->sil);

	// resets slew times
	voice->slew_smp->time = voice->default_slew;
	for (int f = 0; f < FK_NUM_FX; ++f) {
		voice->fxchain[f].slew->time = voice->default_slew;
	}

	unsigned folder = (unsigned)argv[0];
	unsigned subfolder = 0;
	float start = 0.0;
	float wave_freq = 440.0f;
	float pan = 0.0f;
	float smp_transpose = 1.0;

	if (argc >= 2) {
		subfolder = (unsigned)argv[1];
	}

	if (argc >= 3) {
		int arg = argv[2];
		wave_freq = (float)voice->tuning_link->oct[arg];
		if (arg == orca_z) { // small offset
			start = 1.0f / 32.0f;
		} else if (arg == orca_y) {
			start = 1.0f / 64.0f; // smaller offset
		} else if (arg >= 17) {
			arg = arg - 17; // jittered start
			float jit = dsp_urand() * 0.0625f;
			start = (((float)arg) / 16.0f) + jit;
		} else {
			start = (((float)arg) / 16.0f);
		}
	}

	if (argc >= 4) {
		smp_transpose = (float)voice->tuning_link->xpose[argv[3]];
	}

	if (argc >= 5) {
		switch (argv[4]) {
		// clang-format off
		default: pan = 0.0f; break;
		case orca_1: pan = -1.0f;   break;
		case orca_2: pan = -0.75f;  break;
		case orca_3: pan = -0.5f;   break;
		case orca_4: pan = -0.25f;  break;
		case orca_5: pan =  0.0f;   break;
		case orca_6: pan =  0.25f;  break;
		case orca_7: pan =  0.5f;   break;
		case orca_8: pan =  0.75f;  break;
		case orca_9: pan =  1.0f;   break;
			// clang-format on
		}
	}
	float theta = (pan * M_PI + M_PI) * 0.25f;
	voice->pans[0] = (float)cos(theta);
	voice->pans[1] = (float)sin(theta);

	if (folder != 0) { // avoid retriggering when folder 0
		// setting variables --------------------
		fk_sample_t * sample = fk_library_search(voice->library_link,
		                                         folder, subfolder);
		unsigned current_sampler = voice->current_sampler;
		unsigned old_sampler = current_sampler ? 0 : 1;

		fk_sample_t * old_sample = voice->samplers[current_sampler]->sample;

		int sample_change = sample != old_sample;

		// switch samplers
		if (sample == NULL || sample_change || sample->mode != FKIT_MODE_WAVE) {
			old_sampler = current_sampler;
			current_sampler = old_sampler ? 0 : 1;
		}

		int old_sample_is_wave = old_sample != NULL &&
		                         old_sample->mode == FKIT_MODE_WAVE;
		int new_sample_is_wave = sample != NULL && sample->mode == FKIT_MODE_WAVE;

		// old wave fade out or choke ------------
		if (voice->samplers[old_sampler]->sample != NULL) {
			dsp_sampler_fade_out(voice->samplers[old_sampler],
			                     voice->parameters_link->samplers_choke_fadeout,
			                     voice->parameters_link->samplerate);
		}

		// new sample trigger --------------------
		dsp_sampler_trigger(voice->samplers[current_sampler],
		                    sample, start, wave_freq,
		                    voice->parameters_link->samplers_fade_on_offset,
		                    voice->parameters_link->samplerate,
		                    input_ci, input_step);

		// wave reset on sample change
		if (new_sample_is_wave && sample_change) {
			voice->samplers[current_sampler]->cursor = 0.0f;

			// wave fade in on wave change
			if (old_sample_is_wave) {
				dsp_sampler_fade_in(voice->samplers[current_sampler],
				                    voice->parameters_link->samplers_fade_on_offset,
				                    voice->parameters_link->samplerate);
			}
		}
		voice->current_sampler = current_sampler;
	} else {
		dsp_sampler_set_octave(voice->samplers[voice->current_sampler],
		                       wave_freq, voice->parameters_link->samplerate);
	}

	// FX triggering
	fk_fxs_trigger(voice, argc, argv);

	// check and apply slews,
	// slew times are set is set in fk_fxs_trigger
	// TODO: slew jump on sample change???
	dsp_slew_to(voice->slew_smp, smp_transpose);
	for (unsigned f = 0; f < voice->used_fx; ++f) {
		dsp_slew_to(voice->fxchain[f].slew, voice->fxchain[f].transpose);
	}
}

void fk_fxs_trigger(fk_voice_t * voice, unsigned argc, int * argv)
{
	unsigned quit = argc + 16;
	unsigned offset = 4;
	unsigned fx = 0;
	unsigned last_slew = 0;
	unsigned sampler_to_slew = 1;

	for (int f = 0; f < FK_NUM_FX; ++f) {
		voice->fxchain[fx].duck_voice = NO_DUCKING;
	}

	while (offset < argc) {
		int mode = argv[offset];
		int adv = -1;

		switch (mode) {
		case orca_a: // AR amp envelope
			if (argc > offset + 2) {
				adv = 4;
				int dyn = (argc > offset + 3) ? argv[offset + 3] : 0;
				if (dyn == 0) {
					dyn = 16;
				}
				dsp_env_trigger(voice->fxchain[fx].env, argv[offset + 1], 0,
				                argv[offset + 2], dyn, ENV_USE_DB,
				                voice->parameters_link->env_db_min);
			} else {
				adv = 0;
			}
			break;

		case orca_e: // AHR amp envelope
			if (argc > offset + 3) {
				adv = 5;
				int dyn = (argc > offset + 4) ? argv[offset + 4] : 0;
				if (dyn == 0) {
					dyn = 16;
				}
				dsp_env_trigger(voice->fxchain[fx].env, argv[offset + 1], argv[offset + 2],
				                argv[offset + 3], dyn,
				                ENV_USE_DB, voice->parameters_link->env_db_min);
			} else {
				adv = 0;
			}
			break;

		case orca_n: // AR mod envelope
			if (argc > offset + 2) {
				adv = 4;
				int dyn = (argc > offset + 3) ? argv[offset + 3] : 0;
				if (dyn == 0) {
					dyn = 16;
				}
				dsp_env_trigger(voice->fxchain[fx].env, argv[offset + 1], 0,
				                argv[offset + 2], dyn, ENV_USE_MULT,
				                -1234.0); // the db minimum is not needed
			} else {
				adv = 0;
			}
			break;

		case orca_v: // smoothed gain (volume)
			if (argc > offset + 1) {
				adv = 3;
				int a = (argc > offset + 2) ? argv[offset + 2] : 0;
				if (a > 16) {
					a = 16;
				};
				dsp_env_trigger(voice->fxchain[fx].env, a, 0, orca_x,
				                argv[offset + 1], ENV_USE_DB,
				                voice->parameters_link->volume_db_min);
			} else {
				adv = 0;
			}
			break;

		case orca_y: // ducker
			if (argc > offset + 3) {
				adv = 5;
				unsigned dest = (unsigned)argv[offset + 1];
				if (dest > orca_v) {
					dest = NO_DUCKING;
				}
				voice->fxchain[fx].duck_voice = dest;
				int dyn = (argc > offset + 4) ? argv[offset + 4] : 0;
				dsp_env_trigger(voice->fxchain[fx].env, argv[offset + 2], 0,
				                argv[offset + 3], dyn, ENV_INVERT_DB,
				                -1234.0); // the db minimum is not needed
			} else {
				adv = 0;
			}
			break;
		// filters -----------
		case orca_b:
		case orca_c:
		case orca_l:
		case orca_m:
		case orca_h:
		case orca_i:
			if (argc > offset + 1) { // at least the oct cut value
				adv = 5;
				float fx_transpose = 1.0f;
				float reso = 0.0f;
				float mod_amt = 0.0f;
				if (argc > offset + 1) {
					voice->fxchain[fx].oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}
				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					reso = (float)argv[offset + 3] / 16.0f;
					if (reso > 1.0f) {
						reso = 1.0f;
					}
					reso = 1.0f - reso;
					reso *= reso;
					reso = 1.0f - reso;
					reso *= 4.0f;
				}
				if (argc > offset + 4) {
					mod_amt = (float)argv[offset + 4];
				}
				voice->fxchain[fx].transpose = fx_transpose;
				voice->fxchain[fx].vaf[0]->reso = reso;
				voice->fxchain[fx].vaf[1]->reso = reso;
				voice->fxchain[fx].mod_amt = mod_amt;
			} else {
				adv = 0;
			}
			break;

		case orca_j:
			if (argc > offset + 2) { // at least part and vowel
				adv = 5;
				int slew = 0;
				int part = argv[offset + 1];
				int vocal = argv[offset + 2];
				float lpf = (float)voice->tuning_link->oct[16];
				if (argc > offset + 3) {
					slew = argv[offset + 3];
				}
				if (argc > offset + 4) {
					int sel = argv[offset + 4];
					if (sel != 0) {
						lpf = (float)voice->tuning_link->oct[sel];
					}
				}
				dsp_formants_set(voice->fxchain[fx].vow, part, vocal, slew, lpf);
			} else {
				adv = 0;
			}
			break;

		case orca_p:
			if (argc > offset + 1) { // at least octave
				adv = 5;
				float spread = 0.0f;
				float fb = 0.0f;
				float fx_transpose = 1.0f;
				if (argc > offset + 1) {
					voice->fxchain[fx].oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}

				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					fb = (float)argv[offset + 3] / 16.0f;
					if (fb > 1.0f) {
						fb = 1.0f;
					}
					fb = 1.0f - fb;
					fb *= fb;
					fb = 1.0f - fb;
					fb *= 0.96f;
				}
				if (argc > offset + 4) {
					spread = 0.25f * (float)argv[offset + 4];
				}
				voice->fxchain[fx].transpose = fx_transpose;
				voice->fxchain[fx].pha[0]->feedback = fb;
				voice->fxchain[fx].pha[1]->feedback = fb;
				voice->fxchain[fx].pha[0]->spread = spread;
				voice->fxchain[fx].pha[1]->spread = spread;
			} else {
				adv = 0;
			}
			break;

		case orca_f:
			if (argc > offset + 2) { // at least input and fold
				adv += 5;

				int fmode = 0;
				float fold = 0.5f;
				int inputdb = 0;
				int outputdb = 0;
				float fb = 0.0f;

				if (argc > offset + 1) {
					inputdb = argv[offset + 1] * 2;
				}
				if (argc > offset + 2) {
					fold = 0.0625f * (float)argv[offset + 2];
					if (fold >= 1.0f) {
						fold -= 1.0f;
						fmode = 1;
					}
					if (fold >= 1.0f) {
						fold = 1.0f;
					}
				}
				if (argc > offset + 3) {
					fb = (float)argv[offset + 3] * 0.25f;
					if (fb > 4.0f) {
						fb = 4.0f;
					}
				}
				if (argc > offset + 4) {
					outputdb = -argv[offset + 4] * 2;
				}

				dsp_wavefolders_set(2, voice->fxchain[fx].wf, inputdb, fold, fmode, outputdb, fb);
			} else {
				adv = 0;
			}
			break;

		case orca_d:
			if (argc > offset + 1) { // at least time
				adv = 5;
				float send = 1.0f;
				float dry = 1.0f;
				float fb = 0.0f;
				float mod_amt = 0.0f;
				float sync = 1.0f;

				if (argc > offset + 1) {
					switch (argv[offset + 1]) {
					case 0:
						sync = 0.5f;
						break;
					case orca_z:
						sync = 0.25f;
						break;
					case orca_y:
						sync = 0.125f;
						break;
					case orca_x:
						sync = 0.0625f;
						break;
					default:
						sync = (float)argv[offset + 1];
						break;
					}
				}
				if (argc > offset + 2) {
					fb = ((float)argv[offset + 2]) / 16.0f;
					if (fb > 1.0f) {
						fb = 1.0f;
					}
				}
				if (argc > offset + 3) {
					mod_amt = ((float)argv[offset + 3]) / 16.0f;
					if (mod_amt > 1.0f) {
						mod_amt = 1.0f;
					}
					mod_amt = mod_amt * mod_amt * mod_amt;
				}
				if (argc > offset + 4) {
					switch (argv[offset + 4]) {
					case orca_w:
						dry = 0.0f;
						send = 1.0f;
						break;
					case orca_x:
					case orca_y:
					case orca_z:
						send = 0.0f;
						break;
					default:
						send = (float)dB((double)-argv[offset + 4]);
						break;
					}
				}
				dsp_delays_set_params(2, voice->fxchain[fx].delay, sync, fb, dry, send, mod_amt);
			} else {
				adv = 0;
			}
			break;

		case orca_g:                     // gritty delay
			if (argc > offset + 1) { // at least time
				adv = 5;
				float send = 1.0f;
				float dry = 1.0f;
				float fb = 0.0f;
				int wl_arg = 0;
				float sync = 1.0f;

				if (argc > offset + 1) {
					switch (argv[offset + 1]) {
					case 0:
						sync = 0.5f;
						break;
					case orca_z:
						sync = 0.25f;
						break;
					case orca_y:
						sync = 0.125f;
						break;
					case orca_x:
						sync = 0.0625f;
						break;
					default:
						sync = (float)argv[offset + 1];
						break;
					}
				}
				if (argc > offset + 2) {
					fb = ((float)argv[offset + 2]) / 16.0f;
					if (fb > 1.0f) {
						fb = 1.0f;
					}
				}
				if (argc > offset + 3) {
					wl_arg = argv[offset + 3];
				}
				if (argc > offset + 4) {
					switch (argv[offset + 4]) {
					case orca_w:
						dry = 0.0f;
						send = 1.0f;
						break;
					case orca_x:
					case orca_y:
					case orca_z:
						send = 0.0f;
						break;
					default:
						send = (float)dB((double)-argv[offset + 4]);
						break;
					}
				}
				dsp_delays_wl_set_params(2, voice->fxchain[fx].delay, sync, fb, dry, send, wl_arg);
			} else {
				adv = 0;
			}
			break;

		case orca_k:
			if (argc > offset + 1) { // at least octave
				adv = 5;
				float fx_transpose = 1.0f;
				float fb = 0.0f;
				float fx_mod_amt = 0.0f;
				if (argc > offset + 1) {
					voice->fxchain[fx].oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}
				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					fb = ((float)argv[offset + 3]) / 16.0f;
					if (fb > 1.0f) {
						fb = 1.0f;
					}
					fb = 1.0f - fb;
					fb = fb * fb * fb;
					fb = 1.0f - fb;
				}
				if (argc > offset + 4) {
					fx_mod_amt = (float)argv[offset + 4];
				}
				voice->fxchain[fx].transpose = fx_transpose;
				voice->fxchain[fx].mod_amt = fx_mod_amt;
				voice->fxchain[fx].delay[0]->feedback = fb;
				voice->fxchain[fx].delay[1]->feedback = fb;
			} else {
				adv = 0;
			}
			break;

		case orca_x:
			if (argc > offset + 1) { // just decimation oct freq
				adv = 4;
				int bits = 0;
				float fx_transpose = 1.0f;
				// float fx_mod_amt = 0.0f;
				if (argc > offset + 1) {
					voice->fxchain[fx].oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}
				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					bits = argv[offset + 3];
				}
				/*
				if (argc > offset + 4) {
				        fx_mod_amt = (float)argv[offset + 4];
				}
				*/
				voice->fxchain[fx].transpose = fx_transpose;
				voice->fxchain[fx].mod_amt = 0.0f;
				dsp_decimator_set_bits(voice->fxchain[fx].decimator[0], bits);
				dsp_decimator_set_bits(voice->fxchain[fx].decimator[1], bits);
			} else {
				adv = 0;
			}
			break;

		case orca_o:
			if (argc > offset + 1) { // at least oct
				adv = 6;
				float ogain = voice->parameters_link->fm_op_amp;
				float omodulator = 0.0f;
				float oself = 0.0f;
				float igain = 0.0f;
				float oampmod = 1.0f;
				float fx_transpose = 1.0f;

				if (argc > offset + 1) {
					voice->fxchain[fx].oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}
				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					omodulator = (float)argv[offset + 3];
					omodulator /= 16.0f;
					if (omodulator > 1.0f) {
						omodulator = 1.0f;
					}
					omodulator *= omodulator;
					omodulator *= 2.0f;
				}
				if (argc > offset + 4) {
					oself = (float)argv[offset + 4];
					oself = oself / 16.0f;
					if (oself > 1.0f) {
						oself = 1.0f;
					}
					oself *= oself * oself;
				}

				if (argc > offset + 5) {
					switch (argv[offset + 5]) {
					case orca_m:
					default:
						oampmod = 1.0f;
						igain = 0.0f;
						break;
					case orca_a:
						oampmod = 1.0f;
						igain = 1.0f;
						break;
					case orca_b:
						oampmod = 0.0f;
						igain = 1.0f;
						break;
					case orca_x:
						oampmod = 0.0f;
						igain = 0.0f;
						break;
					}
				}
				for (int i = 0; i < 2; ++i) {
					voice->fxchain[fx].transpose = fx_transpose;
					voice->fxchain[fx].op[i]->op_gain = ogain;
					voice->fxchain[fx].op[i]->input_gain = igain;
					voice->fxchain[fx].op[i]->modulator_amount = omodulator;
					voice->fxchain[fx].op[i]->self_amount = oself;
					voice->fxchain[fx].op[i]->amp_modulation = oampmod;
				}
			} else {
				adv = 0;
			}
			break;

		case orca_q:
			if (argc > offset + 3) { // at least mode and freqs
				adv = 5;
				float q = 189.84375;
				int mmode = 0; // defaults to 0 for now
				float fx_oct = 1.0f;
				float fx_transpose = 1.0f;
				if (argc > offset + 1) {
					fx_oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}
				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					mmode = argv[offset + 3];
				}
				if (argc > offset + 4) {
					q = (float)argv[offset + 4];
					if (q == 0.0f) {
						q = 6.0f;
					}

					q /= 16.0f;
					if (q > 1.0f) {
						q = 1.0f;
					}
					q = q * q * q;
					q = q * 3600.0f;
				}
				dsp_modals_set_coeff(2, voice->fxchain[fx].mb, (float)(fx_oct * fx_transpose),
				                     mmode, q, voice->tuning_link->xpose,
				                     voice->tuning_link->degrees);
			} else {
				adv = 0;
			}
			break;

		case orca_r:
			if (argc > offset + 1) {
				adv = 5;
				float rmix = 1.0f;
				float fx_transpose = 1.0f;
				float fx_mod_amt = 0.0f;
				if (argc > offset + 1) {
					voice->fxchain[fx].oct = (float)voice->tuning_link->oct[argv[offset + 1]];
				}
				if (argc > offset + 2) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 2]];
				}
				if (argc > offset + 3) {
					rmix = (float)argv[offset + 3];
					rmix /= 16.0f;
					if (rmix > 1.0f) {
						rmix = 1.0f;
					}
				}
				if (argc > offset + 4) {
					fx_mod_amt = (float)argv[offset + 4];
				}
				voice->fxchain[fx].transpose = fx_transpose;
				voice->fxchain[fx].mod_amt = fx_mod_amt;
				voice->fxchain[fx].rm[0]->mix = rmix;
				voice->fxchain[fx].rm[1]->mix = rmix;
			} else {
				adv = 0;
			}
			break;

		case orca_s: // slew setter
			if (argc > offset + 1) {
				float t = ((float)argv[offset + 1]) / 16.0f;
				if (t > 1.0f) {
					t = 1.0f;
				}
				t = t * t * t * voice->parameters_link->slew_octave_ms;

				// set the slew times for all the effects before the s glyph
				if (sampler_to_slew) {
					voice->slew_smp->time = t;
					sampler_to_slew = 0;
				}
				for (unsigned f = last_slew; f < fx; ++f) {
					voice->fxchain[f].slew->time = t;
				}
				last_slew = fx;
				offset += 2; // do not advance FX, just set the slew
			} else {
				adv = 0;
			}
			break;

		case orca_t:
			if (argc > offset + 2) {
				adv = 5;
				float fx_oct = 17.0f;
				float fx_transpose = 1.0f;
				dsp_saturator_set_drive(voice->fxchain[fx].sat[0], argv[offset + 1]);
				dsp_saturator_set_drive(voice->fxchain[fx].sat[1], argv[offset + 1]);
				fx_oct = (float)voice->tuning_link->oct[argv[offset + 2]];

				if (argc > offset + 3) {
					fx_transpose = (float)voice->tuning_link->xpose[argv[offset + 3]];
				}
				if (argc >= offset + 4) {
					dsp_saturator_set_output_gain(voice->fxchain[fx].sat[0], argv[offset + 4]);
					dsp_saturator_set_output_gain(voice->fxchain[fx].sat[1], argv[offset + 4]);
				}
				dsp_saturator_set_cutoff(voice->fxchain[fx].sat[0], fx_oct * fx_transpose);
				dsp_saturator_set_cutoff(voice->fxchain[fx].sat[1], fx_oct * fx_transpose);
			} else {
				adv = 0;
			}
			break;

		case orca_u:
			if (argc > offset + 2) { // at least wave + time
				adv = 4;
				float amp_mod = 0.0f;
				float slew = (float)voice->tuning_link->oct[6];
				int wave = argv[offset + 1];
				int sixteenths = argv[offset + 2];

				if (argc > offset + 3) {
					int o = argv[offset + 3];
					if (o == 0) {
						o = 6;
					}
					slew = (float)voice->tuning_link->oct[o];
				}

				dsp_lfo_set_params(voice->fxchain[fx].lfo, wave, sixteenths, amp_mod, slew);
			} else {
				adv = 0;
			}
			break;

		case orca_w:
			if (argc > offset + 2) { // at least wave + time
				adv = 4;
				float amp_mod = 1.0f;
				float slew = voice->default_lfo_slew; // default slew in hz
				int wave = argv[offset + 1];
				int sixteenths = argv[offset + 2];

				if (argc > offset + 3) {
					amp_mod = ((float)argv[offset + 3]) / 16.0f;
					if (amp_mod > 1.0f) {
						amp_mod = 1.0f;
					}
					if (amp_mod == 0.0f) {
						amp_mod = 1.0f;
					}
				}

				dsp_lfo_set_params(voice->fxchain[fx].lfo, wave, sixteenths, amp_mod, slew);
			} else {
				adv = 0;
			}
			break;

		case orca_z:
			if (argc > offset + 1) {
				adv = 2;
				dsp_waveloss_set(voice->fxchain[fx].wl[0], argv[offset + 1]);
				dsp_waveloss_set(voice->fxchain[fx].wl[1], argv[offset + 1]);
			} else {
				adv = 0;
			}
			break;

		default: // no fx code
			offset++;
			break; // no operator found
		}

		if (adv > 0) {
			voice->fxchain[fx].fx_mode = mode;
			fx++;
			if (fx >= FK_NUM_FX) {
				offset = quit;
			}
			offset += (unsigned)adv;
		} else if (adv == 0) { // not enough args for last fx
			offset = quit;
		}
	}

	voice->used_fx = fx;
}

void fk__freqbuff_process(fk_voice_t * voice, fk_effect_t * fx, float * mod_buffer, float drift_amount, unsigned buffersize)
{
	dsp_slew_process(fx->slew, voice->freq_buffer, buffersize);
	dsp_apply_drift_mod(voice->freq_buffer, voice->drift->buffer, drift_amount, buffersize);
	dsp_calculate_fx_freq(voice->freq_buffer, fx->oct, mod_buffer, fx->mod_amt,
	                      fx->max_freq, buffersize);
}

void fk_voice_process(fk_voice_t * voice, unsigned start, unsigned buffersize)
{
	if (voice->enabled) {

		// TODO: make checks on envelopes, fx and detect silence
		// to avoid uneeded calculatins
		// for now all the 32 voices are always calculated!

		float drift_amount = voice->parameters_link->drift_amount;
		dsp_drift_render(voice->drift, buffersize);

		dsp_slew_process(voice->slew_smp, voice->freq_buffer, buffersize);

		dsp_set_zero(voice->outputs[0] + start, buffersize);
		dsp_set_zero(voice->outputs[1] + start, buffersize);

		unsigned channels = 0;
		channels += dsp_sampler_layer(voice->samplers[0], voice->outputs, start,
		                              voice->freq_buffer, voice->drift->buffer, drift_amount, buffersize);
		channels += dsp_sampler_layer(voice->samplers[1], voice->outputs, start,
		                              voice->freq_buffer, voice->drift->buffer, drift_amount, buffersize);
		channels--;
		if (channels > 2) {
			channels = 2;
		}

		float * mod_buffer = NULL; // assign this to the last used modulation
		// the function used will correcty avoid computations when mod_buffer is NULL

		for (unsigned f = 0; f < voice->used_fx; ++f) {
			fk_effect_t * fx = &voice->fxchain[f];
			int mode = voice->fxchain[f].fx_mode;

			// TODO there are still some buffers to clear to avoid NaNs?

			switch (mode) {
			case orca_a:
			case orca_e: // amp envelope is also a modulation
				dsp_envelope_render(fx->env, start, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_a_eq_axb(voice->outputs[c] + start, fx->env->buffer + start, buffersize);
				}
				mod_buffer = fx->env->buffer + start;
				break;

			case orca_n: // mod envelope
				dsp_envelope_render(fx->env, start, buffersize);
				mod_buffer = fx->env->buffer + start;
				break;

			case orca_v: // amp envelope is used, but not set as modulation
				dsp_envelope_render(fx->env, start, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_a_eq_axb(voice->outputs[c] + start, fx->env->buffer + start, buffersize);
				}
				break;

			case orca_y: // envelope is only rendered
				dsp_envelope_render(fx->env, start, buffersize);
				break;

			case orca_w: // lfo, amp mod
				dsp_lfo_render(fx->lfo, buffersize);
				float amp_mod = fx->lfo->amount;
				for (unsigned c = 0; c < channels; c++) {
					dsp_amp_modulate(voice->outputs[c] + start, fx->lfo->buffer, amp_mod, buffersize);
				}
				mod_buffer = fx->lfo->buffer;
				break;

			case orca_u: // lfo, only modulation
				dsp_lfo_render(fx->lfo, buffersize);
				mod_buffer = fx->lfo->buffer;
				break;

			case orca_b:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_filter_process_bpf2(fx->vaf[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_c:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_filter_process_bpf4(fx->vaf[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_h:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_filter_process_hpf2(fx->vaf[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_i:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_filter_process_hpf4(fx->vaf[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_l:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_filter_process_lpf2(fx->vaf[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_m:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_filter_process_lpf4(fx->vaf[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_j:
				for (unsigned c = 0; c < channels; c++) {
					dsp_formant_process(fx->vow[c], voice->outputs[c] + start, buffersize);
				}
				break;

			case orca_p:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_phaser_process(fx->pha[c], voice->outputs[c] + start,
					                   voice->freq_buffer, buffersize);
				}
				break;

			case orca_f:
				for (unsigned c = 0; c < channels; c++) {
					dsp_wavefolder_process(fx->wf[c], voice->outputs[c] + start, buffersize);
				}
				break;

			case orca_d: // modulated delay
				for (unsigned c = 0; c < channels; c++) {
					dsp_delay_process_sync(fx->delay[c], voice->outputs[c] + start,
					                       voice->drift->buffer, buffersize);
				}
				break;

			case orca_g: // gritty delay
				for (unsigned c = 0; c < channels; c++) {
					dsp_delay_process_wl(fx->delay[c], voice->outputs[c] + start, buffersize);
				}
				break;

			case orca_k:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_delay_process_tuned(fx->delay[c], voice->outputs[c] + start,
					                        voice->freq_buffer, buffersize);
				}
				break;

			case orca_o:
				// this correctly process with the NULL mod buffer
				fk__freqbuff_process(voice, fx, NULL, drift_amount, buffersize);
				// a zero filled buffer is needed for the operator
				float * op_mod = mod_buffer;
				if (op_mod == NULL) {
					op_mod = voice->dummy_mod;
				}
				for (unsigned c = 0; c < channels; c++) {
					dsp_operator_process(fx->op[c], voice->outputs[c] + start,
					                     voice->freq_buffer, op_mod, buffersize);
				}
				break;

			case orca_q:
				for (unsigned c = 0; c < channels; c++) {
					dsp_modal_process(fx->mb[c], voice->outputs[c] + start, buffersize);
				}
				break;

			case orca_r:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_ringmod_process(fx->rm[c], voice->outputs[c] + start,
					                    voice->freq_buffer, buffersize);
				}
				break;

			case orca_t:
				for (unsigned c = 0; c < channels; c++) {
					dsp_saturator_process(fx->sat[c], voice->outputs[c] + start, buffersize);
				}
				break;

			case orca_x:
				fk__freqbuff_process(voice, fx, mod_buffer, drift_amount, buffersize);
				for (unsigned c = 0; c < channels; c++) {
					dsp_decimator_process(fx->decimator[c], voice->outputs[c] + start,
					                      voice->freq_buffer, buffersize);
				}
				break;

			case orca_z:
				for (unsigned c = 0; c < channels; c++) {
					dsp_waveloss_process(fx->wl[c], voice->outputs[c] + start, buffersize);
				}
				break;

			default:
				break;
			}
		}

		float pl = voice->pans[0];
		float pr = voice->pans[1];

		if (channels == 1) {
			// copy right channel from left
			dsp_a_eq_bxs(voice->outputs[1] + start, voice->outputs[0] + start,
			             pr, buffersize);
			// now we can overwrite the left channel
			dsp_a_eq_bxs(voice->outputs[0] + start, voice->outputs[0] + start,
			             pl, buffersize);
		} else {
			dsp_a_eq_bxs(voice->outputs[0] + start, voice->outputs[0] + start,
			             pl, buffersize);
			dsp_a_eq_bxs(voice->outputs[1] + start, voice->outputs[1] + start,
			             pr, buffersize);
		}

		if (dsp_check_silence(voice->sil, voice->outputs, 2, start, buffersize)) {
			fk_voice_clear(voice);
			voice->enabled = 0;
		}
	}
}
