#include "synthesizer.h"
#include "dsp/buffer.h"
#include "dsp/functions.h"
#include "flags.h"
#include <stdio.h>
#include <string.h>

void fk__destroy_buffers(float ** buffers, unsigned channels)
{
	if (buffers != NULL) {
		for (unsigned i = 0; i < channels; ++i) {
			if (buffers[i] != NULL) {
				dsp_destroy_buffer(buffers[i]);
			}
		}
		free(buffers);
	}
}

float ** fk__allocate_buffers(unsigned buffersize, unsigned channels, const char * name)
{
	float ** buffers = malloc(sizeof(float *) * channels);
	if (buffers == NULL) {
		printf("memory error during %s allocation\n", name);
		goto error;
	}
	for (unsigned i = 0; i < channels; ++i) {
		buffers[i] = dsp_create_buffer(buffersize);
		if (buffers[i] == NULL) {
			printf("memory error during %s allocation\n", name);
			goto error;
		}
	}
	return buffers;
error:
	fk__destroy_buffers(buffers, channels);
	return NULL;
}

double fk__calculate_step_samples(double tempo, double samplerate);

long fk__calculate_input_buffer_max(fk_parameters_t * config)
{
	double samples = config->input_buffers_max_ms * (double)config->samplerate * 0.001;
	long buffersize = config->buffersize;
	long max = (long)samples;
	max = ((max / buffersize) * buffersize) + buffersize;
	return max;
}

int fk_synth_prepare_to_play(fk_synth_t * synthesizer, fk_parameters_t * config, fk_sample_folder_t ** library, fk_sample_t * impulses, fk_osc_buffer_t * osc_buffer)
{
	synthesizer->parameters = config;

	// allocate audio buffers
	synthesizer->buffers = fk__allocate_buffers(config->buffersize,
	                                            config->num_groups * 2,
	                                            "groups");
	if (synthesizer->buffers == NULL) {
		goto error;
	}

	// allocate outputs
	synthesizer->num_outputs = config->audio_outputs;
	synthesizer->outputs = fk__allocate_buffers(config->buffersize,
	                                            config->audio_outputs,
	                                            "audio outputs");
	if (synthesizer->outputs == NULL) {
		goto error;
	}

	synthesizer->osc_link = NULL;
	synthesizer->tuning_link = NULL;

	// ----  allocate voices array ----
	synthesizer->voices = malloc(NUMVOICES * sizeof(fk_voice_t *));
	if (synthesizer->voices == NULL) {
		printf("memory error during voice array allocation\n");
		goto error;
	}
	for (unsigned i = 0; i < NUMVOICES; ++i) {
		synthesizer->voices[i] = NULL;
	}

	// --------  create voices --------
	for (unsigned i = 0; i < NUMVOICES; ++i) {
		synthesizer->voices[i] = fk_voice_create(config);
		if (synthesizer->voices[i] == NULL) {
			printf("memory error during voice %d allocation\n", i);
			goto error;
		}
	}

	// create reverb
	if (impulses != NULL && impulses->buffers[0] != NULL) {
		synthesizer->rev[0] = dsp_fft_reverb_create(impulses->buffers[0],
		                                            impulses->length,
		                                            impulses->samplerate,
		                                            config->buffersize,
		                                            config->samplerate,
		                                            config->rev_low_cut);

		if (impulses->buffers[1] != NULL) {
			synthesizer->rev[1] = dsp_fft_reverb_create(impulses->buffers[1],
			                                            impulses->length,
			                                            impulses->samplerate,
			                                            config->buffersize,
			                                            config->samplerate,
			                                            config->rev_low_cut);
		} else {
			synthesizer->rev[1] = dsp_fft_reverb_create(impulses->buffers[0],
			                                            impulses->length,
			                                            impulses->samplerate,
			                                            config->buffersize,
			                                            config->samplerate,
			                                            config->rev_low_cut);
		}
	} else {
		synthesizer->rev[0] = NULL;
		synthesizer->rev[1] = NULL;
	}

	fk_ir_destroy(impulses);

	// set up high pass filters
	for (unsigned g = 0; g < config->num_groups; ++g) {
		if (config->groups[g]->highpass != 0.0f) {
			float freq = config->groups[g]->highpass;
			dsp_lowcut_set_cutoff(config->groups[g]->hpf_l, freq, config->samplerate);
			dsp_lowcut_set_cutoff(config->groups[g]->hpf_r, freq, config->samplerate);
		}
	}

	// input channels parameters --------------
	synthesizer->indata.channels = config->num_inputs;
	synthesizer->indata.ci = 0;
	synthesizer->indata.max = fk__calculate_input_buffer_max(config);
	synthesizer->indata.glyph = config->input_glyph;
	synthesizer->indata.step_samples = fk__calculate_step_samples(120.0, config->samplerate) * 32.0;
	for (int i = 0; i < (orca_z + 1); ++i) {
		synthesizer->indata.buffers_ref[i] = NULL;
	}
	// assigns references
	for (unsigned i = 0; i < config->num_inputs; ++i) { // audio channels start from 1
		synthesizer->indata.buffers_ref[i] = fk_get_input_sample(library, config, i + 1);
	}

	// osc buffer stuff ----------------------
	synthesizer->osc_link = osc_buffer;

	unsigned max_size = osc_buffer->size + 1;
	synthesizer->messages.data = malloc(sizeof(fk_osc_message_t *) * max_size);
	if (synthesizer->messages.data == NULL) {
		printf("[error] error allocating fk_osc_buffer data array\n");
		goto error;
	}
	synthesizer->messages.max_size = max_size;
	for (unsigned i = 0; i < max_size; ++i) {
		synthesizer->messages.data[i] = NULL;
	}
	synthesizer->messages.size = 0;
	synthesizer->messages.offset = 0;
	synthesizer->messages.step_samples = fk__calculate_step_samples(120.0, config->samplerate);

	return 0;

error:
	fk_synth_release(synthesizer);
	return 1;
}

void fk_synth_release(fk_synth_t * syn)
{
	printf("releasing synth resources...\n");
	if (syn->voices != NULL) {
		for (unsigned i = 0; i < NUMVOICES; ++i) {
			fk_voice_destroy(syn->voices[i]);
		}
		free(syn->voices);
	}

	for (unsigned i = 0; i < FKIT_NUM_IR; ++i) {
		dsp_fft_reverb_destroy(syn->rev[i]);
	}

	fk__destroy_buffers(syn->buffers, syn->parameters->num_groups * 2);
	fk__destroy_buffers(syn->outputs, syn->num_outputs);
	free(syn->messages.data);

	printf("...done\n");
}

static inline void modal_tune_change(fk_synth_t * syn)
{
	for (unsigned v = 0; v < NUMVOICES; ++v) {
		for (int f = 0; f < FK_NUM_FX; ++f) {
			syn->voices[v]->fxchain[f].mb[0]->tuning_change = 1;
			syn->voices[v]->fxchain[f].mb[1]->tuning_change = 1;
		}
	}
}

double fk__calculate_step_samples(double tempo, double samplerate)
{
	double one_bar_secs = 60.0 / (tempo * 0.25);
	double sixteenth_secs = one_bar_secs / 16.0;
	double sixteenth_samples = sixteenth_secs * samplerate;
	return sixteenth_samples / 32.0; // control is in 32th of an orca step (16th)
}

void fk__synth_sort_messages(fk_synth_t * syn)
{
	// selection sort
	// probably there will never be more than 64 elements to sort to it's fine
	// note: never use unsigned for the indices
	// or maxj - 1 would generate undefined behavior for zero size arrays
	int maxj = (int)syn->messages.size;
	int maxi = maxj - 1;
	int min_idx = 0;

	for (int i = 0; i < maxi; i++) {
		// find the minimum element in unsorted array
		min_idx = i;
		for (int j = i + 1; j < maxj; j++) {
			if (syn->messages.data[j]->frame < syn->messages.data[min_idx]->frame) {
				min_idx = j;
			}
		}
		// swap the found minimum element with the first element
		if (min_idx != i) {
			fk_osc_message_t * temp = syn->messages.data[i];
			syn->messages.data[i] = syn->messages.data[min_idx];
			syn->messages.data[min_idx] = temp;
		}
	}
}

void fk_synth_copy_messages(fk_synth_t * syn, unsigned buffersize)
{
	// MEMO for now there are no control on index overflow
	// the array should always be enough as its size is equal
	// to the max buffer for incoming messages + 1

	fk_osc_message_t * data = syn->osc_link->data;
	unsigned data_size = syn->osc_link->size;
	unsigned read = syn->osc_link->read;
	unsigned write = (unsigned)fk_atomic_load(&syn->osc_link->index);

	// 1 - subtract buffersize to all the messages and count message to delete
	unsigned size = syn->messages.size;
	unsigned delete_size = 0;
	for (unsigned n = 0; n < size; ++n) {
		int f = (int)syn->messages.data[n]->frame - (int)buffersize;
		if (f < 0) {
			delete_size++;
			f = 0; // to avoid mapping a negative number to unsigned later
		}
		syn->messages.data[n]->frame = (unsigned)f;
	}

	// 2 - move the message to process to the left overwriting processed messages
	if (delete_size != 0) { //
		size -= delete_size;

		for (unsigned n = 0; n < size; ++n) {
			syn->messages.data[n] = syn->messages.data[n + delete_size];
		}
	}

	// 3 - copy new messages with the right scheduled frame, updates frame delay parameters
	while (read != write) {
		read++;
		if (read == data_size) {
			read = 0;
		}

		switch (data[read].address) {
		case orca_w:
			if (data[read].args > 0) {
				double off = (double)data[read].values[0];
				syn->messages.offset = (unsigned)(syn->messages.step_samples * off);
			}
			if (data[read].args == 2) {
				int v = data[read].values[1];
				if (v > orca_v) {
					v = orca_v;
				}
				fk_voice_clear(syn->voices[v]);
			}
			if (data[read].args >= 3) {
				int v0 = data[read].values[1];
				int v1 = data[read].values[2];
				if (v0 > orca_v) {
					v0 = orca_v;
				}
				if (v1 > orca_v) {
					v1 = orca_v;
				}
				for (int i = v0; i <= v1; ++i) {
					fk_voice_clear(syn->voices[i]);
				}
			}
			break;

		case orca_tempo_change:
			if (data[read].args > 0) { // this will fall through
				double tempo = (double)data[read].values[0];
				double sr = syn->parameters->samplerate;
				syn->messages.step_samples = fk__calculate_step_samples(tempo, sr);
				syn->indata.step_samples = syn->messages.step_samples * 32.0;
			}
		default: // it's okay to fall through
			// copy the pointer to the queue
			syn->messages.data[size] = data + read;

			double delta = fk_osc_buffer_delta(syn->osc_link, data[read].timepoint);
			unsigned frame = (unsigned)(delta * (double)buffersize);

			// MEMO for SIMD
			// frame = frame / 8;
			// frame *= 8;

			if (frame > buffersize) {
				frame = buffersize;
			}
			frame += syn->messages.offset;
			syn->messages.data[size]->frame = frame;

			size++; // size can't overflow because there is always enough space in the buffer
			break;
		}
	}

	syn->osc_link->read = read;
	syn->messages.size = size;

	// 4 - reorder messages with a selection sort
	fk__synth_sort_messages(syn);
}

float * _fk_synth_get_duck_buffer(fk_synth_t * syn, unsigned v)
{
	for (unsigned i = 0; i < v; ++i) {
		for (int fx = 0; fx < FK_NUM_FX; ++fx) {
			if (syn->voices[i]->fxchain[fx].duck_voice == v) {
				return syn->voices[i]->fxchain[fx].env->buffer;
			}
		}
	}
	return NULL;
}

void fk_synth_process(fk_synth_t * syn, unsigned buffersize)
{
	fk_osc_buffer_lap(syn->osc_link);

	fk_synth_copy_messages(syn, buffersize);

	for (unsigned i = 0; i < syn->parameters->num_groups * 2; ++i) {
		dsp_set_zero(syn->buffers[i], buffersize);
	}

	for (unsigned i = 0; i < syn->num_outputs; ++i) {
		dsp_set_zero(syn->outputs[i], buffersize);
	}

	fk_osc_message_t ** data = syn->messages.data;
	unsigned size = syn->messages.size;
	unsigned stop_frame = 0;
	unsigned len = 0;
	unsigned start_frames[NUMVOICES];
	for (unsigned i = 0; i < NUMVOICES; ++i) {
		start_frames[i] = 0;
	}

	for (unsigned m = 0; m < size; ++m) {
		unsigned v = data[m]->address;
		if (data[m]->frame >= buffersize) {
			break;
		} // just process for the current buffersize

		switch (v) {
		case orca_w:
			break;
		case orca_x: // temperated tuning
			fk_set_mt_tuning(syn->tuning_link, data[m]->args, data[m]->values);
			modal_tune_change(syn);
			break;
		case orca_y: // pure tuning
			fk_set_pure_tuning(syn->tuning_link, data[m]->args, data[m]->values);
			modal_tune_change(syn);
			break;
		case orca_z: // pure with numerators / denominators
			if (data[m]->args > 3) {
				fk_tuning_pure_couples(syn->tuning_link, (int)data[m]->args, data[m]->values);
				modal_tune_change(syn);
			}
			break;

		case orca_tempo_change:
			// TODO: those values could be calculated once
			// and then applied to all the voices

			// change timing to all the clocked delays
			for (unsigned i = 0; i < NUMVOICES; ++i) {
				for (int f = 0; f < FK_NUM_FX; ++f) {
					dsp_delay_set_tempo(syn->voices[i]->fxchain[f].delay[0], data[m]->values[0]);
					dsp_delay_set_tempo(syn->voices[i]->fxchain[f].delay[1], data[m]->values[0]);
					dsp_delay_update_sync(syn->voices[i]->fxchain[f].delay[0]);
					dsp_delay_update_sync(syn->voices[i]->fxchain[f].delay[1]);
				}
			}

			// change tempo to all the lfo
			for (unsigned i = 0; i < NUMVOICES; ++i) {
				for (int f = 0; f < FK_NUM_FX; ++f) {
					dsp_lfo_set_tempo(syn->voices[i]->fxchain[f].lfo, data[m]->values[0]);
				}
			}
			break;

		default: { // trigger voices
			stop_frame = data[m]->frame;
			if (stop_frame >= buffersize)
				stop_frame = buffersize - 1;

			len = stop_frame - start_frames[v];

			fk_voice_process(syn->voices[v], start_frames[v], len);
			fk_voice_trigger(syn->voices[v], data[m]->args, data[m]->values,
			                 syn->indata.ci + start_frames[v], syn->indata.step_samples);
			start_frames[v] = stop_frame;
		} break;
		}
	}

	// process remaining buffer frames
	for (unsigned v = 0; v < NUMVOICES; ++v) {
		len = buffersize - start_frames[v];
		fk_voice_process(syn->voices[v], start_frames[v], len);
	}

	// duck voices and add to groups
	for (unsigned v = 0; v < NUMVOICES; ++v) {
		float * duck = _fk_synth_get_duck_buffer(syn, v);
		if (duck != NULL) {
			dsp_a_eq_b_ducks_a(syn->voices[v]->outputs[0], duck, len);
			dsp_a_eq_b_ducks_a(syn->voices[v]->outputs[1], duck, len);
		}

		unsigned slot = syn->parameters->voice_group[v];
		len = buffersize;
		if (syn->voices[v]->enabled) {
			dsp_a_eq_a_plus_b(syn->buffers[slot * 2], syn->voices[v]->outputs[0], len);
			dsp_a_eq_a_plus_b(syn->buffers[slot * 2 + 1], syn->voices[v]->outputs[1], len);
		}
	}

	// clipping groups
	unsigned ji = 2;
	for (unsigned g = 0; g < syn->parameters->num_groups; ++g) {
		unsigned l = g * 2;
		unsigned r = g * 2 + 1;

		if (syn->parameters->groups[g]->highpass != 0.0f) {
			dsp_lowcut_process(syn->parameters->groups[g]->hpf_l,
			                   syn->buffers[l], buffersize);
			dsp_lowcut_process(syn->parameters->groups[g]->hpf_r,
			                   syn->buffers[r], buffersize);
		}

		float gclip = syn->parameters->groups[g]->clip_threshold;
		float ampin = syn->parameters->groups[g]->amp_input;
		if (gclip != 0.0f) {
			dsp_a_eq_bxs(syn->buffers[l], syn->buffers[l], ampin, buffersize);
			dsp_softclip(syn->buffers[l], gclip, buffersize);
			dsp_a_eq_bxs(syn->buffers[r], syn->buffers[r], ampin, buffersize);
			dsp_softclip(syn->buffers[r], gclip, buffersize);
		}
		float ampout = syn->parameters->groups[g]->amp_output;
		dsp_a_eq_bxs(syn->buffers[l], syn->buffers[l], ampout, buffersize);
		dsp_a_eq_bxs(syn->buffers[r], syn->buffers[r], ampout, buffersize);

		// copy for extra jack sends
		if (syn->parameters->groups[g]->use_jack_channels) {
			dsp_a_eq_b(syn->outputs[ji], syn->buffers[l], buffersize);
			dsp_a_eq_b(syn->outputs[ji + 1], syn->buffers[r], buffersize);
			ji += 2;
		}
	}

	// sends and reverb
	if (syn->rev[0] != NULL) {
		float send_mult = syn->parameters->rev_send_mult;
		dsp_fft_reverb_clear_input(syn->rev[0]);
		dsp_fft_reverb_clear_input(syn->rev[1]);
		for (unsigned i = 0; i < syn->parameters->num_groups; ++i) {
			float send = syn->parameters->groups[i]->rev_send * send_mult;
			dsp_a_eq_a_plus_bxs(syn->rev[0]->buffer,
			                    syn->buffers[i * 2], send, buffersize);
			dsp_a_eq_a_plus_bxs(syn->rev[1]->buffer,
			                    syn->buffers[i * 2 + 1], send, buffersize);
		}

		dsp_fft_reverb_render(syn->rev[0]);
		dsp_fft_reverb_render(syn->rev[1]);

		if (syn->parameters->reverb_use_jack) {
			dsp_a_eq_b(syn->outputs[ji], syn->rev[0]->buffer, buffersize);
			dsp_a_eq_b(syn->outputs[ji + 1], syn->rev[1]->buffer, buffersize);
		}
	} else if (syn->parameters->reverb_use_jack) {
		float send_mult = syn->parameters->rev_send_mult;
		dsp_set_zero(syn->outputs[ji], buffersize);
		dsp_set_zero(syn->outputs[ji + 1], buffersize);
		for (unsigned i = 0; i < syn->parameters->num_groups; ++i) {
			float send = syn->parameters->groups[i]->rev_send * send_mult;
			dsp_a_eq_a_plus_bxs(syn->outputs[ji],
			                    syn->buffers[i * 2], send, buffersize);
			dsp_a_eq_a_plus_bxs(syn->outputs[ji + 1],
			                    syn->buffers[i * 2 + 1], send, buffersize);
		}
	}

	// summing to master bus
	for (unsigned i = 0; i < syn->parameters->num_groups; ++i) {
		dsp_a_eq_a_plus_b(syn->outputs[0], syn->buffers[i * 2], buffersize);
		dsp_a_eq_a_plus_b(syn->outputs[1], syn->buffers[i * 2 + 1], buffersize);
	}

	// add reverb just to first stereo outputs
	if (syn->rev[0] != NULL) {
		dsp_a_eq_a_plus_b(syn->outputs[0], syn->rev[0]->buffer, buffersize);
		dsp_a_eq_a_plus_b(syn->outputs[1], syn->rev[1]->buffer, buffersize);
	}

	// clipping master bus
	float mclip = syn->parameters->master_clip;
	float mamp = syn->parameters->master_amp;

	if (mclip != 0.0f) {
		for (unsigned i = 0; i < syn->num_outputs; ++i) {
			dsp_a_eq_bxs(syn->outputs[i], syn->outputs[i], mamp, buffersize);
			dsp_softclip(syn->outputs[i], mclip, buffersize);
		}
	}
}
