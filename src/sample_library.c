#include "sample_library.h"

#define CUTE_FILES_IMPLEMENTATION
#include "3rdp/cute_files.h"

#define DR_WAV_IMPLEMENTATION
#include "3rdp/dr_wav.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dsp/functions.h"
#include "dsp/rand.h"
#include "flags.h"

static void fk_iterate_subfolder(fk_sample_folder_t ** library, unsigned k, const char * path, unsigned max_wave_length);

static fk_sample_folder_t * fk_sample_folder_create(const char * path, unsigned max_wave_length);
static void fk_sample_folder_destroy(fk_sample_folder_t * subfolder);

static int fk_load_sample_data(fk_sample_t * sample, const char * path, unsigned max_wave_length, float amp);

static const char * valid_names[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

static unsigned fk__loaded_samples = 0;

void fk_report_loaded_samples(void)
{
	printf(" (%d samples loaded)\n", fk__loaded_samples);
}

fk_sample_folder_t ** fk_library_create(const char * path, unsigned max_wave_length, int input_glyph)
{
	fk_sample_folder_t ** library = malloc(sizeof(fk_sample_folder_t) * FKIT_MAXFOLDERS_SQUARED);
	if (library == NULL) {
		printf("memory error allocating library\n");
		goto error;
	}

	for (unsigned i = 0; i < FKIT_MAXFOLDERS_SQUARED; ++i) {
		library[i] = NULL;
	}

	printf("loading samples ");

	cf_dir_t dir;
	cf_dir_open(&dir, path);

	while (dir.has_next) {
		cf_file_t file;
		cf_read_file(&dir, &file);

		for (unsigned i = 1; i < FKIT_MAXFOLDERS; ++i) { // 0 excluded
			if (file.is_dir && strcmp(file.name, valid_names[i]) == 0) {
				if (i != (unsigned)input_glyph) {
					fk_iterate_subfolder(library, i, file.path, max_wave_length);
				} else {
					printf("warning, letter %s assigned to audio input\n", valid_names[i]);
				}
			}
		}
		cf_dir_next(&dir);
	}
	cf_dir_close(&dir);

	// printf("\n");

	return library;

error:
	return NULL;
}

void fk_iterate_subfolder(fk_sample_folder_t ** library, unsigned k, const char * path, unsigned max_wave_length)
{
	cf_dir_t dir;
	cf_dir_open(&dir, path);

	while (dir.has_next) {
		cf_file_t file;
		cf_read_file(&dir, &file);

		for (unsigned i = 0; i < FKIT_MAXFOLDERS; ++i) {
			if (file.is_dir && strcmp(file.name, valid_names[i]) == 0) {
				unsigned slot = k * FKIT_MAXFOLDERS + i;
				library[slot] = fk_sample_folder_create(file.path, max_wave_length);
			}
			// TODO : add folders with single files with 0.wav a.wav etc
		}
		cf_dir_next(&dir);
	}

	cf_dir_close(&dir);
}

fk_sample_folder_t * fk_sample_folder_create(const char * path, unsigned max_wave_length)
{

	unsigned found_samples = 0;
	float amp = 1.0f;

	cf_dir_t dir;
	cf_dir_open(&dir, path);
	while (dir.has_next) {
		cf_file_t file;
		cf_read_file(&dir, &file);
		if (cf_match_ext(&file, ".wav")) {
			found_samples++;
		}
		if (cf_match_ext(&file, ".dB")) {
			double value = atof(file.name);
			amp = (float)dB(value);
		}
		cf_dir_next(&dir);
	}
	cf_dir_close(&dir);

	if (found_samples == 0) {
		printf("\nwarning: empty folder %s ignored\n", path);
		return NULL;
	}

	fk_sample_folder_t * folder = malloc(sizeof(fk_sample_folder_t));
	if (folder == NULL) {
		printf("memory error allocating folder for path %s\n", path);
		goto error;
	}
	folder->size = found_samples;
	folder->index = 0;
	folder->samples = malloc(sizeof(fk_sample_t) * folder->size);
	if (folder->samples == NULL) {
		printf("memory error allocating samples array for path %s\n", path);
		goto error;
	}

	for (unsigned i = 0; i < found_samples; ++i) {
		folder->samples[i].buffers[0] = NULL;
		folder->samples[i].buffers[1] = NULL;
		folder->samples[i].length = 0;
		folder->samples[i].channels = 0;
		folder->samples[i].mode = FKIT_MODE_ONESHOT;
		folder->samples[i].samplerate = 1.0;
	}

	int i = 0;
	cf_dir_open(&dir, path);
	while (dir.has_next) {
		cf_file_t file;
		cf_read_file(&dir, &file);
		if (cf_match_ext(&file, ".wav")) {
			// printf( "        loading file %s\n", file.name );
			fk_load_sample_data(folder->samples + i, file.path, max_wave_length, amp);
			if (folder->samples[i].channels == 2) {
				printf(":");
			} else {
				printf(".");
			}
			fflush(stdout);
			i++;
		}
		cf_dir_next(&dir);
	}
	cf_dir_close(&dir);

	return folder;
error:
	if (folder != NULL) {
		fk_sample_folder_destroy(folder);
	}
	return NULL;
}

static int fk__sample_get_mode(const char * path, unsigned sample_len, unsigned max_wave_len)
{
	int len = (int)strlen(path);
	int sp = 0; // slashpoint
	for (int i = len - 1; i >= 0; --i) {
		if (path[i] == '/') {
			sp = i;
			break;
		}
	}

	if (len - sp > 7) {
		if ((path[sp + 1] == 's' || path[sp + 1] == 'S') &&
		    (path[sp + 2] == 'a' || path[sp + 2] == 'A') &&
		    (path[sp + 3] == 'm' || path[sp + 3] == 'M') &&
		    (path[sp + 4] == 'p' || path[sp + 4] == 'P') &&
		    (path[sp + 5] == 'l' || path[sp + 5] == 'L') &&
		    (path[sp + 6] == 'e' || path[sp + 6] == 'E')) {
			return FKIT_MODE_ONESHOT;
		}
	}

	if (len - sp > 5) {
		if ((path[sp + 1] == 'l' || path[sp + 1] == 'L') &&
		    (path[sp + 2] == 'o' || path[sp + 2] == 'O') &&
		    (path[sp + 3] == 'o' || path[sp + 3] == 'O') &&
		    (path[sp + 4] == 'p' || path[sp + 4] == 'P')) {
			return FKIT_MODE_LOOP;
		}
		if ((path[sp + 1] == 'w' || path[sp + 1] == 'W') &&
		    (path[sp + 2] == 'a' || path[sp + 2] == 'A') &&
		    (path[sp + 3] == 'v' || path[sp + 3] == 'V') &&
		    (path[sp + 4] == 'e' || path[sp + 4] == 'E')) {
			return FKIT_MODE_WAVE;
		}
	}

	if (sample_len <= max_wave_len) {
		return FKIT_MODE_WAVE;
	}

	return FKIT_MODE_ONESHOT;
}

int fk_load_sample_data(fk_sample_t * sample, const char * path, unsigned max_wave_length, float amp)
{
	unsigned channels;
	unsigned samplerate;
	drwav_uint64 frame_count;

	float * handle = drwav_open_file_and_read_pcm_frames_f32(path, &channels, &samplerate, &frame_count, NULL);

	if (channels < 1) {
		printf("error, wrong sample channel count\n");
		drwav_free(handle, NULL);
		return 1;
	}

	if (handle == NULL) {
		printf("error while loading wav file\n");
	} else {
		sample->length = (unsigned)frame_count;
		sample->samplerate = (double)samplerate;
		sample->mode = fk__sample_get_mode(path, (unsigned)frame_count, max_wave_length);
		sample->channels = (unsigned)channels;

		for (unsigned c = 0; c < channels; c++) {
			if (sample->buffers[c] != NULL) {
				free(sample->buffers[c]);
			}
			sample->buffers[c] = malloc(sizeof(float) * (sample->length + 2));
			// 2 guard points for interpolation
			if (sample->buffers[c] == NULL) {
				printf("error while allocating mono sample buffer\n");
				drwav_free(handle, NULL);
				return 1;
			}
			for (unsigned n = 0; n < frame_count; ++n) {
				sample->buffers[c][n] = handle[c + n * channels];
				if (amp != 1.0f) {
					sample->buffers[c][n] *= amp;
				}
			}
			sample->buffers[c][frame_count] = 0.0f;     // first guard point
			sample->buffers[c][frame_count + 1] = 0.0f; // second guard point
		}

		fk__loaded_samples++;
	}

	drwav_free(handle, NULL);
	return 0;
}

void fk_library_destroy(fk_sample_folder_t ** library)
{
	if (library != NULL) {
		for (unsigned i = 0; i < FKIT_MAXFOLDERS_SQUARED; ++i) {
			fk_sample_folder_destroy(library[i]);
		}
		free(library);
	}
}

void fk_sample_folder_destroy(fk_sample_folder_t * folder)
{
	if (folder != NULL) {
		for (unsigned i = 0; i < folder->size; ++i) {
			for (unsigned c = 0; c < 2; ++c) {
				if (folder->samples[i].buffers[c] != NULL) {
					free(folder->samples[i].buffers[c]);
				}
			}
		}
		free(folder->samples);
		free(folder);
	}
}

fk_sample_t * fk_library_search(fk_sample_folder_t ** library, unsigned folder, unsigned subfolder)
{

	unsigned slot = folder * FKIT_MAXFOLDERS + subfolder;

	if (library[slot] == NULL) {
		return NULL;
	} else {
		fk_sample_folder_t * f = library[slot];
		unsigned i = f->index;
		unsigned max = f->size;

		switch (max) {
		case 1: // just one sample
			i = 0;
			break;
		case 2: // two samples, gives other
			i = !i;
			break;
		default: { // not repeating random robin
			unsigned r = i;
			while (i == r) {
				i = dsp_dice(max);
			}
		} break;
		}

		f->index = i;
		return f->samples + i;
	}

	return NULL;
}

fk_sample_t * fk_ir_create(const char * path, fk_parameters_t * config)
{
	fk_sample_t * impulses = malloc(sizeof(fk_sample_t));
	if (impulses == NULL) {
		printf("memory error allocating impulse responses sample struct\n");
		goto error;
	}
	impulses->buffers[0] = NULL;
	impulses->buffers[1] = NULL;
	impulses->length = 0;
	impulses->channels = 0;
	impulses->mode = FKIT_MODE_ONESHOT;
	impulses->samplerate = 1.0;

	char irpath[FKIT_MAXLINE];
	snprintf(irpath, FKIT_MAXLINE, "%s/ir", path);

	if (access(irpath, F_OK) != -1) {
		printf("loading /ir folder\n");
		cf_dir_t dir;
		cf_dir_open(&dir, irpath);

		int loaded = 0;
		while (dir.has_next) {
			cf_file_t file;
			cf_read_file(&dir, &file);
			if (cf_match_ext(&file, ".wav")) {
				if (!loaded) {
					if (fk_load_sample_data(impulses, file.path, config->max_wave_length, 1.0f) != 0) {
						printf("[IR] error loading IR data\n");
						goto error;
					}
					loaded = 1;
				}
			}
			if (cf_match_ext(&file, ".dB")) {
				double value = atof(file.name);
				value -= 12.0; // a little softer by default
				float revsend = (float)dB(value);
				config->rev_send_mult = revsend;
			}
			cf_dir_next(&dir);
		}
		cf_dir_close(&dir);
	} else {
		printf("no /ir folder found\n");
		free(impulses);
		return NULL;
	}

	return impulses;

error:
	fk_ir_destroy(impulses);
	return NULL;
}

void fk_ir_destroy(fk_sample_t * impulses)
{
	if (impulses != NULL) {
		for (unsigned c = 0; c < 2; ++c) {
			if (impulses->buffers[c] != NULL) {
				free(impulses->buffers[c]);
			}
		}
		free(impulses);
	}
}

// i has to be the right input channels, they start from 1
float * fk_get_input_sample(fk_sample_folder_t ** library, fk_parameters_t * config, unsigned i)
{
	unsigned slot = ((unsigned)config->input_glyph) * FKIT_MAXFOLDERS + i;
	fk_sample_folder_t * f = library[slot];
	return f->samples[0].buffers[0];
}

void fk_inputs_create(fk_sample_folder_t ** library, fk_parameters_t * config)
{
	unsigned channels = config->num_inputs;
	int glyph = config->input_glyph;

	if (glyph <= orca_z && channels > 0) {
		printf("creating buffers for %d audio inputs\n", channels);
		long buffersize = config->buffersize;
		long buffer_max = (long)(config->input_buffers_max_ms * (double)config->samplerate * 0.001);
		buffer_max = ((buffer_max / buffersize) * buffersize) + buffersize;

		for (unsigned i = 0; i < channels; ++i) {

			fk_sample_folder_t * folder = malloc(sizeof(fk_sample_folder_t));
			if (folder == NULL) {
				printf("memory error allocating folder for audio in %d\n", i);
				// goto error; // TODO add errors later
			}
			folder->size = 1;
			folder->index = 0;
			folder->samples = malloc(sizeof(fk_sample_t));
			if (folder->samples == NULL) {
				printf("memory error allocating samples array for audio in %d\n", i);
				// goto error;
			}
			folder->samples[0].mode = FKIT_MODE_INPUT;
			folder->samples[0].samplerate = (double)config->samplerate;
			folder->samples[0].channels = 1;
			folder->samples[0].length = (unsigned)buffer_max;
			folder->samples[0].buffers[0] = malloc(sizeof(float) * (size_t)buffer_max);
			folder->samples[0].buffers[1] = NULL;

			for (int n = 0; n < buffer_max; ++n) { // clear buffer
				folder->samples[0].buffers[0][n] = 0.0f;
			}

			// assign channels starting from 1
			unsigned slot = ((unsigned)glyph) * FKIT_MAXFOLDERS + (i + 1);
			library[slot] = folder;
		}
	}
}
