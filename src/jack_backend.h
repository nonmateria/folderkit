#pragma once

#include "synthesizer.h"

int fk_jack_backend_open(fk_synth_t * synthesizer, fk_parameters_t * config, fk_osc_buffer_t * osc_buffer, fk_tuning_t * tuning, fk_sample_folder_t ** library, fk_sample_t * impulses);

int fk_jack_backend_close(void);
