
#include "osc_buffer.h"

#include "flags.h"
#include <lo/lo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SOKOL_IMPL
#include "3rdp/sokol_time.h"

fk_osc_buffer_t * fk_osc_buffer_create(unsigned size)
{
	fk_osc_buffer_t * buffer = malloc(sizeof(fk_osc_buffer_t));
	if (buffer == NULL) {
		printf("[error] error allocating fk_osc_buffer struct\n");
		goto error;
	}

	fk_atomic_store(&buffer->index, 0);
	buffer->read = 0;
	buffer->size = size;
	buffer->old_time = 0;
	buffer->new_time = 0;
	buffer->data = malloc(sizeof(fk_osc_message_t) * size);
	if (buffer->data == NULL) {
		printf("[error] error allocating fk_osc_buffer data array\n");
		goto error;
	}

	memset(buffer->data, 0, sizeof(fk_osc_message_t) * size);

	stm_setup(); // init timers

	return buffer;

error:
	fk_osc_buffer_destroy(buffer);
	return NULL;
}

void fk_osc_buffer_destroy(fk_osc_buffer_t * osc_buffer)
{
	if (osc_buffer != NULL) {
		if (osc_buffer->data != NULL) {
			free(osc_buffer->data);
		}
		free(osc_buffer);
	}
}

int fk_osc_handler(const char * path, const char * types, lo_arg ** argv,
                   int argc, void * data, void * user_data)
{
	fk_osc_buffer_t * osc_buffer = (fk_osc_buffer_t *)user_data;

	if (strlen(path) == 2) { // osc from orca
		unsigned address = 0;
		char c = path[1];
		if (c >= '0' && c <= '9') {
			address = (unsigned)(c - '0');
		} else if (c >= 'a' && c <= 'z') {
			address = (unsigned)(c - 'a' + 10);
		}

		// printf("   got message from orca, num = %d\n", address);
		int w = fk_atomic_load(&osc_buffer->index);
		w++;
		if (w == (int)osc_buffer->size)
			w = 0;

		osc_buffer->data[w].address = address;
		osc_buffer->data[w].timepoint = stm_now();
		osc_buffer->data[w].frame = 0; // to be assigned later

		osc_buffer->data[w].args = (unsigned)argc;
		for (int i = 0; i < argc; ++i) {
			osc_buffer->data[w].values[i] = argv[i]->i;
		}
		for (int i = argc; i < FK_OSC_ARGS; ++i) {
			osc_buffer->data[w].values[i] = 0;
		}

		fk_atomic_store(&osc_buffer->index, w);
	} else if (strcmp(path, "/orca/bpm") == 0) {
		int w = fk_atomic_load(&osc_buffer->index);
		w++;
		if (w == (int)osc_buffer->size)
			w = 0;

		osc_buffer->data[w].address = orca_tempo_change;
		osc_buffer->data[w].timepoint = stm_now();
		osc_buffer->data[w].frame = 0; // to be assigned later

		osc_buffer->data[w].args = 1;
		osc_buffer->data[w].values[0] = argv[0]->i;

		for (int i = 1; i < FK_OSC_ARGS; ++i) {
			osc_buffer->data[w].values[i] = 0;
		}

		fk_atomic_store(&osc_buffer->index, w);
	}

	(void)data;
	(void)types;
	return 0;
}

void fk_osc_error(int num, const char * msg, const char * path)
{
	printf("liblo server error %d in path %s: %s\n", num, path, msg);
	fflush(stdout);
}

void fk_osc_buffer_lap(fk_osc_buffer_t * osc_buffer)
{
	osc_buffer->old_time = osc_buffer->new_time;
	osc_buffer->new_time = stm_now();
}

/* the timepoint should be between the old memorized time and the new
 * the first check is for unsigned integer overflow, will just give no offset
 * it means the events would be aligned to the start of the audio buffer
 * just for one buffer, very rarely
 */
double fk_osc_buffer_delta(fk_osc_buffer_t * osc_buffer, uint64_t timepoint)
{
	if (osc_buffer->old_time < osc_buffer->new_time) {
		double off = (double)(timepoint - osc_buffer->old_time);
		double range = (double)(osc_buffer->new_time - osc_buffer->old_time);
		return off / range;
	} else {
		return 0.0;
	}
}

void fk_wait(unsigned ms)
{
	uint64_t start = stm_now();
	double elapsed = 0.0;
	double max = (double)ms;
	while (elapsed < max) {
		elapsed = stm_ms(stm_since(start));
	}
}
