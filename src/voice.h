#pragma once

#include "parameters_init.h"
#include "sample_library.h"
#include "tuning.h"

#include "dsp/decimator.h"
#include "dsp/delay.h"
#include "dsp/drift.h"
#include "dsp/envelope.h"
#include "dsp/formant.h"
#include "dsp/lfo.h"
#include "dsp/modal.h"
#include "dsp/operator.h"
#include "dsp/phaser.h"
#include "dsp/ringmod.h"
#include "dsp/sampler.h"
#include "dsp/saturator.h"
#include "dsp/silence.h"
#include "dsp/slew.h"
#include "dsp/va_filter.h"
#include "dsp/wavefolder.h"
#include "dsp/waveloss.h"

#define NO_DUCKING 44

#define FK_NUM_FX 6
typedef struct fk_effect_t {
	int fx_mode;
	unsigned duck_voice;
	float oct;
	float transpose;
	float max_freq;
	float mod_amt;

	dsp_envelope_t * env;
	dsp_lfo_t * lfo;
	dsp_slew_t * slew;

	dsp_vafilter_t * vaf[2];
	dsp_decimator_t * decimator[2];
	dsp_ringmod_t * rm[2];
	dsp_saturator_t * sat[2];
	dsp_operator_t * op[2];
	dsp_phaser_t * pha[2];
	dsp_formant_t * vow[2];
	dsp_delay_t * delay[2];
	dsp_modal_t * mb[2];
	dsp_wavefolder_t * wf[2];
	dsp_waveloss_t * wl[2];
} fk_effect_t;

typedef struct fk_voice_t {
	unsigned current_sampler;
	int enabled;
	float pans[2];

	float * outputs[2];
	float * freq_buffer;
	float * dummy_mod;

	dsp_sampler_t * samplers[2];
	fk_effect_t fxchain[FK_NUM_FX];
	unsigned used_fx;

	dsp_silence_t * sil;
	dsp_drift_t * drift;
	dsp_slew_t * slew_smp;
	float default_slew;
	float default_lfo_slew;

	fk_tuning_t * tuning_link;
	fk_sample_folder_t ** library_link;
	fk_parameters_t * parameters_link;
} fk_voice_t;

fk_voice_t * fk_voice_create(fk_parameters_t * config);

void fk_voice_destroy(fk_voice_t * voice);

void fk_voice_trigger(fk_voice_t * voice, unsigned argc, int * argv, long input_ci, double input_step);
void fk_voice_process(fk_voice_t * voice, unsigned start, unsigned buffersize);

void fk_voice_clear(fk_voice_t * voice);
