#include "tuning.h"

#include <math.h>
#include <stdio.h>

static void fk__set_octaves(fk_tuning_t * tuning, double pitch)
{
	double base = pow(2.0, (pitch - 69.0) / 12.0) * tuning->reference;
	tuning->oct[0] = base;

	for (unsigned i = 1; i < ORCA_GLYPHS_SIZE; ++i) {
		tuning->oct[i] = tuning->oct[i - 1] * 2.0;
	}

	tuning->oct[orca_z] = base * 0.5;
	tuning->oct[orca_y] = base * 0.25;
	tuning->oct[orca_x] = base * 0.125;
}

void fk_tuning_init(fk_tuning_t * tuning, double reference)
{
	tuning->reference = reference;
	fk_tuning_mt_default(tuning, 24, 0, 12);
}

void fk_tuning_split(fk_tuning_t * tuning, int split, int base)
{
	fk_tuning_mt_default(tuning, split, base, 12);
}

void fk_set_mt_tuning(fk_tuning_t * tuning, unsigned argc, int * argv)
{
	switch (argc) {
	case 0:
		break;
	case 1:
		fk_tuning_split(tuning, argv[0], 0);
		break;
	case 2:
		fk_tuning_split(tuning, argv[0], argv[1]);
		break;
	case 3:
	case 4:
	case 5: {
		int div = argv[2];
		if (div == 0) {
			div = 36;
		} // 0 means 36
		fk_tuning_mt_default(tuning, argv[0], argv[1], div);
		break;
	}
	default:
		fk_tuning_mt_indices(tuning, (int)argc, argv);
		break;
	}
}

void fk_set_pure_tuning(fk_tuning_t * tuning, unsigned argc, int * argv)
{
	if (argc >= 5) {
		fk_tuning_pure_indices(tuning, (int)argc, argv);
	} else if (argc >= 3) {
		int div = argv[2];
		if (div == 0) {
			div = 36;
		} // 0 means 36
		fk_tuning_pure_default(tuning, argv[0], argv[1], div);
	}
}

// TODO redo this better with function pointers

void fk_tuning_mt_indices(fk_tuning_t * tuning, int argc, int * argv)
{
	tuning->degrees = argc - 3;
	fk__set_octaves(tuning, (double)(argv[1] + 12));

	int division = argv[2];
	if (division == 0) {
		division = 36; // use zero for tempered 36
	}

	unsigned split = (unsigned)argv[0];
	double octmult;

	int degree = 0;

	if (split != 0) {
		degree = argc - 1;
		octmult = 0.5;
		for (unsigned i = 35; i >= split; --i) {
			if (argv[degree] == 0) {
				tuning->xpose[i] = octmult;
			} else {
				double note = pow(2.0, (double)argv[degree] / (double)division);
				tuning->xpose[i] = octmult * note;
			}
			degree--;
			if (degree == 2) {
				degree = argc - 1;
				octmult *= 0.5;
			}
		}
	}

	octmult = 1.0;
	degree = 3;
	unsigned max = 36;
	if (split != 0) {
		max = split;
	}

	for (unsigned i = 0; i < max; ++i) {
		if (argv[degree] == 0) {
			tuning->xpose[i] = octmult;
		} else {
			double note = pow(2.0, (double)argv[degree] / (double)division);
			tuning->xpose[i] = octmult * note;
		}

		degree++;
		if (degree == argc) {
			degree = 3;
			octmult *= 2.0;
		}
	}
}

void fk_tuning_pure_indices(fk_tuning_t * tuning, int argc, int * argv)
{
	tuning->degrees = argc - 3;
	fk__set_octaves(tuning, (double)(argv[1] + 12));

	int division = argv[2];
	if (division == 0) {
		division = 36; // use 0 for a value of 36
	}

	unsigned split = (unsigned)argv[0];
	double octmult;

	int degree = 0;

	if (split != 0) {
		degree = argc - 1;
		octmult = 0.5;
		for (unsigned i = 35; i >= split; --i) {
			double grade = (double)(argv[degree] + division) / (double)division;
			tuning->xpose[i] = octmult * grade;
			degree--;
			if (degree == 2) {
				degree = argc - 1;
				octmult *= 0.5;
			}
		}
	}

	octmult = 1.0;
	degree = 3;
	unsigned max = 36;
	if (split != 0) {
		max = split;
	}

	for (unsigned i = 0; i < max; ++i) {
		double grade = (double)(argv[degree] + division) / (double)division;
		tuning->xpose[i] = octmult * grade;
		degree++;
		if (degree == argc) {
			degree = 3;
			octmult *= 2.0;
		}
	}
}

// TODO redo this better with function pointers

void fk_tuning_mt_default(fk_tuning_t * tuning, int split, int base, int division)
{
	fk__set_octaves(tuning, (double)(base + 12));
	if (division == 0) {
		division = 36; // use 0 for a value of 36
	}
	tuning->degrees = division;

	double octmult;
	int degree = 0;

	if (split != 0) {
		degree = division - 1;
		octmult = 0.5;
		for (int i = 35; i >= split; --i) {
			if (degree == 0) {
				tuning->xpose[i] = octmult;
			} else {
				double note = pow(2.0, (double)degree / (double)division);
				tuning->xpose[i] = octmult * note;
			}
			degree--;
			if (degree == -1) {
				degree = division - 1;
				octmult *= 0.5;
			}
		}
	}

	octmult = 1.0;
	degree = 0;
	int max = 36;
	if (split != 0) {
		max = split;
	}

	for (int i = 0; i < max; ++i) {
		if (degree == 0) {
			tuning->xpose[i] = octmult;
		} else {
			double note = pow(2.0, (double)degree / (double)division);
			tuning->xpose[i] = octmult * note;
		}

		degree++;
		if (degree == division) {
			degree = 0;
			octmult *= 2.0;
		}
	}
}

void fk_tuning_pure_default(fk_tuning_t * tuning, int split, int base, int division)
{
	fk__set_octaves(tuning, (double)(base + 12));
	if (division == 0) {
		division = 36; // use 0 for a value of 36
	}
	tuning->degrees = division;

	double octmult;
	int degree = 0;

	if (split != 0) {
		degree = division - 1;
		octmult = 0.5;
		for (int i = 35; i >= split; --i) {
			if (degree == 0) {
				tuning->xpose[i] = octmult;
			} else {
				double grade = (double)(degree + division) / (double)division;
				tuning->xpose[i] = octmult * grade;
			}
			degree--;
			if (degree == -1) {
				degree = division - 1;
				octmult *= 0.5;
			}
		}
	}

	octmult = 1.0;
	degree = 0;
	int max = 36;
	if (split != 0) {
		max = split;
	}

	for (int i = 0; i < max; ++i) {
		if (degree == 0) {
			tuning->xpose[i] = octmult;
		} else {
			double grade = (double)(degree + division) / (double)division;
			tuning->xpose[i] = octmult * grade;
		}

		degree++;
		if (degree == division) {
			degree = 0;
			octmult *= 2.0;
		}
	}
}

void fk_tuning_pure_couples(fk_tuning_t * tuning, int argc, int * argv)
{
	fk__set_octaves(tuning, (double)(argv[1] + 12));

	unsigned split = (unsigned)argv[0];
	double octmult;
	int degree;
	int maxdegree = argc;
	if (maxdegree % 2 == 0) {
		maxdegree--;
	}
	tuning->degrees = (maxdegree - 2) / 2;

	if (split != 0) {
		degree = (int)maxdegree - 2;
		octmult = 0.5;
		for (unsigned i = 35; i >= split; --i) {
			int div = argv[degree + 1];
			if (div == 0) {
				div = 36;
			} // divisor of 0 means 36 for tuning
			double grade = (double)(argv[degree] + div) / (double)div;
			tuning->xpose[i] = octmult * grade;

			degree -= 2;
			if (degree < 2) {
				degree = (int)maxdegree - 2;
				octmult *= 0.5;
			}
		}
	}

	octmult = 1.0;
	degree = 2;
	unsigned max = 36;
	if (split != 0) {
		max = split;
	}

	for (unsigned i = 0; i < max; ++i) {
		int div = argv[degree + 1];
		if (div == 0) {
			div = 36;
		} // divisor of 0 means 36 for tuning
		double grade = (double)(argv[degree] + div) / (double)div;
		tuning->xpose[i] = octmult * grade;

		degree += 2;
		if (degree >= maxdegree) {
			degree = 2;
			octmult *= 2.0;
		}
	}
}
