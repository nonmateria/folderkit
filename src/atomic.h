
/* TODO:
 * rework this again checking better all the possible options and compiler flags
 */

#pragma once

#if __STDC_VERSION__ >= 201112L && !defined(__STDC_NO_ATOMICS__)

#include <stdatomic.h>

typedef _Atomic int fk_atomic_int_t;

static inline void fk_atomic_store(fk_atomic_int_t * a, int value)
{
	atomic_store(a, value);
}

static inline int fk_atomic_load(fk_atomic_int_t * a)
{
	return atomic_load(a);
}

#elif defined(__linux__)

#include <signal.h>

typedef sig_atomic_t fk_atomic_int_t;

static inline void fk_atomic_store(fk_atomic_int_t * a, int value)
{
	*a = value;
}

static inline int fk_atomic_load(fk_atomic_int_t * a)
{
	return *a;
}

#else

typedef int fk_atomic_int_t;

static inline void fk_atomic_store(fk_atomic_int_t * a, int value)
{
	*a = value;
}

static inline int fk_atomic_load(fk_atomic_int_t * a)
{
	return *a;
}

#endif
