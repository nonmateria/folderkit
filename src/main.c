
#include "dsp/fft_utils.h"
#include "dsp/rand.h"
#include "flags.h"
#include "jack_backend.h"
#include "osc_buffer.h"
#include "parameters_init.h"
#include "sample_library.h"
#include "synthesizer.h"
#include "tuning.h"
#include <lo/lo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int add_quit_handlers(void);
int running = 1;

int main(int argc, char * argv[])
{
	if (argc < 2 || argc > 3) {
		printf("usage: folderkit [path] [osc port]\n"
		       "                        (^ optional, defaults to 49162)\n");
		return 1;
	}
	// TODO : check argv[1]

	size_t len = strlen(argv[1]);
	if (len > FKIT_MAXLINE - 128) {
		printf("filepath too long, aborting...\n");
		return 1;
	}
	char noslash[FKIT_MAXLINE];
	memmove(noslash, argv[1], len + 1);
	if (noslash[len - 1] == '/') {
		noslash[len - 1] = '\0';
	}

	dsp_seed_random();

	// TODO: allocate this on the heap
	fk_parameters_t parameters;
	fk_parameters_init(&parameters, noslash);

	fk_tuning_t tuning;
	fk_tuning_init(&tuning, parameters.reference_freq);

	fk_sample_folder_t ** library = fk_library_create(noslash, parameters.max_wave_length,
	                                                  parameters.input_glyph);
	fk_report_loaded_samples();

	fk_sample_t * impulses = fk_ir_create(noslash, &parameters);

	int rc = 0;

	rc = add_quit_handlers();
	if (rc != 0)
		goto error;

	fk_osc_buffer_t * osc_buffer = fk_osc_buffer_create(2048);

	// TODO: allocate synth on the heap
	fk_synth_t synthesizer;
	rc = fk_jack_backend_open(&synthesizer, &parameters, osc_buffer, &tuning, library, impulses);
	if (rc != 0)
		goto error;

	printf("synth created and connected with JACK.\n");

	fk_wait(FK_WAIT_BEFORE_OSC);

	const char * default_port = "49162";
	const char * osc_port = NULL;
	if (argc == 3) {
		// check out that the argv[2] is a valid port
		int i = 0;
		while (argv[2][i] != '\0') {
			if (argv[2][i] < '0' || argv[2][i] > '9') {
				printf("%s is not a valid OSC port\n", argv[2]);
				goto error;
			}
			i++;
		}
		osc_port = argv[2];
	} else {
		osc_port = default_port;
	}

	lo_server receiver = lo_server_new(osc_port, fk_osc_error);
	lo_server_add_method(receiver, NULL, NULL, fk_osc_handler, osc_buffer);
	printf("waiting for messages on port %s...\n", osc_port);

	while (running) {
		lo_server_recv_noblock(receiver, 1000);
	}

	printf("cleaning...\n");
	fk_jack_backend_close();

	fk_synth_release(&synthesizer);

	fk_osc_buffer_destroy(osc_buffer);

	lo_server_free(receiver);

	fk_library_destroy(library);

	fk_parameters_release(&parameters);

	dsp_fft_cleanup();

	return 0;
error:
	exit(1);
}

// --------- signal handling -------------

void quit_handler(int signo)
{
	if (signo == SIGINT || signo == SIGTERM) {
		printf("\nreceived SIGINT or SIGTERM, quitting...\n");
		running = 0;
	}
}

int add_quit_handlers()
{
	if (signal(SIGINT, quit_handler) == SIG_ERR) {
		printf("error on assigning signal handler\n");
		return 2;
	}
	if (signal(SIGTERM, quit_handler) == SIG_ERR) {
		printf("error on assigning signal handler\n");
		return 2;
	}
	return 0;
}
