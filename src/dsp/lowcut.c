#include "lowcut.h"

#include "functions.h"

#include <stdio.h>
#include <stdlib.h>

dsp_lowcut_t * dsp_lowcut_create(void)
{

	dsp_lowcut_t * lc = malloc(sizeof(dsp_lowcut_t));
	if (lc == NULL) {
		printf("error allocatind lowcut_t\n");
		return NULL;
	}

	lc->p1_z1 = 0.0f;
	lc->p2_z1 = 0.0f;

	// 20 hz 44100hz DC blocker as default
	dsp_lowcut_set_cutoff(lc, 20.0, 44100.0);

	return lc;
}

void dsp_lowcut_destroy(dsp_lowcut_t * lc)
{
	if (lc != NULL) {
		free(lc);
	}
}

void dsp_lowcut_set_cutoff(dsp_lowcut_t * lc, double frequency, double samplerate)
{
	double fc = frequency / samplerate;

	double b1 = exp(-M_TWO_PI * fc);
	lc->a0 = (float)(1.0 - b1);
	lc->b1 = (float)b1;
}

void dsp_lowcut_process(dsp_lowcut_t * lc, float * duplex, unsigned buffersize)
{
	float a0 = lc->a0;
	float b1 = lc->b1;
	float p1_z1 = lc->p1_z1;
	float p2_z1 = lc->p2_z1;

	for (unsigned n = 0; n < buffersize; ++n) {
		// first pole
		float xn = duplex[n];
		float yn = xn * a0 + p1_z1 * b1;
		p1_z1 = yn;

		xn = xn - yn;

		// second pole
		yn = xn * a0 + p2_z1 * b1;
		p2_z1 = yn;

		duplex[n] = xn - yn;

		sanitize_float(&p1_z1);
		sanitize_float(&p2_z1);
	}

	lc->p1_z1 = p1_z1;
	lc->p2_z1 = p2_z1;
}

void dsp_lowcut_clear(dsp_lowcut_t * lc)
{
	lc->p1_z1 = 0.0f;
	lc->p2_z1 = 0.0f;
}
