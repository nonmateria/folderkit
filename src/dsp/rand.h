#pragma once

void dsp_seed_random(void);

float dsp_brand(void);

float dsp_urand(void);

unsigned dsp_dice(unsigned size);
