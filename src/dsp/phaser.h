#pragma once

typedef struct dsp_phaser_t {
	float x_z1[4];
	float y_z1[4];
	float fb_z1;
	float alpha_mul;
	float feedback;
	float spread;
	float maxfreq;
} dsp_phaser_t;

dsp_phaser_t * dsp_phaser_create(double samplerate);
void dsp_phaser_destroy(dsp_phaser_t * pha);

void dsp_phaser_process(dsp_phaser_t * pha, float * duplex, float * freq, unsigned buffersize);

void dsp_phaser_clear(dsp_phaser_t * pha);
