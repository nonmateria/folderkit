#pragma once

#define FK_MAX_RESONATORS 8

#include "tuning.h"

// made as struct of arrays for easy SIDM optimization (still TODO)
typedef struct dsp_modal_t {

	float z0[FK_MAX_RESONATORS];
	float z1[FK_MAX_RESONATORS];

	float b1[FK_MAX_RESONATORS];
	float a1[FK_MAX_RESONATORS];
	float a2[FK_MAX_RESONATORS];
	float s[FK_MAX_RESONATORS];

	float ratio[FK_MAX_RESONATORS];
	int rsize;

	float freq;
	int mode;
	float q;
	float sr;
	float scale;
	float dry;
	int tuning_change;
} dsp_modal_t;

dsp_modal_t * dsp_modal_create(unsigned int samplerate);

void dsp_modal_destroy(dsp_modal_t * mb);

void dsp_modal_set_coeff(dsp_modal_t * mb, float freq, int mode, float q, double * ratios, int degrees);

void dsp_modals_set_coeff(int num, dsp_modal_t ** mb, float freq, int mode, float q, double * ratios, int degrees);

void dsp_modal_process(dsp_modal_t * mb, float * duplex, unsigned buffersize);

void dsp_modal_clear(dsp_modal_t * mb);
