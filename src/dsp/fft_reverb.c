/*
 * thanks to hints from
 * By Steven W. Smith's DSP Guide
 * (chapter 18 on FFT Convolution)
 * http://www.dspguide.com/ch18/2.htm
 *
 * and thanks to the paper:
 * Implementing Real-Time Partitioned Convolution Algorithms
 * on Conventional Operating Systems
 * by Eric Battenberg, Rimas Avižienis
 * http://ericbattenberg.com/pdf/partconvDAFx2011.pdf
 */

#include "fft_reverb.h"

#include "buffer.h"
#include "functions.h"

#include <stdio.h>

dsp_fft_reverb_t * dsp_fft_reverb_create(float * sample_buffer, unsigned sample_length, double sample_samplerate, unsigned dsp_buffersize, double dsp_samplerate, double lowcut)
{
	dsp_fft_reverb_t * rev = malloc(sizeof(dsp_fft_reverb_t));
	if (rev == NULL) {
		printf("error allocating fft reverb!\n");
		goto error;
	}

	rev->buffersize = dsp_buffersize;
	rev->buffer = dsp_create_buffer(rev->buffersize);
	if (rev->buffer == NULL) {
		printf("error allocating fft reverb input buffer!\n");
		goto error;
	}
	dsp_set_zero(rev->buffer, dsp_buffersize);

	rev->lowcut = dsp_lowcut_create();
	dsp_lowcut_set_cutoff(rev->lowcut, lowcut, dsp_samplerate);

	unsigned irsample_len = 0;
	float * irsample = dsp_convert_sample_buffer(sample_buffer, sample_length,
	                                             sample_samplerate, dsp_samplerate,
	                                             &irsample_len, dsp_buffersize);

	rev->fft_conf = dsp_fft_create(dsp_buffersize);

	rev->num_blocks = (irsample_len / dsp_buffersize);

	rev->silence_counter = rev->num_blocks + 1;

	rev->block = 0;
	unsigned csize = dsp_get_fft_complex_size(rev->fft_conf);

	// TODO : optimize this by using monodimensional array instead of 2d
	// TODO : add memchecks for malloc
	rev->ir_re = malloc(sizeof(float *) * rev->num_blocks);
	rev->ir_im = malloc(sizeof(float *) * rev->num_blocks);
	for (unsigned i = 0; i < rev->num_blocks; ++i) {
		rev->ir_re[i] = dsp_create_buffer(csize);
		rev->ir_im[i] = dsp_create_buffer(csize);
		float * samplezone = irsample + (i * dsp_buffersize);
		dsp_fft(rev->fft_conf, samplezone, rev->ir_re[i], rev->ir_im[i]);
	}

	rev->fdl_re = malloc(sizeof(float *) * rev->num_blocks);
	rev->fdl_im = malloc(sizeof(float *) * rev->num_blocks);
	for (unsigned i = 0; i < rev->num_blocks; ++i) {
		rev->fdl_re[i] = dsp_create_buffer(csize);
		rev->fdl_im[i] = dsp_create_buffer(csize);
		dsp_set_zero(rev->fdl_re[i], csize);
		dsp_set_zero(rev->fdl_im[i], csize);
	}

	rev->now_re = dsp_create_buffer(csize);
	rev->now_im = dsp_create_buffer(csize);
	dsp_set_zero(rev->now_re, csize);
	dsp_set_zero(rev->now_im, csize);

	rev->fft_size = dsp_get_fft_real_size(rev->fft_conf);
	rev->fft_output = dsp_create_buffer(rev->fft_size);
	dsp_set_zero(rev->fft_output, rev->fft_size);

	free(irsample);

	return rev;

error:
	dsp_fft_reverb_destroy(rev);
	return NULL;
}

// -96 dB
#define SILENCE_EPSILON 1.5848931924611107e-05

void dsp_fft_reverb_render(dsp_fft_reverb_t * rev)
{
	unsigned blk = rev->block;
	unsigned max_blk = rev->num_blocks;
	unsigned csize = dsp_get_fft_complex_size(rev->fft_conf);
	unsigned buffersize = rev->buffersize;
	unsigned silence = rev->silence_counter;

	// input sanitizing -----------------------------
	/*
	 * someway very soft (inaudible) signals
	 * create HEAVY cpu spikes into the FFT reverb
	 * (probably in the complex multiply routine)
	 * this code sanitize the input to avoid this
	 */
	int loud = 0;
	for (unsigned n = 0; n < buffersize; ++n) {
		sanitize_float(rev->buffer + n);
		if (rev->buffer[n] > SILENCE_EPSILON) {
			loud = 1;
			break;
		}
	}
	if (loud) {
		silence = 0;
	} else {
		silence++;
		dsp_set_zero(rev->buffer, buffersize);
	}
	// -----------------------------------------------

	if (silence <= max_blk) {
		// put buffer into fdl
		dsp_fft(rev->fft_conf, rev->buffer, rev->fdl_re[blk], rev->fdl_im[blk]);

		// cleans complex buffer
		for (unsigned i = 0; i < csize; ++i) {
			rev->now_re[i] = 0.0f;
			rev->now_im[i] = 0.0f;
		}

		// complex multiply and add all the blocks, accumulate into rev->now
		for (unsigned i = 0; i < max_blk; ++i) {
			dsp_sum_complex_multiply(rev->now_re, rev->now_im,
			                         rev->fdl_re[blk], rev->fdl_im[blk],
			                         rev->ir_re[i], rev->ir_im[i], csize);
			blk++;
			if (blk == max_blk) {
				blk = 0;
			}
		}

		// put the tail of the old convolution into the buffersize
		for (unsigned n = 0; n < buffersize; ++n) {
			rev->buffer[n] = rev->fft_output[n + buffersize];
		}

		// convolve the new summed complex signal
		dsp_ifft(rev->fft_conf, rev->now_re, rev->now_im, rev->fft_output);

		// add the first half of the new convoluted output into the rev buffer
		for (unsigned n = 0; n < buffersize; ++n) {
			rev->buffer[n] += rev->fft_output[n];
		}

		// hp filter filters
		dsp_lowcut_process(rev->lowcut, rev->buffer, buffersize);

		// move block index
		if (rev->block == 0) {
			rev->block = max_blk - 1;
		} else {
			rev->block = rev->block - 1;
		}
	} else {
		dsp_set_zero(rev->buffer, buffersize);
	}

	rev->silence_counter = silence;
}

void dsp_fft_reverb_clear_input(dsp_fft_reverb_t * rev)
{
	dsp_set_zero(rev->buffer, rev->buffersize);
}

void dsp_fft_reverb_destroy(dsp_fft_reverb_t * rev)
{
	if (rev != NULL) {
		dsp_destroy_buffer(rev->buffer);
		dsp_destroy_buffer(rev->fft_output);
		dsp_destroy_buffer(rev->now_re);
		dsp_destroy_buffer(rev->now_im);

		if (rev->ir_re != NULL) {
			for (unsigned i = 0; i < rev->num_blocks; ++i) {
				dsp_destroy_buffer(rev->ir_re[i]);
			}
			free(rev->ir_re);
		}
		if (rev->ir_im != NULL) {
			for (unsigned i = 0; i < rev->num_blocks; ++i) {
				dsp_destroy_buffer(rev->ir_im[i]);
			}
			free(rev->ir_im);
		}
		if (rev->fdl_re != NULL) {
			for (unsigned i = 0; i < rev->num_blocks; ++i) {
				dsp_destroy_buffer(rev->fdl_re[i]);
			}
			free(rev->fdl_re);
		}
		if (rev->fdl_im != NULL) {
			for (unsigned i = 0; i < rev->num_blocks; ++i) {
				dsp_destroy_buffer(rev->fdl_im[i]);
			}
			free(rev->fdl_im);
		}
		if (rev->fft_conf != NULL) {
			dsp_fft_destroy(rev->fft_conf);
		}

		dsp_lowcut_destroy(rev->lowcut);

		free(rev);
	}
}
