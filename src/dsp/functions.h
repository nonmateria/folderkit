#pragma once

#include <math.h>

#include <stdint.h>

#ifndef M_TWO_PI
#define M_TWO_PI 6.2831853071795864769252867665590f
#endif

static inline double dB(double x)
{
	return pow(10.0, x * 0.05);
}

static inline float dBf(float x)
{
	return powf(10.0f, x * 0.05f);
}

// removes denormalized numbers and NaNs
// from Urs Heckmann, found on musicdsp.org archives
// https://www.musicdsp.org/en/latest/Other/191-cure-for-malicious-samples.html
static inline void sanitize_float(float * x)
{
	uint32_t * casted = (uint32_t *)x;
	uint32_t exponent = *casted & 0x7F800000;
	// exponent < 0x7F800000 is 0 if NaN or Infinity, otherwise 1
	// exponent > 0 is 0 if denormalized, otherwise 1
	uint32_t aNaN = exponent < 0x7F800000;
	uint32_t aDen = exponent > 0;
	uint32_t result = *casted * (aNaN & aDen);
	*casted = result;
}
