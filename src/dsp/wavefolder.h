#pragma once

typedef struct dsp_wavefolder_t {
	float amp;
	float trim;
	float feedback;
	float control;
	int mode;
	float z1;
} dsp_wavefolder_t;

dsp_wavefolder_t * dsp_wavefolder_create(void);
void dsp_wavefolder_destroy(dsp_wavefolder_t * wf);

void dsp_wavefolders_set(int num, dsp_wavefolder_t ** wf, int inputdb, float ctrl, int mode, int outputdb, float fb);

void dsp_wavefolder_process(dsp_wavefolder_t * wf, float * duplex, unsigned buffersize);
