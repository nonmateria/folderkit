#pragma once

typedef struct dsp_delay_t {
	int write;
	int size;
	float dry;
	float send;
	float send_slew;
	float feedback;
	float z1;
	float leak_a0;
	float leak_b1;
	float leak_z1;
	float read_offset;
	float read_offset_slew;
	float samplerate;
	float sync_16th;
	float sync_coeff;
	float mod_amt;
	float mod_mult;
	float mod_slew;
	float lfo_drift;
	float lfo_ph;
	float lfo_inc;

	unsigned wl_count;
	unsigned wl_actives;
	unsigned wl_max;
	int wl_mode;
	float wl_on;

	float * buffer;
} dsp_delay_t;

dsp_delay_t * dsp_delay_create(double samplerate, unsigned max_ms, float lfo_rate, float lfo_drift);

void dsp_delay_destroy(dsp_delay_t * del);

void dsp_delay_set_tempo(dsp_delay_t * del, int tempo);

void dsp_delay_set_sync(dsp_delay_t * del, float sixteenths);
void dsp_delay_update_sync(dsp_delay_t * del);

void dsp_delay_process_sync(dsp_delay_t * del, float * duplex, float * drift, unsigned buffersize);
void dsp_delay_process_wl(dsp_delay_t * del, float * duplex, unsigned buffersize);
void dsp_delay_process_tuned(dsp_delay_t * del, float * duplex, float * freq, unsigned buffersize);

void dsp_delays_set_params(int num, dsp_delay_t ** delays, float sixteenths, float fb, float dry, float send, float mod_amt);
void dsp_delays_wl_set_params(int num, dsp_delay_t ** delays, float sixteenths, float fb, float dry, float send, int ctrl);

void dsp_delay_clear(dsp_delay_t * del);
