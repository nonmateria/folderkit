#include "phaser.h"

#include <functions.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

dsp_phaser_t * dsp_phaser_create(double samplerate)
{
	dsp_phaser_t * pha = malloc(sizeof(dsp_phaser_t));
	if (pha == NULL) {
		printf("memory error while creating phaser\n");
		return NULL;
	}

	pha->alpha_mul = (float)((M_TWO_PI * 0.5) / samplerate);

	for (int i = 0; i < 4; ++i) {
		pha->x_z1[i] = 0.0f;
		pha->y_z1[i] = 0.0f;
	}
	pha->fb_z1 = 0.0f;
	pha->spread = 1.0f;
	pha->feedback = 0.0f;
	pha->maxfreq = (float)(samplerate * 0.5);

	return pha;
}

void dsp_phaser_destroy(dsp_phaser_t * pha)
{
	if (pha != NULL) {
		free(pha);
	}
}

void dsp_phaser_process(dsp_phaser_t * pha, float * duplex, float * freq, unsigned buffersize)
{
	float x_z1[4];
	float y_z1[4];
	float fb_z1 = pha->fb_z1;
	float amult = pha->alpha_mul;
	for (int i = 0; i < 4; ++i) {
		x_z1[i] = pha->x_z1[i];
		y_z1[i] = pha->y_z1[i];
	}
	float feedback = pha->feedback;
	float spread = pha->spread;
	float maxfreq = pha->maxfreq;

	float alpha[4];

	for (unsigned n = 0; n < buffersize; ++n) {

		for (int i = 0; i < 4; ++i) {
			float polefreq = freq[n] * (1.0f + spread * (float)i);
			if (polefreq >= maxfreq) {
				polefreq = maxfreq;
			}
			alpha[i] = tanf(amult * polefreq);
			alpha[i] = (alpha[i] - 1.0f) / (alpha[i] + 1.0f);
		}

		float xn = duplex[n] + feedback * fb_z1;
		float yn;

		for (int i = 0; i < 4; ++i) { // four stages
			yn = alpha[i] * (xn - y_z1[i]) + x_z1[i];
			y_z1[i] = yn;
			x_z1[i] = xn;
			xn = yn;
		}

		fb_z1 = yn;
		sanitize_float(&fb_z1);
		duplex[n] = yn;
	}

	pha->fb_z1 = fb_z1;
	for (int i = 0; i < 4; ++i) {
		pha->x_z1[i] = x_z1[i];
		pha->y_z1[i] = y_z1[i];
	}
}

void dsp_phaser_clear(dsp_phaser_t * pha)
{
	for (int i = 0; i < 4; ++i) {
		pha->x_z1[i] = 0.0f;
		pha->y_z1[i] = 0.0f;
	}
	pha->fb_z1 = 0.0f;
}
