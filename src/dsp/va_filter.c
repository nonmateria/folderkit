/*
 * implemented thanks to the W. Pirke book "Designing Software Synthesizer Plug-Ins in C++"
 * algorithm based on the paper "The Art of VA Filter Design" by Vadim Zavalishin
 * https://www.native-instruments.com/fileadmin/ni_media/downloads/pdf/VAFilterDesign_1.1.1.pdf
 */

#include "va_filter.h"

#include "functions.h"

#include <stdio.h>
#include <stdlib.h>

dsp_vafilter_t * dsp_filter_create(double samplerate)
{
	dsp_vafilter_t * vaf = malloc(sizeof(dsp_vafilter_t));
	if (vaf == NULL) {
		printf("error allocating va filter\n");
		return NULL;
	}
	vaf->z1_1 = 0.0f;
	vaf->z1_2 = 0.0f;
	vaf->z1_3 = 0.0f;
	vaf->z1_4 = 0.0f;
	vaf->reso = 0.0f; // reso should be in the 0.0f <--> 4.0f range
	double halft = 0.5 / samplerate;
	double twoslasht = 1.0 / halft;
	vaf->halft = (float)halft;
	vaf->twoslasht = (float)twoslasht;

	return vaf;
}

void dsp_filter_destroy(dsp_vafilter_t * vaf)
{
	if (vaf != NULL) {
		free(vaf);
	}
}

void dsp_filter_clear(dsp_vafilter_t * vaf)
{
	vaf->z1_1 = 0.0f;
	vaf->z1_2 = 0.0f;
	vaf->z1_3 = 0.0f;
	vaf->z1_4 = 0.0f;
}

// clang-format off
#define BEGIN_FILTER(TYPE)                                                 \
void dsp_filter_process_##TYPE(dsp_vafilter_t * vaf,                       \
                               float * duplex, const float * freq_buffer,  \
                               unsigned buffersize)                        \
{                                                                          \
                                                                           \
	float halft = vaf->halft;                                      \
	float twoslasht = vaf->twoslasht;                              \
	float K = vaf->reso;                                           \
	float z1_1 = vaf->z1_1;                                        \
	float z1_2 = vaf->z1_2;                                        \
	float z1_3 = vaf->z1_3;                                        \
	float z1_4 = vaf->z1_4;                                        \
                                                                              \
	for (unsigned n = 0; n < buffersize; ++n) {                    \
		float xn = duplex[n];                                  \
		float freq = freq_buffer[n];                           \
                                                                              \
		float wd = M_TWO_PI * freq;                            \
		float wa = twoslasht * tanf(wd * halft);               \
                                                                              \
		float g = wa * halft;                                  \
		float alpha = g / (1.0f + g);                          \
		float beta4 = 1.0f / (1.0f + g);                       \
		float beta3 = beta4 * alpha;                           \
		float beta2 = beta3 * alpha;                           \
		float beta1 = beta2 * alpha;                           \
		float gamma = alpha * alpha * alpha * alpha;           \
                                                                              \
		float alpha0 = 1.0f / (1.0f + (gamma * K * 4.0f));     \
                                                                              \
		float sigma = z1_1 * beta1 + z1_2 * beta2 +            \
		              z1_3 * beta3 + z1_4 * beta4;             \
                                                                              \
		float u = (xn - K * sigma) * alpha0;                   \
                                                                              \
		float vn = (u - z1_1) * alpha;                         \
		float lpf1 = vn + z1_1;                                \
		z1_1 = vn + lpf1;                                      \
                                                                              \
		vn = (lpf1 - z1_2) * alpha;                            \
		float lpf2 = vn + z1_2;                                \
		z1_2 = vn + lpf2;                                      \
                                                                              \
		vn = (lpf2 - z1_3) * alpha;                            \
		float lpf3 = vn + z1_3;                                \
		z1_3 = vn + lpf3;                                      \
                                                                              \
		vn = (lpf3 - z1_4) * alpha;                            \
		float lpf4 = vn + z1_4;                                \
		z1_4 = vn + lpf4;                                      \
																		     \
		sanitize_float( &z1_1 );                                               \
		sanitize_float( &z1_2 );                                               \
		sanitize_float( &z1_3 );                                               \
		sanitize_float( &z1_4 );                                              

#define END_FILTER                                                         \
	}                                                             		   \
	vaf->z1_1 = z1_1;                                                      \
	vaf->z1_2 = z1_2;                                                      \
	vaf->z1_3 = z1_3;                                                      \
	vaf->z1_4 = z1_4;                                                      \
}

// clang-format on 

BEGIN_FILTER(lpf2)
	duplex[n] = lpf2;
END_FILTER

BEGIN_FILTER(lpf4)
	duplex[n] = lpf4;
END_FILTER

BEGIN_FILTER(bpf2)
	duplex[n] = lpf1 * 2.0f + lpf2 * -2.0f;
END_FILTER

BEGIN_FILTER(bpf4)
	duplex[n] = lpf2 * 4.0f + lpf3 * -8.0f + lpf4 * 4.0f;
END_FILTER

BEGIN_FILTER(hpf2)
	duplex[n] = u + (lpf1 * -2.0f) + lpf2;
END_FILTER

BEGIN_FILTER(hpf4)
	duplex[n] = u + (lpf1 * -4.0f) + (lpf2 * 6.0f) + (lpf3 * -4.0f) + lpf4;
END_FILTER
