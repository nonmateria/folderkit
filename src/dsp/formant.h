#pragma once

// partially based on https://pbat.ch/sndkit/vowel/

typedef struct dsp_vowel_t {
	float freq[5];
	float amp[5];
	float q[5];
} dsp_vowel_t;

#define FK_VOW_PARTS 5
#define FK_VOW_VOCALS 5

#define PART_BASS 0
#define PART_TENOR 1
#define PART_CTENOR 2
#define PART_ALTO 3
#define PART_SOPRANO 4

#define VOCAL_A 0
#define VOCAL_E 1
#define VOCAL_I 2
#define VOCAL_O 3
#define VOCAL_U 4

typedef struct dsp_biquads_parameters_t {
	float b0[5];
	float b1[5];
	float b2[5];
	float a1[5];
	float a2[5];
	float x_z1[5];
	float x_z2[5];
	float y_z1[5];
	float y_z2[5];
} dsp_biquads_parameters_t;

typedef struct dsp_formant_t {
	dsp_biquads_parameters_t params;
	float samplerate;
	float master_gain;

	dsp_vowel_t vow_now;
	dsp_vowel_t vow_dest;

	// parameters for the slew
	float slew_a0;
	float slew_b1;

	// parameters for the lpf
	float lpf_a0;
	float lpf_b1;
	float lpf_z1;
	float lpf_z2;

	// 2 dimensional array in one dimension, parts is y, vocals is x
	dsp_vowel_t vow_library[FK_VOW_PARTS * FK_VOW_VOCALS];
} dsp_formant_t;

dsp_formant_t * dsp_formant_create(double samplerate);
void dsp_formant_destroy(dsp_formant_t * vow);

void dsp_formants_set(dsp_formant_t ** vows, int part, int vocal, int slew, float cutoff);

void dsp_formant_process(dsp_formant_t * vow, float * duplex, unsigned buffersize);

void dsp_formant_clear(dsp_formant_t * vow);
