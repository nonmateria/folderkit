#pragma once

typedef struct dsp_operator_t {
	float phase;
	float size;
	float oneslashsr;
	float op_gain;
	float input_gain;
	float modulator_amount;
	float self_amount;
	float amp_modulation;
	float z1;
	float * wave_link;
} dsp_operator_t;

dsp_operator_t * dsp_operator_create(float * table, unsigned size, double sop_gainlerate);

void dsp_operator_destroy(dsp_operator_t * op);

void dsp_operator_process(dsp_operator_t * op, float * duplex, const float * freq_buffer, const float * mod_buffer, unsigned buffersize);
