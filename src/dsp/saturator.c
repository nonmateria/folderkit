#include "saturator.h"

#include "functions.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

dsp_saturator_t * dsp_saturator_create(double samplerate)
{
	dsp_saturator_t * sat = malloc(sizeof(dsp_saturator_t));
	if (sat == NULL) {
		printf("error allocatind lowcut_t\n");
		return NULL;
	}

	sat->p1_z1 = 0.0f;
	sat->p2_z1 = 0.0f;

	sat->samplerate = samplerate;

	dsp_saturator_set_cutoff(sat, 22050.0);
	dsp_saturator_set_drive(sat, 0);
	dsp_saturator_set_output_gain(sat, 0);

	return sat;
}

void dsp_saturator_destroy(dsp_saturator_t * sat)
{
	if (sat != NULL) {
		free(sat);
	}
}

void dsp_saturator_set_cutoff(dsp_saturator_t * sat, double frequency)
{
	double samplerate = sat->samplerate;
	double max_freq = samplerate * 0.5;
	if (frequency > max_freq) {
		frequency = max_freq;
	}
	double fc = frequency / samplerate;
	double b1 = exp(-M_TWO_PI * fc);
	sat->a0 = (float)(1.0 - b1);
	sat->b1 = (float)b1;
}

void dsp_saturator_set_drive(dsp_saturator_t * sat, int input_gain)
{
	sat->amp = (float)dB((double)input_gain);
}

void dsp_saturator_set_output_gain(dsp_saturator_t * sat, int output_trim)
{
	sat->trim = (float)dB((double)-output_trim);
}

void dsp_saturator_process(dsp_saturator_t * sat, float * duplex, unsigned buffersize)
{
	float a0 = sat->a0;
	float b1 = sat->b1;
	float p1_z1 = sat->p1_z1;
	float p2_z1 = sat->p2_z1;
	float amp = sat->amp;
	float trim = sat->trim;

	if (amp > 1.0f) { // 0 dB amp disable saturation
		for (unsigned n = 0; n < buffersize; ++n) {
			duplex[n] = tanhf(duplex[n] * amp);
		}
	}

	for (unsigned n = 0; n < buffersize; ++n) {
		// first pole
		float xn = duplex[n];
		float yn = xn * a0 + p1_z1 * b1;
		p1_z1 = yn;
		// second pole
		yn = yn * a0 + p2_z1 * b1;
		p2_z1 = yn;
		duplex[n] = yn;

		sanitize_float(&p1_z1);
		sanitize_float(&p2_z1);
	}

	if (trim < 1.0f) {
		for (unsigned n = 0; n < buffersize; ++n) {
			duplex[n] *= trim;
		}
	}

	sat->p1_z1 = p1_z1;
	sat->p2_z1 = p2_z1;
}

void dsp_saturator_clear(dsp_saturator_t * sat)
{
	sat->p1_z1 = 0.0f;
	sat->p2_z1 = 0.0f;
}
