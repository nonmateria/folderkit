#include "silence.h"

#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

dsp_silence_t * dsp_silence_create(double samplerate, double max_time_ms, double db_threshold)
{
	dsp_silence_t * sil = malloc(sizeof(dsp_silence_t));
	if (sil == NULL) {
		printf("memory error during s dsp_silence_t allocation\n");
		return NULL;
	}

	sil->counter = 0;
	sil->max = (long)((max_time_ms / 1000.0) * samplerate);
	sil->threshold = dBf((float)db_threshold);
	return sil;
}

void dsp_silence_destroy(dsp_silence_t * sil)
{
	if (sil != NULL) {
		free(sil);
	}
}

void dsp_silence_retrigger(dsp_silence_t * sil)
{
	sil->counter = 0;
}

int dsp_check_silence(dsp_silence_t * sil, float ** buffers, unsigned channels, unsigned start, unsigned buffersize)
{
	float t = sil->threshold;
	for (unsigned c = 0; c < channels; ++c) {
		unsigned max = start + buffersize;
		for (unsigned n = start; n < max; ++n) {
			if (fabs(buffers[c][n]) > t) {
				sil->counter = 0;
				return 0;
			}
		}
	}

	sil->counter += (long)buffersize;
	if (sil->counter > sil->max) {
		return 1; // voice is silent
	}

	return 0;
}
