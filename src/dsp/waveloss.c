#include "waveloss.h"

// base on Supercollider WaveLoss plugin
// https://doc.sccode.org/Classes/WaveLoss.html

#include <stdio.h>
#include <stdlib.h>

#include "../flags.h"
#include "functions.h"
#include "rand.h"

dsp_waveloss_t * dsp_waveloss_create(void)
{
	dsp_waveloss_t * wl = malloc(sizeof(dsp_waveloss_t));
	if (wl == NULL) {
		printf("memory error during waveloss module allocation\n");
		return NULL;
	}
	wl->z1 = 0.0f;
	wl->count = 0;
	wl->actives = 1;
	wl->max = 4;
	wl->mode = FKIT_WAVELOSS_MODE_PULSE;
	wl->on = 1.0f;

	return wl;
}

void dsp_waveloss_destroy(dsp_waveloss_t * wl)
{
	if (wl != NULL) {
		free(wl);
	}
}

void dsp_waveloss_set(dsp_waveloss_t * wl, int ctrl)
{
	unsigned wmax, wactives;
	int mode = FKIT_WAVELOSS_MODE_PULSE;
	if (ctrl == orca_z) {
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		wactives = 1;
		wmax = 128;
	} else if (ctrl == orca_y) {
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		wactives = 1;
		wmax = 64;
	} else if (ctrl == orca_x) {
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		wactives = 1;
		wmax = 32;
	} else if (ctrl >= orca_h) { // random mode, proportional
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		ctrl -= orca_h;
		wactives = 16 - (unsigned)ctrl;
		wmax = 16;
	} else { // pulse mode, controls increases window
		float wpct = (float)ctrl / 16.0f;
		wpct = 1.0f - wpct;
		float wmaxf = 2.0f + wpct * wpct * 64.0f;
		wmax = (unsigned)wmaxf;
		wactives = (unsigned)(wmaxf * 0.5f + wmaxf * wpct * 0.5f);
	}
	wl->actives = wactives;
	wl->max = wmax;
	wl->mode = mode;
}

void dsp_waveloss_process(dsp_waveloss_t * wl, float * duplex, unsigned buffersize)
{
	float z1 = wl->z1;
	float on = wl->on;
	unsigned count = wl->count;
	unsigned actives = wl->actives;
	unsigned max = wl->max;
	int mode = wl->mode;

	for (unsigned n = 0; n < buffersize; ++n) {
		float xn = duplex[n];

		if (z1 <= 0.0f && xn > 0.0f) { // positive crossing
			switch (mode) {
			case 0:
			case FKIT_WAVELOSS_MODE_PULSE:
			default:
				count++;
				if (count >= max) {
					count = 0;
				}
				on = (count < actives) ? 1.0f : 0.0f;
				break;
			case FKIT_WAVELOSS_MODE_RANDOM: {
				unsigned dice = dsp_dice(max);
				on = (dice < actives) ? 1.0f : 0.0f;
			} break;
			}
		}

		z1 = xn;
		duplex[n] = xn * on;
	}

	wl->z1 = z1;
	wl->count = count;
	wl->on = on;
}

void dsp_waveloss_clear(dsp_waveloss_t * wl)
{
	wl->z1 = 0.0f;
	wl->count = 0;
}
