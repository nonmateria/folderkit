#include "slew.h"

#include "buffer.h"
#include "functions.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

dsp_slew_t * dsp_slew_create(double samplerate)
{
	dsp_slew_t * sl = malloc(sizeof(dsp_slew_t));
	if (sl == NULL) {
		printf("error allocatind dsp_slew_t\n");
		return NULL;
	}

	sl->ratemult = (float)(1.0 / samplerate);
	sl->time = 50.0f;
	dsp_slew_to(sl, 1.0f);
	sl->time = 0.0f;
	dsp_slew_to(sl, 1.0f);

	return sl;
}

void dsp_slew_destroy(dsp_slew_t * sl)
{
	if (sl != NULL) {
		free(sl);
	}
}

void dsp_slew_reset(dsp_slew_t * sl, float value)
{
	float temp = sl->time;
	sl->time = 0.0f;
	dsp_slew_to(sl, value);
	sl->time = temp;
}

void dsp_slew_to(dsp_slew_t * sl, float dest)
{
	assert(dest != 0.0f); // inc should never be 0.0f

	if (sl->time == 0.0f) {
		sl->start = dest;
		sl->delta = 0.0f;
		sl->phase = 0.0f;
		sl->reached = 1;
	} else {
		if (!sl->reached) {
			sl->start = sl->start + sl->phase * sl->delta;
		}

		if (sl->start != dest) {
			sl->reached = 0;
			sl->phase = 0.0f;
			sl->delta = dest - sl->start;

			// sets slew time/octave
			/**
			float p0 = log2f( sl->start / 440.0 ) * 12.0f) + 69.0f;
			float p1 = log2f( dest / 440.0 ) * 12.0f) + 69.0f;
			float pdeltaref = abs( p1 - p0 ) / 12.0f;
			// simplified to the formula below:
			**/
			sl->inc = (1000.0f / sl->time) * sl->ratemult;
			float pdeltaref = fabsf(log2f(dest / 440.0f) - log2f(sl->start / 440.0f));
			if (pdeltaref != 0.0f) {
				sl->inc /= pdeltaref;
			}
		}
	}
}

void dsp_slew_process(dsp_slew_t * sl, float * duplex, unsigned buffersize)
{
	float s = sl->start;

	if (sl->reached) {
		dsp_set_value(duplex, s, buffersize);
	} else {
		float ph = sl->phase;
		float inc = sl->inc;
		float d = sl->delta;

		for (unsigned n = 0; n < buffersize; ++n) {

			float inv = 1.0f - ph;
			float iexp = 1.0f - (inv * inv);

			duplex[n] = s + iexp * d;

			ph += inc;
			if (ph > 1.0f) {
				ph = 1.0f;
			}
		}

		if (ph >= 1.0f && !sl->reached) {
			sl->reached = 1;
			sl->start = s + d;
		}

		sl->phase = ph;
	}
}
