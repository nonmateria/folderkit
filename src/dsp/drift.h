#pragma once

typedef struct dsp_drift_t {
	float a0;
	float b1;
	float z1;
	float d;
	float inc;
	float phase;

	float * buffer;
} dsp_drift_t;

dsp_drift_t * dsp_drift_create(double frequency, double samplerate, unsigned buffersize);

void dsp_drift_destroy(dsp_drift_t * drift);

void dsp_drift_render(dsp_drift_t * drift, unsigned bufferize);
