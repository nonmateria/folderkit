#include "lfo.h"

#include <stdio.h>
#include <stdlib.h>

#include "../flags.h"
#include "functions.h"

#define FK_LFO_SINE 0
#define FK_LFO_TRI 1
#define FK_LFO_RAMP 2
#define FK_LFO_SAW 3
#define FK_LFO_SQUARE 4

dsp_lfo_t * dsp_lfo_create(unsigned buffersize, double samplerate, float * table, unsigned size)
{
	dsp_lfo_t * lfo = malloc(sizeof(dsp_lfo_t));
	if (lfo == NULL) {
		printf("memory error during lfo module allocation\n");
		return NULL;
	}
	lfo->buffer = malloc(sizeof(float) * buffersize);
	if (lfo->buffer == NULL) {
		printf("error allocating lfo buffer\n");
		goto error;
	}

	lfo->phase = 0.0f;
	lfo->samplerate = (float)samplerate;
	lfo->sin_table_link = table;
	lfo->size = (float)size;

	lfo->sixteenths = 1.0f;
	dsp_lfo_set_tempo(lfo, 120);

	lfo->wave = FK_LFO_SINE;
	lfo->amount = 0.0f;
	lfo->single = 1;
	lfo->expo = 0;

	return lfo;

error:
	dsp_lfo_destroy(lfo);
	return NULL;
}

void dsp_lfo_destroy(dsp_lfo_t * lfo)
{
	if (lfo != NULL) {
		if (lfo->buffer != NULL) {
			free(lfo->buffer);
		}
		free(lfo);
	}
}

void dsp_lfo_clear(dsp_lfo_t * lfo)
{
	// one shot and ended
	lfo->phase = 1.0f;
	lfo->sixteenths = 1.0f;
	lfo->wave = FK_LFO_SINE;
	lfo->amount = 0.0f;
	lfo->single = 1;
	lfo->expo = 0;
}

void dsp_lfo_set_tempo(dsp_lfo_t * lfo, int tempo)
{
	double bar_time_s = (60.0 * 4.0) / (double)tempo; // seconds for one bar
	double bar_time_samples = bar_time_s * lfo->samplerate;
	double inc = 1.0 / bar_time_samples;
	lfo->base_inc = (float)(inc * 16.0); // base inc is 16x faster
}

#define FK_LFO_SINE 0
#define FK_LFO_TRI 1
#define FK_LFO_RAMP 2
#define FK_LFO_SAW 3
#define FK_LFO_SQUARE 4

void dsp_lfo_set_params(dsp_lfo_t * lfo, int wave, int sixteenths, float amount, float slew)
{
	if (wave != 0) {
		lfo->phase = 0.0f;
	}

	switch (wave) {
	default:
		break;
	case orca_m:
		lfo->wave = FK_LFO_SINE;
		lfo->single = 0;
		lfo->expo = 0;
		break;
	case orca_n:
		lfo->wave = FK_LFO_SINE;
		lfo->single = 1;
		lfo->expo = 0;
		break;
	case orca_w:
		lfo->wave = FK_LFO_SAW;
		lfo->single = 0;
		lfo->expo = 0;
		break;
	case orca_v:
		lfo->wave = FK_LFO_SAW;
		lfo->single = 1;
		lfo->expo = 0;
		break;
	case orca_x:
		lfo->wave = FK_LFO_SAW;
		lfo->single = 0;
		lfo->expo = 1;
		break;
	case orca_l:
		lfo->wave = FK_LFO_SAW;
		lfo->single = 1;
		lfo->expo = 1;
		break;
	case orca_r:
		lfo->wave = FK_LFO_RAMP;
		lfo->single = 0;
		lfo->expo = 0;
		break;
	case orca_f:
		lfo->wave = FK_LFO_RAMP;
		lfo->single = 1;
		lfo->expo = 0;
		break;
	case orca_q:
		lfo->wave = FK_LFO_SQUARE;
		lfo->single = 0;
		lfo->expo = 0;
		break;
	case orca_p:
		lfo->wave = FK_LFO_SQUARE;
		lfo->single = 1;
		lfo->expo = 0;
		break;
	case orca_a:
		lfo->wave = FK_LFO_TRI;
		lfo->single = 0;
		lfo->expo = 0;
		break;
	case orca_t:
		lfo->wave = FK_LFO_TRI;
		lfo->single = 1;
		lfo->expo = 0;
		break;
	case orca_y:
		lfo->wave = FK_LFO_TRI;
		lfo->single = 0;
		lfo->expo = 1;
		break;
	case orca_i:
		lfo->wave = FK_LFO_TRI;
		lfo->single = 1;
		lfo->expo = 1;
		break;
	}

	switch (sixteenths) {
	case 0:
		lfo->sixteenths = 0.5f;
		break;
	case orca_z:
		lfo->sixteenths = 0.25f;
		break;
	case orca_y:
		lfo->sixteenths = 0.125f;
		break;
	case orca_x:
		lfo->sixteenths = 0.0625f;
		break;
	default:
		lfo->sixteenths = sixteenths;
		break;
	}

	lfo->amount = amount;

	double frequency = (double)slew;
	double samplerate = (double)lfo->samplerate;
	double max_freq = samplerate * 0.25;
	if (frequency > max_freq) {
		frequency = max_freq;
	}
	double fc = frequency / samplerate;
	double b1 = exp(-M_TWO_PI * fc);
	lfo->a0 = (float)(1.0 - b1);
	lfo->b1 = (float)b1;
}

void dsp_lfo_render(dsp_lfo_t * lfo, unsigned buffersize)
{
	float phase = lfo->phase;
	float sin_size = lfo->size;
	float * sin_table = lfo->sin_table_link;
	float * buffer = lfo->buffer;
	int single = lfo->single;
	float inc = lfo->base_inc / lfo->sixteenths; // more sixteenths make inc slower

	// first: fill buffer with phase
	for (unsigned n = 0; n < buffersize; ++n) {
		buffer[n] = phase;
		phase += inc;
		if (phase >= 1.0f) {
			if (single) { // this also works because of the 0.75f offset in the sin wave
				phase = 1.0f;
			} else {
				phase -= 1.0f;
			}
		}
	}
	lfo->phase = phase;

	// second: generate selected unipolar wave
	switch (lfo->wave) {
	case FK_LFO_SINE:
		for (unsigned n = 0; n < buffersize; ++n) {
			float ph0 = buffer[n] + 0.75f;
			float ph1 = ph0 - 1.0f;
			float ph = (ph0 > 1.0f) ? ph1 : ph0;
			float tablepos = ph * sin_size; // buffer is phase
			int ipart = (int)tablepos;
			float fract = tablepos - (float)ipart;
			float frame0 = sin_table[ipart];
			float frame1 = sin_table[ipart + 1];
			float w = frame0 * (1.0f - fract) + frame1 * fract;
			buffer[n] = (w * 0.5f) + 0.5f; // unipolar
		}
		break;

	case FK_LFO_TRI:
		for (unsigned n = 0; n < buffersize; ++n) {
			float ph = buffer[n];
			float w = fabsf(ph * 2.0f - 1.0f); // unipolar triangle
			buffer[n] = 1.0f - w;              // unipolar triangle starting from 0.0f
		}
		break;

	case FK_LFO_RAMP:
	default:
		for (unsigned n = 0; n < buffersize; ++n) {
			if (buffer[n] == 1.0f) { // this is needed for oneshot
				buffer[n] = 0.0f;
			}
		}
		break;

	case FK_LFO_SAW:
		for (unsigned n = 0; n < buffersize; ++n) {
			buffer[n] = 1.0f - buffer[n];
		}
		break;

	case FK_LFO_SQUARE:
		for (unsigned n = 0; n < buffersize; ++n) {
			buffer[n] = (buffer[n] < 0.5f) ? 1.0f : 0.0f;
		}
		break;
	}

	if (lfo->expo) {
		for (unsigned n = 0; n < buffersize; ++n) {
			buffer[n] *= buffer[n];
		}
	}

	// third: slew generated wave with a 12db/oct lowpass filter
	float a0 = lfo->a0;
	float b1 = lfo->b1;
	float p1_z1 = lfo->p1_z1;
	float p2_z1 = lfo->p2_z1;

	for (unsigned n = 0; n < buffersize; ++n) {
		sanitize_float(&p1_z1);
		sanitize_float(&p2_z1);

		// first pole
		float xn = buffer[n];
		float yn = xn * a0 + p1_z1 * b1;
		p1_z1 = yn;
		// second pole
		yn = yn * a0 + p2_z1 * b1;
		p2_z1 = yn;
		buffer[n] = yn;
	}

	lfo->p1_z1 = p1_z1;
	lfo->p2_z1 = p2_z1;
}
