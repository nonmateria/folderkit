#include "operator.h"

#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

dsp_operator_t * dsp_operator_create(float * table, unsigned size, double sop_gainlerate)
{
	dsp_operator_t * op = malloc(sizeof(dsp_operator_t));
	if (op == NULL) {
		printf("memory error during operator module allocation\n");
		return NULL;
	}
	op->phase = 0.0f;
	op->oneslashsr = (float)(1.0 / sop_gainlerate);
	op->wave_link = table;
	op->size = (float)size;
	op->op_gain = 1.0f;
	op->input_gain = 0.0f;
	op->modulator_amount = 0.0f;
	op->self_amount = 0.0f;
	op->amp_modulation = 0.0f;
	op->z1 = 0.0f;

	return op;
}

void dsp_operator_destroy(dsp_operator_t * op)
{
	if (op != NULL) {
		free(op);
	}
}

void dsp_operator_process(dsp_operator_t * op, float * duplex, const float * freq_buffer, const float * mod_buffer, unsigned buffersize)
{
	float oneslashsr = op->oneslashsr;
	float phase = op->phase;
	float size = op->size;
	float * wave = op->wave_link;
	float op_gain = op->op_gain;
	float input_gain = op->input_gain;
	float env_amount = op->modulator_amount / op_gain; // rescaling is based on op_gain
	float z1 = op->z1;
	float self_amount = op->self_amount;
	float amp0 = 1.0f - op->amp_modulation;
	float amp1 = op->amp_modulation;

	for (unsigned n = 0; n < buffersize; ++n) {

		float mod_wave = duplex[n] * env_amount; // from modulator
		mod_wave += z1 * self_amount;            // from self modulation

		float phase_fm = phase + mod_wave;
		int ipart = (int)phase_fm;

		if (phase_fm >= 1.0f) {
			phase_fm -= (float)ipart;
		} else if (phase_fm < 0.0f) {
			phase_fm += (float)(1 - ipart);
		}

		float tablepos = phase_fm * size;
		ipart = (int)tablepos;
		float fract = tablepos - (float)ipart;
		float frame0 = wave[ipart];
		float frame1 = wave[ipart + 1];
		float yn = frame0 * (1.0f - fract) + frame1 * fract;

		yn = yn * amp0 + yn * amp1 * mod_buffer[n];
		z1 = yn;

		duplex[n] = duplex[n] * input_gain + yn * op_gain;

		float inc = freq_buffer[n] * oneslashsr;
		phase += inc;
		if (phase >= 1.0f) {
			phase -= 1.0f;
		}
	}

	op->phase = phase;
	op->z1 = z1;
}
