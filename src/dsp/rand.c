#include "rand.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

unsigned linux_get_devrandom(void)
{
	unsigned seed = (unsigned)time(NULL); // fallback

	int random_fd = -1;
	random_fd = open("/dev/urandom", O_RDONLY);

	if (random_fd == -1) {
		fprintf(stderr,
		        "[nsketch] error opening /dev/urandom: "
		        "errno [%d], strerrer [%s], falling back to time(NULL)\n",
		        errno, strerror(errno));
	} else {
		ssize_t ret = 0;
		char buf[4];
		ret = read(random_fd, buf, 4);
		if (ret != 4) {
			fprintf(stderr,
			        "Only read [%d] bytes, expected 4,"
			        " falling back to time(NULL)\n",
			        (int)ret);
		} else {
			seed = (unsigned)(buf[3] + (buf[2] << 8) + (buf[1] << 16) + (buf[0] << 24));
		}
		close(random_fd);
	}
	return seed;
}

#define RND_IMPLEMENTATION
#include "3rdp/rnd.h"

rnd_pcg_t dsp__pnrg;

void dsp_seed_random(void)
{
	unsigned seed = linux_get_devrandom();
	rnd_pcg_seed(&dsp__pnrg, seed);
}

// 2.0f is excluded i know, but it's good enough
float dsp_brand(void)
{
	return rnd_pcg_nextf(&dsp__pnrg) * 2.0f - 1.0f;
}

float dsp_urand(void)
{
	return rnd_pcg_nextf(&dsp__pnrg);
}

unsigned dsp_dice(unsigned size)
{
	return rnd_pcg_next(&dsp__pnrg) % size;
}
