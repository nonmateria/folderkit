#include "delay.h"

#include <stdio.h>
#include <stdlib.h>

#include "../flags.h"
#include "buffer.h"
#include "functions.h"
#include "rand.h"

#define DELAY_WL_CMAX 32
#define DELAY_SLEW0 0.0002f
#define DELAY_SLEW1 0.9998f

#define DELAY_SSLEW0 0.01f
#define DELAY_SSLEW1 0.99f

dsp_delay_t * dsp_delay_create(double samplerate, unsigned max_delay_ms, float lfo_rate, float lfo_drift)
{
	dsp_delay_t * del = malloc(sizeof(dsp_delay_t));
	if (del == NULL) {
		printf("memory error allocating delay module\n");
		return NULL;
	}

	del->write = 0;
	del->read_offset = 1.0f;
	del->read_offset_slew = 1.0f;
	del->mod_amt = 0.0f;
	del->mod_slew = 0.0f;
	del->mod_mult = (float)(100000000.0 / samplerate);

	del->samplerate = (float)samplerate;
	del->size = (int)(samplerate * 0.001 * (double)max_delay_ms);
	del->buffer = dsp_create_buffer((unsigned)del->size);
	del->dry = 1.0f;
	del->lfo_drift = lfo_drift;

	double fc = 20.0 / samplerate; // 20 hz leak dc
	double b1 = exp(-M_TWO_PI * fc);
	del->leak_a0 = (float)(1.0 - b1);
	del->leak_b1 = (float)b1;

	dsp_set_zero(del->buffer, (unsigned)del->size);
	del->send = 0.0f;
	del->send_slew = 0.0f;
	del->feedback = 0.0f;
	del->z1 = 0.0f;
	del->leak_z1 = 0.0f;

	dsp_delay_set_tempo(del, 120);
	dsp_delay_set_sync(del, 4.0);

	del->lfo_ph = 0.0f;
	del->lfo_inc = (float)(lfo_rate / samplerate);

	del->wl_actives = 16;
	del->wl_mode = FKIT_WAVELOSS_MODE_PULSE;
	del->wl_count = 0;
	del->wl_on = 1.0f;

	return del;
}

void dsp_delay_destroy(dsp_delay_t * del)
{
	if (del != NULL) {
		dsp_destroy_buffer(del->buffer);
		free(del);
	}
}

void dsp_delay_set_tempo(dsp_delay_t * del, int tempo)
{
	double bar_time_ms = (60000.0 * 4.0) / (double)tempo;
	double sixteenth = bar_time_ms / 16.0f;
	del->sync_coeff = (float)(del->samplerate * 0.001 * sixteenth);
}

void dsp_delay_set_sync(dsp_delay_t * del, float sixteenths)
{
	del->sync_16th = sixteenths;
	float frames = del->sync_coeff * del->sync_16th;
	float max = (float)del->size;
	if (frames >= max) {
		frames = max - 1.0f;
	}
	del->read_offset = frames;
}

void dsp_delays_set_params(int num, dsp_delay_t ** delays, float sixteenths, float fb, float dry, float send, float mod_amt)
{
	for (int i = 0; i < num; ++i) {
		delays[i]->feedback = fb;
		delays[i]->send = send;
		delays[i]->dry = dry;
		delays[i]->mod_amt = mod_amt;
		dsp_delay_set_sync(delays[i], sixteenths);
	}
}

void dsp_delays_wl_set_params(int num, dsp_delay_t ** delays, float sixteenths, float fb, float dry, float send, int ctrl)
{
	unsigned wmax, wactives;
	int mode = FKIT_WAVELOSS_MODE_PULSE;
	if (ctrl == orca_z) {
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		wactives = 1;
		wmax = 128;
	} else if (ctrl == orca_y) {
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		wactives = 1;
		wmax = 64;
	} else if (ctrl == orca_x) {
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		wactives = 1;
		wmax = 32;
	} else if (ctrl >= orca_h) { // random mode, proportional
		mode = FKIT_WAVELOSS_MODE_RANDOM;
		ctrl -= orca_h;
		if (ctrl > 16) {
			ctrl = 16;
		}
		wactives = 16 - (unsigned)ctrl;
		wmax = 16;
	} else { // pulse mode, controls increases window
		float wpct = (float)ctrl / 16.0f;
		wpct = 1.0f - wpct;
		float wmaxf = 2.0f + wpct * wpct * 64.0f;
		wmax = (unsigned)wmaxf;
		wactives = (unsigned)(wmaxf * 0.5f + wmaxf * wpct * 0.5f);
	}

	for (int i = 0; i < num; ++i) {
		delays[i]->feedback = fb;
		delays[i]->send = send;
		delays[i]->dry = dry;
		dsp_delay_set_sync(delays[i], sixteenths);
		delays[i]->wl_actives = wactives;
		delays[i]->wl_mode = mode;
		delays[i]->wl_max = wmax;
	}
}

void dsp_delay_update_sync(dsp_delay_t * del)
{
	dsp_delay_set_sync(del, del->sync_16th);
}

void dsp_delay_process_sync(dsp_delay_t * del, float * duplex, float * drift, unsigned buffersize)
{
	float dry = del->dry;
	float feedback = del->feedback;
	int write = del->write;
	int size = del->size;
	int rebound = size - 1;

	float send = del->send;
	float send_slew = del->send_slew;
	float roffset = del->read_offset;
	float roffset_slew = del->read_offset_slew;
	float mod_amt = del->mod_amt * del->mod_mult;
	float mod_slew = del->mod_slew;

	float g = 0.5f * (1.0f - feedback);
	float z1 = del->z1;
	float a0 = del->leak_a0;
	float b1 = del->leak_b1;
	float lz1 = del->leak_z1;

	float lfo_drift = del->lfo_drift;
	float lfo_ph = del->lfo_ph;
	float lfo_inc = del->lfo_inc;

	for (unsigned n = 0; n < buffersize; ++n) {

		// triangle wave lfo
		lfo_ph += lfo_inc * (1.0f + drift[n] * lfo_drift);
		if (lfo_ph > 1.0f) {
			lfo_ph -= 1.0f;
		}
		float lfo = fabsf(lfo_ph * 2.0f - 1.0f); // only positive

		// slew
		roffset_slew = roffset * DELAY_SLEW0 + roffset_slew * DELAY_SLEW1;
		send_slew = send * DELAY_SSLEW0 + send_slew * DELAY_SSLEW1;
		mod_slew = mod_amt * DELAY_SLEW0 + mod_slew * DELAY_SLEW1;

		// calculates indices
		float offset = roffset_slew + lfo * mod_slew;
		int read0 = (int)offset;
		float f1 = offset - (float)read0; // fractional part for lerp
		float f0 = 1.0f - f1;
		read0 += write;
		int read1 = read0 + 1;
		if (read0 >= size) {
			read0 -= size;
		}
		if (read1 >= size) {
			read1 -= size;
		}
		// ------

		float xn = duplex[n];

		float frame0 = del->buffer[read0];
		float frame1 = del->buffer[read1];
		float yn = frame0 * f0 + frame1 * f1;

		duplex[n] = duplex[n] * dry + yn;

		// damping inside the feedback path
		yn = yn + g * z1;
		z1 = yn;

		// leak dc inside the feedback path
		float lxn = yn;
		yn = lxn * a0 + lz1 * b1;
		lz1 = yn;
		yn = lxn - yn;

		// removes denormalized numbers and NaNs
		sanitize_float(&z1);
		sanitize_float(&lz1);
		sanitize_float(&yn);

		del->buffer[write] = xn * send_slew + yn * feedback;

		write--;
		if (write == -1) {
			write = rebound;
		}
	}

	del->read_offset_slew = roffset_slew;
	del->mod_slew = mod_slew;
	del->send_slew = send_slew;
	del->write = write;
	del->z1 = z1;
	del->leak_z1 = lz1;
	del->lfo_ph = lfo_ph;
}

void dsp_delay_process_wl(dsp_delay_t * del, float * duplex, unsigned buffersize)
{
	float send = del->send;
	float dry = del->dry;
	float feedback = del->feedback;
	int write = del->write;
	int size = del->size;
	int rebound = size - 1;

	float roffset = del->read_offset;
	float roffset_slew = del->read_offset_slew;
	float send_slew = del->send_slew;

	float wl_on = del->wl_on;
	unsigned wl_count = del->wl_count;
	unsigned wl_actives = del->wl_actives;
	unsigned wl_max = del->wl_max;
	int wl_mode = del->wl_mode;
	float z1 = del->z1;

	for (unsigned n = 0; n < buffersize; ++n) {

		// slew
		roffset_slew = roffset * DELAY_SLEW0 + roffset_slew * DELAY_SLEW1;
		send_slew = send * DELAY_SSLEW0 + send_slew * DELAY_SSLEW1;

		// calculates indices
		float offset = roffset_slew;
		int read0 = (int)offset;
		float f1 = offset - (float)read0; // fractional part for lerp
		float f0 = 1.0f - f1;
		read0 += write;
		int read1 = read0 + 1;
		if (read0 >= size) {
			read0 -= size;
		}
		if (read1 >= size) {
			read1 -= size;
		}
		// ------

		float xn = duplex[n];

		float frame0 = del->buffer[read0];
		float frame1 = del->buffer[read1];
		float yn = frame0 * f0 + frame1 * f1;

		if (z1 <= 0.0f && yn > 0.0f) { // positive crossing
			switch (wl_mode) {
			case 0:
			case FKIT_WAVELOSS_MODE_PULSE:
			default:
				wl_count++;
				if (wl_count >= wl_max) {
					wl_count = 0;
				}
				wl_on = (wl_count < wl_actives) ? 1.0f : 0.0f;
				break;
			case FKIT_WAVELOSS_MODE_RANDOM: {
				unsigned dice = dsp_dice(wl_max);
				wl_on = (dice < wl_actives) ? 1.0f : 0.0f;
			} break;
			}
		}

		z1 = yn;

		sanitize_float(&yn);

		yn *= wl_on;

		duplex[n] = duplex[n] * dry + yn;

		del->buffer[write] = xn * send_slew + yn * feedback;

		write--;
		if (write == -1) {
			write = rebound;
		}
	}

	del->read_offset_slew = roffset_slew;
	del->send_slew = send_slew;
	del->write = write;
	del->z1 = z1;
	del->wl_count = wl_count;
	del->wl_on = wl_on;
}

void dsp_delay_process_tuned(dsp_delay_t * del, float * duplex, float * freq, unsigned buffersize)
{
	float samplerate = del->samplerate;
	float feedback = del->feedback;
	int write = del->write;

	int size = del->size;
	int rebound = size - 1;
	float sizef = (float)size;

	float g = 0.5f * (1.0f - feedback);
	float z1 = del->z1;
	float a0 = del->leak_a0;
	float b1 = del->leak_b1;
	float lz1 = del->leak_z1;

	for (unsigned n = 0; n < buffersize; ++n) {

		float offset = samplerate / freq[n];
		offset += (float)write;
		while (offset >= sizef) { // TODO optimize out this while
			offset -= sizef;
		}
		int read0 = (int)offset;
		int read1 = read0 + 1;
		if (read1 == size) {
			read1 = 0;
		}
		float fract = offset - (float)read0;

		float xn = duplex[n];

		float frame0 = del->buffer[read0];
		float frame1 = del->buffer[read1];
		float yn = frame0 * (1.0f - fract) + frame1 * fract;

		duplex[n] += yn * feedback;

		// damping inside the feedback path
		yn = yn + g * z1;
		z1 = yn;

		// leak dc inside the feedback path
		float lxn = yn;
		yn = lxn * a0 + lz1 * b1;
		lz1 = yn;
		yn = lxn - yn;

		sanitize_float(&z1);
		sanitize_float(&lz1);
		sanitize_float(&yn);

		del->buffer[write] = xn + yn * feedback;

		write--;
		if (write == -1) {
			write = rebound;
		}
	}

	del->write = write;
	del->z1 = z1;
	del->leak_z1 = lz1;
}

void dsp_delay_clear(dsp_delay_t * del)
{
	// dsp_set_zero(del->buffer, (unsigned)del->size);
	//  ^^^ clearing the delay buffer causes underruns very often
	del->send = 0.0f;
	del->send_slew = 0.0f;
	del->feedback = 0.0f;
	del->z1 = 0.0f;
	del->leak_z1 = 0.0f;
}