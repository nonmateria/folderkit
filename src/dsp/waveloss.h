#pragma once

typedef struct dsp_waveloss_t {
	float z1;
	unsigned count;
	unsigned actives;
	unsigned max;
	int mode;
	float on;
} dsp_waveloss_t;

dsp_waveloss_t * dsp_waveloss_create(void);

void dsp_waveloss_destroy(dsp_waveloss_t * wl);

void dsp_waveloss_set(dsp_waveloss_t * wl, int ctrl);

void dsp_waveloss_process(dsp_waveloss_t * wl, float * duplex, unsigned buffersize);

void dsp_waveloss_clear(dsp_waveloss_t * wl);
