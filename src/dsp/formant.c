
#include "formant.h"

#include "../flags.h"
#include "functions.h"
#include <stdio.h>
#include <stdlib.h>

static void _dsp_formant_build_library(dsp_formant_t * vow)
{
	vow->vow_library[PART_BASS * FK_VOW_VOCALS + VOCAL_A] =
	    (dsp_vowel_t){.freq = {600, 1040, 2250, 2450, 2750},
	                  .amp = {dBf(0), dBf(-7), dBf(-9), dBf(-9), dBf(-20)},
	                  .q = {60, 70, 110, 120, 130}};

	vow->vow_library[PART_BASS * FK_VOW_VOCALS + VOCAL_E] =
	    (dsp_vowel_t){.freq = {400, 1620, 2400, 2800, 2750},
	                  .amp = {dBf(0), dBf(-12), dBf(-9), dBf(-12), dBf(-19)},
	                  .q = {40, 80, 100, 120, 120}};

	vow->vow_library[PART_BASS * FK_VOW_VOCALS + VOCAL_I] =
	    (dsp_vowel_t){.freq = {250, 1750, 2600, 3050, 3340},
	                  .amp = {dBf(0), dBf(-30), dBf(-16), dBf(-22), dBf(-28)},
	                  .q = {60, 90, 100, 120, 120}};

	vow->vow_library[PART_BASS * FK_VOW_VOCALS + VOCAL_O] =
	    (dsp_vowel_t){.freq = {400, 750, 2400, 2600, 2900},
	                  .amp = {dBf(0), dBf(-11), dBf(-21), dBf(-20), dBf(-40)},
	                  .q = {60, 90, 100, 120, 120}};

	vow->vow_library[PART_BASS * FK_VOW_VOCALS + VOCAL_U] =
	    (dsp_vowel_t){.freq = {350, 600, 2400, 2675, 2950},
	                  .amp = {dBf(0), dBf(-20), dBf(-32), dBf(-28), dBf(-36)},
	                  .q = {40, 80, 100, 120, 120}};

	vow->vow_library[PART_TENOR * FK_VOW_VOCALS + VOCAL_A] =
	    (dsp_vowel_t){.freq = {650, 1080, 2650, 2900, 3250},
	                  .amp = {dBf(0), dBf(-6), dBf(-7), dBf(-8), dBf(-22)},
	                  .q = {80, 90, 120, 130, 140}};

	vow->vow_library[PART_TENOR * FK_VOW_VOCALS + VOCAL_E] =
	    (dsp_vowel_t){.freq = {440, 1700, 2600, 3200, 3580},
	                  .amp = {dBf(0), dBf(-14), dBf(-12), dBf(-14), dBf(-20)},
	                  .q = {70, 80, 100, 120, 120}};

	vow->vow_library[PART_TENOR * FK_VOW_VOCALS + VOCAL_I] =
	    (dsp_vowel_t){.freq = {290, 1870, 2800, 3250, 3540},
	                  .amp = {dBf(0), dBf(-15), dBf(-18), dBf(-20), dBf(-30)},
	                  .q = {40, 90, 100, 120, 120}};

	vow->vow_library[PART_TENOR * FK_VOW_VOCALS + VOCAL_O] =
	    (dsp_vowel_t){.freq = {400, 800, 2600, 2800, 3000},
	                  .amp = {dBf(0), dBf(-10), dBf(-12), dBf(-12), dBf(-26)},
	                  .q = {70, 80, 100, 130, 135}};

	vow->vow_library[PART_TENOR * FK_VOW_VOCALS + VOCAL_U] =
	    (dsp_vowel_t){.freq = {350, 600, 2700, 2900, 3300},
	                  .amp = {dBf(0), dBf(-20), dBf(-17), dBf(-14), dBf(-26)},
	                  .q = {40, 60, 100, 120, 120}};

	vow->vow_library[PART_CTENOR * FK_VOW_VOCALS + VOCAL_A] =
	    (dsp_vowel_t){.freq = {660, 1120, 2750, 3000, 3350},
	                  .amp = {dBf(0), dBf(-6), dBf(-23), dBf(-24), dBf(-38)},
	                  .q = {80, 90, 120, 130, 140}};

	vow->vow_library[PART_CTENOR * FK_VOW_VOCALS + VOCAL_E] =
	    (dsp_vowel_t){.freq = {440, 1800, 2700, 3000, 3300},
	                  .amp = {dBf(0), dBf(-14), dBf(-18), dBf(-20), dBf(-20)},
	                  .q = {70, 80, 100, 120, 120}};

	vow->vow_library[PART_CTENOR * FK_VOW_VOCALS + VOCAL_I] =
	    (dsp_vowel_t){.freq = {270, 1850, 2900, 3350, 3590},
	                  .amp = {dBf(0), dBf(-24), dBf(-24), dBf(-36), dBf(-36)},
	                  .q = {40, 90, 100, 120, 120}};

	vow->vow_library[PART_CTENOR * FK_VOW_VOCALS + VOCAL_O] =
	    (dsp_vowel_t){.freq = {430, 820, 2700, 3000, 3300},
	                  .amp = {dBf(0), dBf(-10), dBf(-26), dBf(-22), dBf(-34)},
	                  .q = {40, 80, 100, 120, 120}};

	vow->vow_library[PART_CTENOR * FK_VOW_VOCALS + VOCAL_U] =
	    (dsp_vowel_t){.freq = {370, 630, 2750, 3000, 3400},
	                  .amp = {dBf(0), dBf(-20), dBf(-23), dBf(-30), dBf(-34)},
	                  .q = {40, 60, 100, 120, 120}};

	vow->vow_library[PART_ALTO * FK_VOW_VOCALS + VOCAL_A] =
	    (dsp_vowel_t){.freq = {800, 1150, 2800, 3500, 4950},
	                  .amp = {dBf(0), dBf(-4), dBf(-20), dBf(-36), dBf(-60)},
	                  .q = {80, 90, 120, 130, 140}};

	vow->vow_library[PART_ALTO * FK_VOW_VOCALS + VOCAL_E] =
	    (dsp_vowel_t){.freq = {400, 1600, 2700, 3300, 4950},
	                  .amp = {dBf(0), dBf(-24), dBf(-30), dBf(-35), dBf(-60)},
	                  .q = {60, 80, 120, 150, 200}};

	vow->vow_library[PART_ALTO * FK_VOW_VOCALS + VOCAL_I] =
	    (dsp_vowel_t){.freq = {350, 1700, 2700, 3700, 4950},
	                  .amp = {dBf(0), dBf(-20), dBf(-30), dBf(-36), dBf(-60)},
	                  .q = {50, 100, 120, 150, 200}};

	vow->vow_library[PART_ALTO * FK_VOW_VOCALS + VOCAL_O] =
	    (dsp_vowel_t){.freq = {450, 800, 2830, 3500, 4950},
	                  .amp = {dBf(0), dBf(-9), dBf(-16), dBf(-28), dBf(-55)},
	                  .q = {70, 80, 100, 130, 135}};

	vow->vow_library[PART_ALTO * FK_VOW_VOCALS + VOCAL_U] =
	    (dsp_vowel_t){.freq = {325, 700, 2530, 3500, 4950},
	                  .amp = {dBf(0), dBf(-12), dBf(-30), dBf(-40), dBf(-64)},
	                  .q = {50, 60, 170, 180, 200}};

	vow->vow_library[PART_SOPRANO * FK_VOW_VOCALS + VOCAL_A] =
	    (dsp_vowel_t){.freq = {800, 1150, 2900, 3900, 4950},
	                  .amp = {dBf(0), dBf(-6), dBf(-32), dBf(-20), dBf(-50)},
	                  .q = {80, 90, 120, 130, 140}};

	vow->vow_library[PART_SOPRANO * FK_VOW_VOCALS + VOCAL_E] =
	    (dsp_vowel_t){.freq = {350, 2000, 2800, 3600, 4950},
	                  .amp = {dBf(0), dBf(-20), dBf(-15), dBf(-40), dBf(-56)},
	                  .q = {60, 100, 120, 150, 200}};

	vow->vow_library[PART_SOPRANO * FK_VOW_VOCALS + VOCAL_I] =
	    (dsp_vowel_t){.freq = {270, 2140, 2950, 3900, 4950},
	                  .amp = {dBf(0), dBf(-12), dBf(-26), dBf(-26), dBf(-44)},
	                  .q = {60, 90, 100, 120, 120}};

	vow->vow_library[PART_SOPRANO * FK_VOW_VOCALS + VOCAL_O] =
	    (dsp_vowel_t){.freq = {450, 800, 2830, 3800, 4950},
	                  .amp = {dBf(0), dBf(-11), dBf(-22), dBf(-22), dBf(-50)},
	                  .q = {40, 80, 100, 120, 120}};

	vow->vow_library[PART_SOPRANO * FK_VOW_VOCALS + VOCAL_U] =
	    (dsp_vowel_t){.freq = {325, 700, 2700, 3800, 4950},
	                  .amp = {dBf(0), dBf(-16), dBf(-35), dBf(-40), dBf(-60)},
	                  .q = {50, 60, 170, 180, 200}};
}

dsp_formant_t * dsp_formant_create(double samplerate)
{
	dsp_formant_t * vow = malloc(sizeof(dsp_formant_t));
	if (vow == NULL) {
		printf("memory error while creating formant filter\n");
		return NULL;
	}
	vow->samplerate = (float)samplerate;

	_dsp_formant_build_library(vow);
	vow->master_gain = dBf(-18.0f); // 18 dB attenuation

	dsp_formant_clear(vow);

	return vow;
}

void dsp_formant_clear(dsp_formant_t * vow)
{
	for (int i = 0; i < 5; ++i) {
		vow->params.x_z1[i] = 0.0f;
		vow->params.x_z2[i] = 0.0f;
		vow->params.y_z1[i] = 0.0f;
		vow->params.y_z2[i] = 0.0f;
	}
	vow->vow_now = vow->vow_library[0];
	vow->vow_dest = vow->vow_now;
	vow->lpf_z1 = 0.0f;
	vow->lpf_z2 = 0.0f;
}

void dsp_formant_destroy(dsp_formant_t * vow)
{
	if (vow != NULL) {
		free(vow);
	}
}

void dsp_formants_set(dsp_formant_t ** vows, int part, int vocal, int slew, float cutoff)
{
	switch (part) {
	case 0:
	case orca_b:
		part = PART_BASS;
		break;
	case 1:
	case orca_t:
		part = PART_TENOR;
		break;
	case 2:
	case orca_c:
		part = PART_CTENOR;
		break;
	case 3:
	case orca_a:
		part = PART_ALTO;
		break;
	case 4:
	case orca_s:
		part = PART_SOPRANO;
		break;
	default:
		part = PART_BASS;
		break;
	}
	switch (vocal) {
	case 0:
	case orca_a:
		vocal = VOCAL_A;
		break;
	case 1:
	case orca_e:
		vocal = VOCAL_E;
		break;
	case 2:
	case orca_i:
		vocal = VOCAL_I;
		break;
	case 3:
	case orca_o:
		vocal = VOCAL_O;
		break;
	case 4:
	case orca_u:
		vocal = VOCAL_U;
		break;
	default:
		vocal = VOCAL_A;
		break;
	}

	for (unsigned c = 0; c < 2; ++c) {
		vows[c]->vow_dest = vows[c]->vow_library[part * FK_VOW_VOCALS + vocal];
	}
	float sr = vows[0]->samplerate;

	float freq = sr * 0.5f;

	for (int i = 0; i < slew; ++i) {
		freq *= 0.25f;
	}
	// slew coefficient calculation
	float x = expf(-M_TWO_PI * freq / sr);

	for (unsigned c = 0; c < 2; ++c) {
		vows[c]->slew_a0 = 1.0f - x;
		vows[c]->slew_b1 = x;
	}

	// filter coefficient calculation
	float xf = expf(-M_TWO_PI * cutoff / sr);

	for (unsigned c = 0; c < 2; ++c) {
		vows[c]->lpf_a0 = 1.0f - xf;
		vows[c]->lpf_b1 = xf;
	}
}

void dsp_formant_process(dsp_formant_t * vow, float * duplex, unsigned buffersize)
{
	dsp_vowel_t z1 = vow->vow_now;
	dsp_vowel_t d = vow->vow_dest;
	dsp_biquads_parameters_t p = vow->params;

	float one_slash_sr = 1.0f / vow->samplerate;
	float master_gain = vow->master_gain;
	float slew_a0 = vow->slew_a0;
	float slew_b1 = vow->slew_b1;
	float lpf_a0 = vow->lpf_a0;
	float lpf_b1 = vow->lpf_b1;
	float lpf_z1 = vow->lpf_z1;
	float lpf_z2 = vow->lpf_z2;

	for (unsigned i = 0; i < 5; ++i) {
		p.b1[i] = 0.0f; // in BPF2 biquads b1 is always zero
	}

	for (unsigned n = 0; n < buffersize; ++n) {
		float xn = duplex[n];
		float out = 0.0f;

		for (unsigned i = 0; i < 5; ++i) {
			// 0 - slew the parameters
			float freq = slew_a0 * d.freq[i] + slew_b1 * z1.freq[i];
			z1.freq[i] = freq;
			float q = slew_a0 * d.q[i] + slew_b1 * z1.q[i];
			z1.q[i] = q;
			float amp = slew_a0 * d.amp[i] + slew_b1 * z1.amp[i];
			z1.amp[i] = amp;

			// 1 - calculate biquad coefficients
			float wc = freq * M_TWO_PI;
			if (q == 0) {
				q = 0.0000001f;
			}

			float a1 = 1.0f / q;
			float a0 = 1.0f;
			float b2 = 0.0f;
			float b1 = amp;
			float b0 = 0.0f;

			float c = 1.0f / tan(wc * 0.5f * one_slash_sr);
			float csq = c * c;
			float d = a0 + a1 * c + csq;

			p.b0[i] = (b0 + b1 * c + b2 * csq) / d;
			p.b1[i] = 2.0f * (b0 - b2 * csq) / d;
			p.b2[i] = (b0 - b1 * c + b2 * csq) / d;

			p.a1[i] = 2.0f * (a0 - csq) / d;
			p.a2[i] = (a0 - a1 * c + csq) / d;

			// 2 - process biquad
			float yn = p.b0[i] * xn + p.b1[i] * p.x_z1[i] + p.b2[i] * p.x_z2[i] - p.a1[i] * p.y_z1[i] - p.a2[i] * p.y_z2[i];

			p.x_z2[i] = p.x_z1[i];
			p.x_z1[i] = xn;
			p.y_z2[i] = p.y_z1[i];
			p.y_z1[i] = yn;

			// 3 - sum the biquad output to the others
			out += yn;
		}

		// 4 - slew the output with two 6 oct/db one pole filters
		float in = out * master_gain;

		float stage_0 = lpf_a0 * in + lpf_b1 * lpf_z1;
		lpf_z1 = stage_0;

		float stage_1 = lpf_a0 * stage_0 + lpf_b1 * lpf_z2;
		lpf_z2 = stage_1;

		duplex[n] = stage_1;
	}

	vow->vow_now = z1;
	vow->params = p;
	vow->lpf_z1 = lpf_z1;
	vow->lpf_z2 = lpf_z2;
}
