#include "softclip.h"

void dsp_softclip(float * duplex, float threshold, unsigned buffersize)
{
	float constant = -threshold + threshold * threshold;

	for (unsigned n = 0; n < buffersize; ++n) {

		float xn = duplex[n];

		float sign = (xn > 0.0f) ? 1.0f : -1.0f;

		float yn = xn * sign; // abs value

		float saturated = (yn + constant) / yn;
		saturated *= sign; // sign mask

		yn = (yn > threshold) ? saturated : xn;

		duplex[n] = yn;
	}
}
