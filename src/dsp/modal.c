/*
 * implemented thanks to this article in Paul Batchelor's sndkit wiki
 * 		https://pbat.ch/sndkit/modalres/
 * and the linked page in the csound manual appendix
 * 		https://csound.com/docs/manual/MiscModalFreq.html
 */

#include "modal.h"

#include "functions.h"

#include <stdio.h>
#include <stdlib.h>

#define FK_MODAL_EPSILON 0.000000000000000000000000000000000001f
#define FK_MODAL_AMPBASE 0.125f

dsp_modal_t * dsp_modal_create(unsigned int samplerate)
{

	dsp_modal_t * mb = malloc(sizeof(dsp_modal_t));
	if (mb == NULL) {
		printf("error allocatind modal_t\n");
		return NULL;
	}

	mb->sr = (float)samplerate;
	mb->tuning_change = 0;
	mb->dry = 0.0f;

	double dummy[] = {1.0, 2.0};
	dsp_modal_set_coeff(mb, 440.0f, 0, 1.0f, dummy, 8);

	for (int i = 0; i < FK_MAX_RESONATORS; ++i) {
		mb->z0[i] = 0.0f;
		mb->z1[i] = 0.0f;
	}

	return mb;
}

void dsp_modal_destroy(dsp_modal_t * mb)
{
	if (mb != NULL) {
		free(mb);
	}
}

static void _set_ratios(dsp_modal_t * mb, float * ratios, int num, float amp)
{
	if (num > FK_MAX_RESONATORS) {
		num = FK_MAX_RESONATORS;
	}
	for (int i = 0; i < num; ++i) {
		mb->ratio[i] = ratios[i];
	}

	mb->rsize = num;
	mb->scale = (amp * FK_MODAL_AMPBASE) / (float)num;
}

static void _set_ratios_from_tuning_simple(dsp_modal_t * mb, double * ratios, int max, float stretch, int reverse)
{
	if (max > FK_MAX_RESONATORS) {
		max = FK_MAX_RESONATORS;
	}
	if (reverse) {
		for (int i = 0; i < max; ++i) {
			mb->ratio[i] = (float)(ratios[max - i - 1] * (1.0f + stretch * (double)i));
		}
	} else {
		for (int i = 0; i < max; ++i) {
			mb->ratio[i] = (float)(ratios[i] * (1.0f + stretch * (double)i));
		}
	}
	mb->rsize = max;
	mb->scale = FK_MODAL_AMPBASE / (float)max;
}

static void _set_ratios_from_tuning_spaced(dsp_modal_t * mb, double * ratios, int max, int step)
{
	if (max > FK_MAX_RESONATORS) {
		max = FK_MAX_RESONATORS;
	}
	for (int i = 0; i < max; ++i) {
		int d = (i * step) % 36;
		mb->ratio[i] = (float)ratios[d];
	}
	mb->rsize = max;
	mb->scale = FK_MODAL_AMPBASE / (float)max;
}

static void _set_ratios_from_tuning_centered(dsp_modal_t * mb, double * ratios, int center, int max, float stretch)
{
	// this function will get the ratio from the tuning starting first from
	// the first (unison) and then the middle degree (presumibly a very
	// consonant interval like 4th or 5th);
	// then will add the adiacent degrees starting from the center

	if (max > FK_MAX_RESONATORS) {
		max = FK_MAX_RESONATORS;
	}
	mb->ratio[0] = 1.0f;
	mb->ratio[1] = (float)ratios[center] * (1.0f + stretch);

	int op = 1;
	int on = -1;
	int dir = 0;
	for (int n = 2; n < max; ++n) {
		// adds from the left and right of the given center
		if (dir) {
			mb->ratio[n] = (float)(ratios[center + op] * (1.0 + stretch * (double)n));
			op++;
			dir = 0;
		} else {
			mb->ratio[n] = (float)(ratios[center + on] * (1.0 + stretch * (double)n));
			on--;
			dir = 1;
		}
	}

	mb->rsize = max;
	mb->scale = FK_MODAL_AMPBASE / (float)max;
}

void dsp_modal_set_coeff(dsp_modal_t * mb, float freq, int mode, float q, double * ratios, int degrees)
{
	int mode_updated = 0;
	int c = degrees / 2;
	if (degrees % 2 == 1) {
		c++;
	}
	if (degrees > 8) {
		degrees = 8;
	}

	// updates ratios of the resonators
	if (mode != mb->mode || mb->tuning_change) {
		mb->mode = mode;
		mb->tuning_change = 0;
		mode_updated = 1;

		switch (mode) {

		case 1: {
			float ra[] = {1.0f};
			_set_ratios(mb, ra, 1, 0.25f);
		} break;
		case 2: {
			float ra[] = {1.0f, 2.0f};
			_set_ratios(mb, ra, 2, 0.3333f);
		} break;
		case 3:
		default: {
			float ra[] = {1.0f, 2.0f, 3.0f, 4.0f};
			_set_ratios(mb, ra, 4, 0.6666f);
		} break;
		case 4: {
			float ra[] = {1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case 5: {
			float ra[] = {1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f};
			_set_ratios(mb, ra, 8, 1.0f);
		} break;
		case 6: { // primes
			float ra[] = {1.0f, 2.0f, 3.0f, 5.0f, 7.0f, 11.0f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case 7: { // 2^x
			float ra[] = {1.0f, 2.0f, 4.0f, 8.0f, 16.0f};
			_set_ratios(mb, ra, 5, 1.0f);
		} break;
		case 8: { // 3^x
			float ra[] = {1.0f, 3.0f, 9.0f, 27.0f};
			_set_ratios(mb, ra, 4, 1.0f);
		} break;
		case 9: { // odd
			float ra[] = {1.0f, 3.0f, 5.0f, 7.0f, 9.0f, 11.0f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case orca_a: { // even
			float ra[] = {1.0f, 2.0f, 4.0f, 6.0f, 8.0f, 10.0f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;

		case orca_b: //	pseudo xylophone
		{
			float ra[] = {1.f, 4.0f, 9.f, 16.f, 24.f, 31.f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case orca_c: //	pseudo vibraphone 1
		{
			float ra[] = {1.f, 4.0f, 11.0f, 18.0f, 23.f, 34.0f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case orca_d: //	pseudo vibraphone 2
		{
			float ra[] = {1.f, 4.0f, 9.5f, 16.f, 21.0f, 29.0f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case orca_e: //	pseudo chalandi plates
		{
			float ra[] = {1.f, 1.5f, 6.0f, 7.5f, 14.0f};
			_set_ratios(mb, ra, 5, 1.0f);
		} break;
		case orca_f: //	pseudo wine glass
		{
			float ra[] = {1.f, 2.5f, 4.0f, 6.5f, 9.0f};
			_set_ratios(mb, ra, 5, 1.0f);
		} break;
		case orca_g: //	pseudo - uniform wooden bar
		{
			float ra[] = {1.f, 2.5f, 5.0f, 7.0f, 10.0f, 12.f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;

		case orca_h: //	red cedar wood plate
		{
			float ra[] = {1.f, 1.47f, 2.09f, 2.56f};
			_set_ratios(mb, ra, 4, 1.0f);
		} break;
		case orca_i: //	douglas fir wood plate
		{
			float ra[] = {1.f, 1.42f, 2.11f, 2.47f};
			_set_ratios(mb, ra, 4, 1.0f);
		} break;
		case orca_j: //	dahina tabla
		{
			float ra[] = {1.0f, 2.89f, 4.95f, 6.99f, 8.01f, 9.02f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;
		case orca_k: //	bayan tabla
		{
			float ra[] = {1.0f, 2.0f, 3.01f, 4.01f, 4.69f, 5.63f};
			_set_ratios(mb, ra, 6, 1.0f);
		} break;

		case orca_l: //	singing bowl 180mm
		{
			float ra[] = {1.f, 2.77828f, 5.18099f, 8.16289f, 11.66063f, 15.63801f, 19.99f};
			_set_ratios(mb, ra, 7, 1.0f);
		} break;
		case orca_m: //	singing bowl 152mm
		{
			float ra[] = {1.f, 2.66242f, 4.83757f, 7.51592f, 10.64012f, 14.21019f, 18.14027f};
			_set_ratios(mb, ra, 7, 1.0f);
		} break;
		case orca_n: //	singing bowl 140mm
		{
			float ra[] = {1.f, 2.76515f, 5.12121f, 7.80681f, 10.78409f};
			_set_ratios(mb, ra, 5, 1.0f);
		} break;

		case orca_o: // from tuning
			_set_ratios_from_tuning_spaced(mb, ratios, degrees, degrees - 1);
			break;
		case orca_p: // from tuning
			_set_ratios_from_tuning_spaced(mb, ratios, degrees, degrees + 1);
			break;
		case orca_q: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 0.0f, 0);
			break;
		case orca_r: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 0.5f, 1);
			break;
		case orca_s: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 0.5f, 0);
			break;
		case orca_t: // from tuning
			_set_ratios_from_tuning_centered(mb, ratios, c, degrees, 0.5f);
			break;
		case orca_u: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 1.0f, 1);
			break;
		case orca_v: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 1.0f, 0);
			break;
		case orca_w: // from tuning
			_set_ratios_from_tuning_centered(mb, ratios, c, degrees, 1.0f);
			break;
		case orca_x: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 2.0f, 1);
			break;
		case orca_y: // from tuning
			_set_ratios_from_tuning_simple(mb, ratios, degrees, 2.0f, 0);
			break;
		case orca_z: // from tuning
			_set_ratios_from_tuning_centered(mb, ratios, c, degrees, 2.0f);
			break;
		}
	}

	// updates coefficients
	if (mode_updated || freq != mb->freq || q != mb->q) {
		mb->freq = freq;
		mb->q = q;
		float sr = mb->sr;

		float fmax = sr * 0.25f;
		while (freq > fmax) {
			freq *= 0.5f;
		}

		for (int i = 0; i < FK_MAX_RESONATORS; ++i) {
			float w, a, b, d;
			float f = freq * mb->ratio[i];

			while (f > fmax) {
				f *= 0.5f;
			}

			w = f * M_TWO_PI;
			a = sr / w;
			b = a * a;
			d = a * 0.5f;

			mb->b1[i] = 1.0f / (b + d / q);
			mb->a1[i] = (1.0f - 2.0f * b) * mb->b1[i];
			mb->a2[i] = (b - d / q) * mb->b1[i];
			mb->s[i] = d;
		}
	}
}

void dsp_modal_process(dsp_modal_t * mb, float * duplex, unsigned buffersize)
{
	float z0[FK_MAX_RESONATORS];
	float z1[FK_MAX_RESONATORS];
	float b1[FK_MAX_RESONATORS];
	float a1[FK_MAX_RESONATORS];
	float a2[FK_MAX_RESONATORS];
	float s[FK_MAX_RESONATORS];

	float scale = mb->scale;
	float dry = mb->dry;
	int size = mb->rsize;

	for (int i = 0; i < size; ++i) {
		z0[i] = mb->z0[i];
		z1[i] = mb->z1[i];
		b1[i] = mb->b1[i];
		a1[i] = mb->a1[i];
		a2[i] = mb->a2[i];
		s[i] = mb->s[i];
	}

	for (unsigned n = 0; n < buffersize; ++n) {

		float xn = duplex[n];
		float yn = 0.0f;

		for (int i = 0; i < size; ++i) {
			float out = b1[i] * xn - a1[i] * z0[i] - a2[i] * z1[i];

			sanitize_float(&out);

			z1[i] = z0[i];
			z0[i] = out;

			yn += out * s[i];
		}

		yn *= scale;
		duplex[n] = yn + xn * dry;
	}

	for (int i = 0; i < size; ++i) {
		mb->z0[i] = z0[i];
		mb->z1[i] = z1[i];
	}
}

void dsp_modals_set_coeff(int num, dsp_modal_t ** mb, float freq, int mode, float q, double * ratios, int degrees)
{
	dsp_modal_set_coeff(mb[0], freq, mode, q, ratios, degrees);

	// calculate the first, copy the rest
	for (int i = 1; i < num; ++i) {
		for (int n = 0; n < FK_MAX_RESONATORS; ++n) {
			mb[i]->b1[n] = mb[0]->b1[n];
			mb[i]->a1[n] = mb[0]->a1[n];
			mb[i]->a2[n] = mb[0]->a2[n];
			mb[i]->s[n] = mb[0]->s[n];
			mb[i]->ratio[n] = mb[0]->ratio[n];
		}
		mb[i]->rsize = mb[0]->rsize;
		mb[i]->freq = mb[0]->freq;
		mb[i]->mode = mb[0]->mode;
		mb[i]->q = mb[0]->q;
		mb[i]->sr = mb[0]->sr;
		mb[i]->scale = mb[0]->scale;
		mb[i]->dry = mb[0]->dry;
		mb[i]->tuning_change = mb[0]->tuning_change;
	}
}

void dsp_modal_clear(dsp_modal_t * mb)
{
	for (int i = 0; i < FK_MAX_RESONATORS; ++i) {
		mb->z0[i] = 0.0f;
		mb->z1[i] = 0.0f;
	}
}
