#pragma once

typedef struct dsp_lfo_t {
	float phase;
	float size;
	float samplerate;
	float base_inc;
	float sixteenths;
	int wave;
	int single;
	int expo;
	float amount;

	float p1_z1;
	float p2_z1;
	float a0;
	float b1;

	float * sin_table_link;
	float * buffer;
} dsp_lfo_t;

dsp_lfo_t * dsp_lfo_create(unsigned buffersize, double samplerate, float * table, unsigned size);
void dsp_lfo_destroy(dsp_lfo_t * lfo);
void dsp_lfo_clear(dsp_lfo_t * lfo);

void dsp_lfo_set_tempo(dsp_lfo_t * lfo, int tempo);

void dsp_lfo_set_params(dsp_lfo_t * lfo, int wave, int sixteenths, float amount, float slew);

void dsp_lfo_render(dsp_lfo_t * lfo, unsigned buffersize);
