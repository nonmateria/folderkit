#pragma once

typedef struct dsp_saturator_t {
	float p1_z1;
	float p2_z1;
	float a0;
	float b1;
	float amp;
	float trim;
	double samplerate;
} dsp_saturator_t;

dsp_saturator_t * dsp_saturator_create(double samplerate);
void dsp_saturator_destroy(dsp_saturator_t * sat);

void dsp_saturator_set_cutoff(dsp_saturator_t * sat, double frequency);

void dsp_saturator_set_drive(dsp_saturator_t * sat, int input_gain);
void dsp_saturator_set_output_gain(dsp_saturator_t * sat, int output_trim);

void dsp_saturator_process(dsp_saturator_t * sat, float * duplex, unsigned buffersize);

void dsp_saturator_clear(dsp_saturator_t * sat);
