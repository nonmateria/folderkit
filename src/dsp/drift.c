#include "drift.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "buffer.h"
#include "functions.h"
#include "rand.h"

dsp_drift_t * dsp_drift_create(double frequency, double samplerate, unsigned buffersize)
{
	dsp_drift_t * drift = malloc(sizeof(dsp_drift_t));
	if (drift == NULL) {
		printf("memory error during struct dsp_drift_t allocation\n");
		return NULL;
	}

	double fc = frequency * 0.25f / samplerate;
	double b1 = exp(-M_TWO_PI * fc);
	drift->a0 = (float)(1.0 - b1);
	drift->b1 = (float)b1;
	drift->z1 = 0.0f;
	drift->d = dsp_brand();

	drift->inc = (float)(frequency / samplerate);
	drift->phase = 0.0f;

	drift->buffer = dsp_create_buffer(buffersize);
	dsp_set_zero(drift->buffer, buffersize);

	return drift;
}

void dsp_drift_destroy(dsp_drift_t * drift)
{
	if (drift != NULL) {
		dsp_destroy_buffer(drift->buffer);
		free(drift);
	}
}

void dsp_drift_render(dsp_drift_t * drift, unsigned buffersize)
{
	float phase = drift->phase;
	float inc = drift->inc;
	float d = drift->d;
	float z1 = drift->z1;
	float a0 = drift->a0;
	float b1 = drift->b1;

	for (unsigned n = 0; n < buffersize; ++n) {
		phase += inc;
		if (phase >= 1.0f) {
			phase -= 1.0f;
			d = dsp_brand();
		}
		float yn = d * a0 + z1 * b1;
		z1 = yn;
		drift->buffer[n] = yn;
	}

	drift->d = d;
	drift->z1 = z1;
	drift->phase = phase;
}
