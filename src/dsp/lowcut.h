#pragma once

typedef struct dsp_lowcut_t {
	float p1_z1;
	float p2_z1;
	float a0;
	float b1;
} dsp_lowcut_t;

dsp_lowcut_t * dsp_lowcut_create(void);

void dsp_lowcut_destroy(dsp_lowcut_t * lc);

void dsp_lowcut_set_cutoff(dsp_lowcut_t * lc, double frequency, double samplerate);

void dsp_lowcut_process(dsp_lowcut_t * lc, float * duplex, unsigned buffersize);

void dsp_lowcut_clear(dsp_lowcut_t * lc);
