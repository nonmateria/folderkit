#pragma once

typedef struct dsp_slew_t {
	float start;
	float delta;
	float phase;
	float inc;
	float ratemult;
	unsigned reached;
	float time;
} dsp_slew_t;

dsp_slew_t * dsp_slew_create(double samplerate);
void dsp_slew_destroy(dsp_slew_t * sl);

void dsp_slew_reset(dsp_slew_t * sl, float value); // jump to value
void dsp_slew_to(dsp_slew_t * sl, float dest);

void dsp_slew_process(dsp_slew_t * sl, float * duplex, unsigned buffersize);
