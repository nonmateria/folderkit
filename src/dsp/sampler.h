#pragma once

#include "../sample_library.h"

typedef struct dsp_sampler_t {
	float cursor;
	float max;
	float inc;
	int is_fading;
	float fade;
	float fade_amt;
	fk_sample_t * sample;
} dsp_sampler_t;

dsp_sampler_t * dsp_sampler_create(void);
void dsp_sampler_destroy(dsp_sampler_t * sampler);

void dsp_sampler_set_octave(dsp_sampler_t * sampler, float wave_freq, double samplerate);

void dsp_sampler_trigger(dsp_sampler_t * sampler, fk_sample_t * sample, float start, float wave_freq, double fade_on_offset, double samplerate, long input_ci, double input_step);

void dsp_sampler_fade_out(dsp_sampler_t * sampler, double ms, double samplerate);
void dsp_sampler_fade_in(dsp_sampler_t * sampler, double ms, double samplerate);

unsigned dsp_sampler_layer(dsp_sampler_t * sampler, float ** buffers, unsigned start, float * freq_buffer, float * drift_buffer, float drift_amount, unsigned buffersize);
