#pragma once

typedef struct dsp_vafilter_t {
	float z1_1;
	float z1_2;
	float z1_3;
	float z1_4;
	float halft;
	float twoslasht;
	float reso;
} dsp_vafilter_t;

dsp_vafilter_t * dsp_filter_create(double samplerate);

void dsp_filter_destroy(dsp_vafilter_t * vaf);

void dsp_filter_process_lpf4(dsp_vafilter_t * vaf, float * duplex, const float * freq_buffer, unsigned buffersize);
void dsp_filter_process_lpf2(dsp_vafilter_t * vaf, float * duplex, const float * freq_buffer, unsigned buffersize);
void dsp_filter_process_bpf4(dsp_vafilter_t * vaf, float * duplex, const float * freq_buffer, unsigned buffersize);
void dsp_filter_process_bpf2(dsp_vafilter_t * vaf, float * duplex, const float * freq_buffer, unsigned buffersize);
void dsp_filter_process_hpf4(dsp_vafilter_t * vaf, float * duplex, const float * freq_buffer, unsigned buffersize);
void dsp_filter_process_hpf2(dsp_vafilter_t * vaf, float * duplex, const float * freq_buffer, unsigned buffersize);

void dsp_filter_clear(dsp_vafilter_t * vaf);
