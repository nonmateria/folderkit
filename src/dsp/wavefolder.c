
// digital wavefolding tecniques liberally taken from
// "Complex Nonlinearities Episode 6: Wavefolding"
// by Jatin Chowdhury
// https://ccrma.stanford.edu/~jatin/ComplexNonlinearities/Wavefolder.html
// (the parallel saturated path is not used, only the feedback path)

#include "wavefolder.h"

#include "functions.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

dsp_wavefolder_t * dsp_wavefolder_create(void)
{
	dsp_wavefolder_t * wf = malloc(sizeof(dsp_wavefolder_t));
	if (wf == NULL) {
		printf("error allocatind wavefolder_t\n");
		return NULL;
	}

	wf->amp = 1.0f;
	wf->trim = 1.0f;
	wf->mode = 1;
	wf->control = 0.5f;
	wf->feedback = 0.0f;
	wf->z1 = 0.f;

	return wf;
}

void dsp_wavefolder_destroy(dsp_wavefolder_t * wf)
{
	if (wf != NULL) {
		free(wf);
	}
}

void dsp_wavefolder_process(dsp_wavefolder_t * wf, float * duplex, unsigned buffersize)
{
	float amp = wf->amp;
	float trim = wf->trim;
	float ctrl = wf->control;
	float fb = wf->feedback;
	float z1 = wf->z1;

	if (wf->mode == 0) { // sine folding
		for (unsigned n = 0; n < buffersize; ++n) {
			float xn = duplex[n] * amp;
			// TODO use a faster sin() for this calculations
			float yn = sinf(M_TWO_PI * xn * ctrl);
			duplex[n] = yn;
		}
	} else { // triangle folding
		float p = 1.0f / ctrl;
		for (unsigned n = 0; n < buffersize; ++n) {
			float xn = (duplex[n] * amp) + (p * 0.25f);
			float x_slash_p = xn / p;
			float yn = 4.0f * fabsf(x_slash_p - floorf(x_slash_p + 0.5f)) - 1.0f;
			duplex[n] = yn;
		}
	}

	if (fb > 0.0f) { // saturated feedback path
		for (unsigned n = 0; n < buffersize; ++n) {
			duplex[n] += tanhf(z1 * fb);
			z1 = duplex[n];
		}
	} else {
		z1 = 0.0f;
	}

	for (unsigned n = 0; n < buffersize; ++n) {
		duplex[n] *= trim;
	}

	wf->z1 = z1;
}

void dsp_wavefolders_set(int num, dsp_wavefolder_t ** wf, int inputdb, float ctrl, int mode, int outputdb, float fb)
{
	for (int i = 0; i < num; ++i) {
		wf[i]->amp = (float)dB((double)inputdb);
		wf[i]->trim = (float)dB((double)outputdb);
		if (ctrl == 0.0f) {
			ctrl = 0.001f;
		}
		wf[i]->control = ctrl;
		wf[i]->mode = mode;
		wf[i]->feedback = fb;
	}
}
