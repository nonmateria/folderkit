#include "fft_utils.h"

#include "stdio.h"
#include "stdlib.h"

// should include c99 complex numbers for complex multiply
#include <complex.h>
#include <fftw3.h>

struct dsp_fft_conf_t {
	float * data;
	float * re;
	float * im;

	unsigned size;
	float scale;

	fftwf_plan r2c_plan;
	fftwf_plan c2r_plan;
};

dsp_fft_conf_t * dsp_fft_create(unsigned blocksize)
{
	// the fft logical isze is blocksize * 2
	// the fft real and imaginary blocks are half the logical +1, so blocksize +1
	dsp_fft_conf_t * cfg = malloc(sizeof(dsp_fft_conf_t));
	if (cfg == NULL) {
		printf("memory error during fft config creation!\n");
		return NULL;
	}

	cfg->size = blocksize * 2;
	cfg->data = fftwf_malloc(sizeof(float) * blocksize * 2);
	cfg->re = fftwf_malloc(sizeof(float) * (blocksize + 1));
	cfg->im = fftwf_malloc(sizeof(float) * (blocksize + 1));

	fftw_iodim dim;
	dim.n = (int)blocksize * 2;
	dim.is = 1;
	dim.os = 1;

	cfg->r2c_plan = fftwf_plan_guru_split_dft_r2c(1, &dim, 0, 0,
	                                              cfg->data,
	                                              cfg->re,
	                                              cfg->im,
	                                              FFTW_MEASURE);

	cfg->c2r_plan = fftwf_plan_guru_split_dft_c2r(1, &dim, 0, 0,
	                                              cfg->re,
	                                              cfg->im,
	                                              cfg->data,
	                                              FFTW_MEASURE);

	if (cfg->data == NULL || cfg->re == NULL || cfg->im == NULL ||
	    cfg->r2c_plan == NULL || cfg->c2r_plan == NULL) {
		printf("memory error during fft config creation!\n");
		dsp_fft_destroy(cfg);
		return NULL;
	}

	cfg->scale = 1.0f / (float)cfg->size;

	return cfg;
}

void dsp_fft_destroy(dsp_fft_conf_t * cfg)
{
	if (cfg != NULL) {
		if (cfg->data != NULL) {
			fftwf_free(cfg->data);
		}
		if (cfg->re != NULL) {
			fftwf_free(cfg->re);
		}
		if (cfg->im != NULL) {
			fftwf_free(cfg->im);
		}
		if (cfg->r2c_plan != NULL) {
			fftwf_destroy_plan(cfg->r2c_plan);
		}
		if (cfg->c2r_plan != NULL) {
			fftwf_destroy_plan(cfg->c2r_plan);
		}
		free(cfg);
	}
}

void dsp_fft_cleanup(void)
{
	fftwf_cleanup();
}

void dsp_fft(dsp_fft_conf_t * cfg, float * data, float * re, float * im)
{
	// pads with zeroes
	unsigned max = cfg->size / 2;
	unsigned n = 0;
	for (; n < max; ++n) {
		cfg->data[n] = data[n];
	}
	max = cfg->size;
	for (; n < max; ++n) {
		cfg->data[n] = 0.0f;
	}

	fftwf_execute_split_dft_r2c(cfg->r2c_plan, cfg->data, cfg->re, cfg->im);

	max = cfg->size / 2 + 1;
	for (n = 0; n < max; ++n) {
		re[n] = cfg->re[n];
		im[n] = cfg->im[n];
	}
}

void dsp_ifft(dsp_fft_conf_t * cfg, float * re, float * im, float * data)
{
	unsigned max = cfg->size / 2 + 1;
	for (unsigned n = 0; n < max; ++n) {
		cfg->re[n] = re[n];
		cfg->im[n] = im[n];
	}

	fftwf_execute_split_dft_c2r(cfg->c2r_plan, cfg->re, cfg->im, cfg->data);

	max = cfg->size;
	for (unsigned n = 0; n < max; ++n) {
		data[n] = cfg->data[n] * cfg->scale;
	}
}

unsigned dsp_get_fft_complex_size(dsp_fft_conf_t * config)
{
	return (config->size / 2) + 1;
}

unsigned dsp_get_fft_real_size(dsp_fft_conf_t * config)
{
	return config->size;
}

float * dsp_convert_sample_buffer(float * input_sample, unsigned input_len, double input_samplerate, double output_samplerate, unsigned * output_len, unsigned buffersize)
{
	unsigned len = (unsigned)((output_samplerate / input_samplerate) * (double)input_len);
	unsigned numblocks = (len / buffersize);
	len = (numblocks + 1) * buffersize;

	float * output = malloc(sizeof(float) * len);
	if (output == NULL) {
		printf("memory error during impulse response conversion!\n");
		return NULL;
	}

	double inc = input_samplerate / output_samplerate;
	double cursor = 0.0;

	for (unsigned n = 0; n < len; ++n) {
		if (cursor < (double)(input_len - 1)) {
			unsigned pos = (unsigned)cursor;
			float fract = (float)cursor - (float)pos;
			// lerp
			output[n] = input_sample[pos] * (1.0f - fract);
			output[n] += input_sample[pos + 1] * fract;
		} else {
			output[n] = 0.0f;
		}
		cursor += inc;
	}

	*output_len = len;

	return output;
}

void dsp_sum_complex_multiply(float * out_re, float * out_im, const float * a_re, const float * a_im, const float * b_re, const float * b_im, unsigned len)
{
	// TODO is a priority to optimize this with SIMD
	for (unsigned n = 0; n < len; ++n) {
		float re = a_re[n] * b_re[n] - a_im[n] * b_im[n];
		float im = a_re[n] * b_im[n] + a_im[n] * b_re[n];
		out_re[n] += re;
		out_im[n] += im;
	}
}
