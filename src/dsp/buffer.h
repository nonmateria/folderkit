#pragma once

#include <math.h>
#include <stdlib.h>

// warning! memory checks have to be done in another place
static inline float * dsp_create_buffer(unsigned buffersize)
{
	return malloc(sizeof(float) * buffersize);
	// MEMO: in c11 we can get aligned memory this way
	// 16 bytes = 128 bits aligned for SIMD
	// return aligned_alloc(16, sizeof(float) * buffersize);
}

static inline void dsp_destroy_buffer(float * buffer)
{
	if (buffer != NULL) {
		free(buffer);
	}
}

static inline void dsp_set_zero(float * buffer, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		buffer[n] = 0.0f;
	}
}

static inline void dsp_set_value(float * buffer, float value, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		buffer[n] = value;
	}
}

static inline void dsp_a_eq_b(float * a, const float * b, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		a[n] = b[n];
	}
}

static inline void dsp_a_eq_bxs(float * a, const float * b, float s, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		a[n] = b[n] * s;
	}
}

static inline void dsp_a_eq_a_plus_bxs(float * a, const float * b, float s, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		a[n] += b[n] * s;
	}
}

static inline void dsp_a_eq_axb(float * a, const float * b, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		a[n] *= b[n];
	}
}

static inline void dsp_a_eq_a_plus_b(float * a, const float * b, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		a[n] += b[n];
	}
}

static inline void dsp_a_eq_b_ducks_a(float * a, const float * b, unsigned buffersize)
{
	for (unsigned n = 0; n < buffersize; ++n) {
		a[n] *= 1.0f - b[n];
	}
}

static inline void dsp_apply_drift_mod(float * buffer, float * drift_buffer, float drift_amount, unsigned buffersize)
{
	drift_amount /= 12.0f;
	for (unsigned n = 0; n < buffersize; ++n) {
		float xpose = drift_amount * drift_buffer[n];
		float mult = powf(2.0f, xpose);
		buffer[n] *= mult;
	}
}

static inline void dsp_calculate_fx_freq(float * buffer, float fx_oct, float * mod_buffer, float mod_amount, float max_freq, unsigned buffersize)
{
	if (mod_buffer != NULL) {
		for (unsigned n = 0; n < buffersize; ++n) {
			float xpose = mod_amount * mod_buffer[n];
			float freq = fx_oct * buffer[n] * powf(2.0f, xpose);
			if (freq > max_freq) {
				freq = max_freq;
			}
			buffer[n] = freq;
		}
	} else {
		for (unsigned n = 0; n < buffersize; ++n) {
			float freq = fx_oct * buffer[n];
			if (freq > max_freq) {
				freq = max_freq;
			}
			buffer[n] = freq;
		}
	}
}

static inline void dsp_amp_modulate(float * duplex, float * mod_buffer, float amount, unsigned buffersize)
{
	float neg = 1.0f - amount;
	for (unsigned n = 0; n < buffersize; ++n) {
		float xn = duplex[n];
		duplex[n] = xn * amount * mod_buffer[n] + xn * neg;
	}
}
