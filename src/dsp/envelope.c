#include "envelope.h"
#include "functions.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void dsp_set_attack_time(dsp_envelope_t * env, float ms)
{
	float samples = env->samplerate * ms * 0.001f;
	env->attack_coeff = (float)exp(-log((1.0f + env->attack_tco) / env->attack_tco) / samples);
	env->attack_offset = (1.0f + env->attack_tco) * (1.0f - env->attack_coeff);

	env->decay_coeff = (float)exp(-log((1.0f + env->release_tco) / env->release_tco) / samples);
	env->decay_offset = (-env->release_tco) * (1.0f - env->decay_coeff);
}

void dsp_set_release_time(dsp_envelope_t * env, float ms)
{
	float samples = env->samplerate * ms * 0.001f;
	env->release_coeff = (float)exp(-log((1.0f + env->release_tco) / env->release_tco) / samples);
	env->release_offset = (-env->release_tco) * (1.0f - env->release_coeff);
}

void dsp_set_hold_time(dsp_envelope_t * env, float ms)
{
	float samples = env->samplerate * ms * 0.001f;
	env->hold_max = (unsigned)samples;
}

dsp_envelope_t * dsp_envelope_create(unsigned buffersize, unsigned samplerate, float max_atk_ms, float max_hold_ms, float max_rel_ms)
{
	dsp_envelope_t * env = malloc(sizeof(dsp_envelope_t));
	if (env == NULL) {
		printf("error allocating envelope data\n");
		goto error;
	}
	env->buffer = malloc(sizeof(float) * buffersize);
	if (env->buffer == NULL) {
		printf("error allocating envelope buffer\n");
		goto error;
	}

	env->samplerate = (float)samplerate;
	env->stage = STAGE_OFF;

	env->attack_tco = (float)exp(-1.5);    // analog-like attack TCO
	env->release_tco = (float)exp(-11.05); // linear-in-db release TCO

	env->gate = 0;
	env->hold_counter = 0;

	env->max_atk_ms = max_atk_ms;
	env->max_hold_ms = max_hold_ms;
	env->max_rel_ms = max_rel_ms;

	dsp_set_attack_time(env, 0.0f);
	dsp_set_hold_time(env, 50.0f);
	dsp_set_release_time(env, 500.0f);

	env->output = 0.0f;
	env->target = 1.0f;

	return env;
error:
	dsp_envelope_destroy(env);
	return NULL;
}

void dsp_envelope_destroy(dsp_envelope_t * env)
{
	if (env != NULL) {
		if (env->buffer != NULL) {
			free(env->buffer);
		}
		free(env);
	}
}

void dsp_envelope_render(dsp_envelope_t * env, unsigned start, unsigned buffersize)
{
	float out = env->output;
	unsigned stage = env->stage;
	float attack_offset = env->attack_offset;
	float attack_coeff = env->attack_coeff;
	float release_offset = env->release_offset;
	float release_coeff = env->release_coeff;
	float decay_offset = env->decay_offset;
	float decay_coeff = env->decay_coeff;
	unsigned hold_counter = env->hold_counter;
	unsigned hold_max = env->hold_max;
	float target = env->target;

	// TODO : avoid loop if STAGE_OFF from start ?
	for (unsigned n = 0; n < buffersize; ++n) {
		switch (stage) {

		case STAGE_ATTACK:
			out = attack_offset + out * attack_coeff;
			if (out >= target) {
				stage = STAGE_HOLD;
				out = target;
			}
			break;

		case STAGE_DECAY:
			out = decay_offset + out * decay_coeff;
			if (out <= target) {
				stage = STAGE_HOLD;
				out = target;
			}
			break;

		case STAGE_HOLD:
			out = target;
			hold_counter++;
			if (hold_counter > hold_max) {
				stage = STAGE_RELEASE;
			}
			break;

		case STAGE_RELEASE:
			out = release_offset + out * release_coeff;
			if (out <= 0.0f) {
				out = 0.0f;
				stage = STAGE_OFF;
			}
			break;

		case STAGE_OFF:
			out = 0.0f;
			break;
		}

		env->buffer[n + start] = out;
	}

	env->output = out;
	env->stage = stage;
	env->hold_counter = hold_counter;
}

void dsp_env_clear(dsp_envelope_t * env)
{
	env->gate = 0;
	env->hold_counter = 0;
	env->output = 0.0f;
	env->target = 0.0f;
	env->stage = STAGE_OFF;
}

// if you want to port the dsp you can delete this function, is useful only with folderkit
#include "../flags.h"
void dsp_env_trigger(dsp_envelope_t * env, int a, int h, int r, int dyn, int dbMode, double db_min)
{
	float amp = 0.0f;

	if (dyn != 0 && dyn < orca_x) { // xyz as argument will mute
		switch (dbMode) {
		case ENV_USE_MULT:
			if (dyn > 16) {
				dyn = 16;
			}
			amp = ((float)dyn) / 16.0f;
			break;
		case ENV_USE_DB:
			if (dyn > 16) {
				dyn = 16;
			}
			amp = (float)dB((db_min / 15.0) * (double)(16 - dyn));
			break;
		case ENV_INVERT_DB:
			amp = 1.0f - (float)dB(-dyn);
			break;
		}
	}

	float atk_ms = ((float)a) / 16.0f;
	if (atk_ms > 1.0f) {
		atk_ms = 1.0f;
	}
	atk_ms = atk_ms * atk_ms * atk_ms * env->max_atk_ms;

	float hold_ms = ((float)h) / 16.0f;
	if (hold_ms > 1.0f) {
		hold_ms = 1.0f;
	}
	hold_ms = hold_ms * hold_ms * hold_ms * env->max_hold_ms;

	float rel_ms = ((float)r) / 16.0f;
	if (rel_ms > 1.0f) {
		rel_ms = 1.0f;
	}
	rel_ms = rel_ms * rel_ms * rel_ms * env->max_rel_ms;

	if (r >= orca_x) {
		rel_ms = 60000.0f * 60.0f * 24.0f; // one day
	}

	dsp_set_attack_time(env, atk_ms);
	dsp_set_hold_time(env, hold_ms);
	dsp_set_release_time(env, rel_ms);

	env->gate = 0;
	if (amp > env->output) {
		env->stage = STAGE_ATTACK;
	} else if (amp < env->output) {
		env->stage = STAGE_DECAY;
	} else {
		env->stage = STAGE_HOLD;
	}
	env->hold_counter = 0;
	env->target = amp;
}
