#include "decimator.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

dsp_decimator_t * dsp_decimator_create(double samplerate)
{
	dsp_decimator_t * de = malloc(sizeof(dsp_decimator_t));
	if (de == NULL) {
		printf("memory error while creating decimator\n");
		return NULL;
	}
	de->bits = 0;
	de->stored = 0.0f;
	de->phase = 0.0f;
	de->inc = 1.0f;
	de->one_slash_sr = (float)(1.0 / samplerate);
	return de;
}

void dsp_decimator_destroy(dsp_decimator_t * de)
{
	if (de != NULL) {
		free(de);
	}
}

void dsp_decimator_set_bits(dsp_decimator_t * de, int bits)
{
	if (bits > 24) {
		bits = 24;
	}
	de->bits = bits;
}

void dsp_decimator_process(dsp_decimator_t * de, float * duplex, float * freq, unsigned buffersize)
{
	int bits = de->bits;
	float one_slash_sr = de->one_slash_sr;
	float yn = de->stored;
	float phase = de->phase;

	for (unsigned n = 0; n < buffersize; ++n) {
		float inc = freq[n] * one_slash_sr;
		phase += inc;
		if (phase >= 0.0f) {
			phase -= 1.0f;
			yn = duplex[n];
		}
		duplex[n] = yn;
	}

	if (bits) {
		float multiply = powf(2.0f, (float)bits);
		float rescale = 1.0f / multiply;

		for (unsigned n = 0; n < buffersize; ++n) {
			float xc = duplex[n] * multiply;
			int xi = (int)xc;
			duplex[n] = rescale * (float)xi;
		}
	}

	de->phase = phase;
	de->stored = yn;
}
