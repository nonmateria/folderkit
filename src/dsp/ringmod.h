#pragma once

typedef struct dsp_ringmod_t {
	float phase;
	float size;
	float oneslashsr;
	float mix;
	float * wave_link;
} dsp_ringmod_t;

dsp_ringmod_t * dsp_ringmod_create(float * table, unsigned size, double samplerate);

void dsp_ringmod_destroy(dsp_ringmod_t * rm);

void dsp_ringmod_process(dsp_ringmod_t * rm, float * duplex, const float * freq_buffer, unsigned buffersize);
