#pragma once

typedef struct dsp_fft_conf_t dsp_fft_conf_t;

// usually the fftsize is the buffersize * 2
dsp_fft_conf_t * dsp_fft_create(unsigned blocksize);
void dsp_fft_destroy(dsp_fft_conf_t * cfg);

void dsp_fft_cleanup(void);

void dsp_fft(dsp_fft_conf_t * cfg, float * data, float * re, float * im);
void dsp_ifft(dsp_fft_conf_t * cfg, float * re, float * im, float * data);

unsigned dsp_get_fft_complex_size(dsp_fft_conf_t * config);
unsigned dsp_get_fft_real_size(dsp_fft_conf_t * config);

// returns a buffer good to be used as convolver input with the right block size
float * dsp_convert_sample_buffer(float * input_sample, unsigned input_len, double input_samplerate, double output_samplerate, unsigned * output_len, unsigned buffersize);

// sum the comlex multiply of a and b to out
void dsp_sum_complex_multiply(float * out_re, float * out_im, const float * a_re, const float * a_im, const float * b_re, const float * b_im, unsigned len);
