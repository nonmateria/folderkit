#pragma once

#include "../sample_library.h"

#include "fft_utils.h"

#include "lowcut.h"

typedef struct dsp_fft_reverb_t {
	float * buffer;
	unsigned buffersize;
	unsigned silence_counter;

	dsp_fft_conf_t * fft_conf;

	float * now_re;
	float * now_im;
	float ** ir_re;
	float ** ir_im;
	float ** fdl_re;
	float ** fdl_im;
	unsigned num_blocks;
	unsigned block;

	float * fft_output;
	unsigned fft_size;

	dsp_lowcut_t * lowcut;
} dsp_fft_reverb_t;

dsp_fft_reverb_t * dsp_fft_reverb_create(float * sample_buffer, unsigned sample_length, double sample_samplerate, unsigned dsp_buffersize, double dsp_samplerate, double lowcut);

void dsp_fft_reverb_destroy(dsp_fft_reverb_t * rev);

void dsp_fft_reverb_clear_input(dsp_fft_reverb_t * rev);

void dsp_fft_reverb_render(dsp_fft_reverb_t * rev);
