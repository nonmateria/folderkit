
#include "sampler.h"
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

dsp_sampler_t * dsp_sampler_create(void)
{
	dsp_sampler_t * sampler = malloc(sizeof(dsp_sampler_t));
	if (sampler == NULL) {
		printf("memory error during sampler module allocation\n");
		return NULL;
	}
	sampler->sample = NULL;
	sampler->inc = 1.0f;
	sampler->cursor = 0.0f;
	sampler->max = 0.0f;
	sampler->fade = 1.0f;
	sampler->fade_amt = 0.0f;
	return sampler;
}

void dsp_sampler_destroy(dsp_sampler_t * sampler)
{
	if (sampler != NULL) {
		free(sampler);
	}
}

void dsp_sampler_set_octave(dsp_sampler_t * sampler, float wave_freq, double samplerate)
{
	if (sampler->sample) {
		if (sampler->sample->mode == FKIT_MODE_WAVE) {
			sampler->inc = (float)((wave_freq * (float)sampler->sample->length) / samplerate);
		} else {
			sampler->inc = (float)(sampler->sample->samplerate / samplerate);
		}
	}
}

void dsp_sampler_trigger(dsp_sampler_t * sampler, fk_sample_t * sample, float start, float wave_freq, double fade_on_offset, double samplerate, long input_ci, double input_step)
{
	assert(start >= 0.0f && start <= 1.0f);

	sampler->sample = sample;
	if (sampler->sample) {
		float max = (float)sample->length;
		sampler->max = max;

		switch (sample->mode) {
		case FKIT_MODE_WAVE:
			sampler->inc = (float)((wave_freq * (float)sample->length) / samplerate);
			sampler->cursor = (float)fmod(sampler->cursor, max);
			break;

		case FKIT_MODE_INPUT:        // audio input, start is used to go back in time
			sampler->inc = 1.0f; // audio input are always at samplerate
			long cursor_off = (long)(input_step * start * 16.0);
			long cursor_l = input_ci - cursor_off;
			while (cursor_l < 0) {
				cursor_l += (long)sample->length;
			}
			sampler->cursor = (float)cursor_l;
			break;

		default: // sample or loop
			sampler->inc = (float)(sample->samplerate / samplerate);
			sampler->cursor = (float)sample->length * start;
			break;
		}

		if (sample->mode == FKIT_MODE_INPUT ||
		    (sample->mode != FKIT_MODE_WAVE && fade_on_offset > 0.0 && start > 0.0f)) {
			sampler->fade = 0.0f;
			sampler->fade_amt = (float)((1000.0 / fade_on_offset) / samplerate);
		} else {
			sampler->fade = 1.0f;
			sampler->fade_amt = 0.0f;
		}
	}
}

void dsp_sampler_fade_out(dsp_sampler_t * sampler, double ms, double samplerate)
{
	sampler->fade_amt = -(float)((1000.0 / ms) / samplerate);
}

void dsp_sampler_fade_in(dsp_sampler_t * sampler, double ms, double samplerate)
{
	sampler->fade_amt = (float)((1000.0 / ms) / samplerate);
	sampler->fade = 0.0f;
}

unsigned dsp_sampler_layer(dsp_sampler_t * sampler, float ** buffers, unsigned start, float * freq_buffer, float * drift_buffer, float drift_amount, unsigned buffersize)
{
	if (sampler->sample) {
		float cursor = sampler->cursor;
		float inc = sampler->inc;
		float fade_amt = sampler->fade_amt;
		float fade = sampler->fade;
		float max = sampler->max;
		int imax = (int)max;
		int check_imax = (sampler->sample->mode == FKIT_MODE_WAVE);
		int loop = (sampler->sample->mode > 0);
		unsigned channels = sampler->sample->channels;
		unsigned rchan = 0;
		if (channels == 2) {
			rchan = 1;
		}
		float * buf_l = sampler->sample->buffers[0];
		float * buf_r = sampler->sample->buffers[rchan];
		if (sampler->sample->mode == FKIT_MODE_INPUT) {
			drift_amount = 0.0f;
		} else {
			drift_amount /= 12.0f;
		}

		for (unsigned n = 0; n < buffersize; ++n) {
			if (cursor <= max) { // there are two guardpoints, == is safe
				int i = (int)cursor;
				int i1 = i + 1;
				if (check_imax && i1 == imax) { // only for waves
					i1 = 0;
				}
				float fract = cursor - (float)i;

				float frame0 = buf_l[i];
				float frame1 = buf_l[i1];
				float value = frame0 * (1.0f - fract) + frame1 * fract;
				buffers[0][n + start] += value * fade;

				// is this faster than putting an if? TODO check it
				float frame0_r = buf_r[i];
				float frame1_r = buf_r[i1];
				float value_r = frame0_r * (1.0f - fract) + frame1_r * fract;
				buffers[1][n + start] += value_r * fade;

				// apply drift inside sampler
				float xpose = drift_amount * drift_buffer[n];
				float mult = powf(2.0f, xpose);
				xpose = freq_buffer[n] * mult;

				cursor += inc * xpose;

				if (loop && cursor >= max) {
					cursor -= max;
				}

				fade += fade_amt;
				if (fade < 0.0f) {
					fade = 0.0f;
				}
				if (fade > 1.0f) {
					fade = 1.0f;
				}
			} else {
				break;
			}
		}

		sampler->cursor = cursor;
		sampler->fade = fade;
		if (fade == 0.0f) {
			sampler->sample = NULL;
		}
		return channels;
	}

	return 1;
}
