#include "ringmod.h"

#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

dsp_ringmod_t * dsp_ringmod_create(float * table, unsigned size, double samplerate)
{
	dsp_ringmod_t * rm = malloc(sizeof(dsp_ringmod_t));
	if (rm == NULL) {
		printf("memory error during ringmod module allocation\n");
		return NULL;
	}
	rm->phase = 0.0f;
	rm->oneslashsr = (float)(1.0 / samplerate);
	rm->wave_link = table;
	rm->size = (float)size;
	rm->mix = 0.5f;

	return rm;
}

void dsp_ringmod_destroy(dsp_ringmod_t * rm)
{
	if (rm != NULL) {
		free(rm);
	}
}

void dsp_ringmod_process(dsp_ringmod_t * rm, float * duplex, const float * freq_buffer, unsigned buffersize)
{
	float oneslashsr = rm->oneslashsr;
	float phase = rm->phase;
	float size = rm->size;
	float * wave = rm->wave_link;
	float mix = rm->mix;
	float bmix = 1.0f - mix;

	for (unsigned n = 0; n < buffersize; ++n) {
		float tablepos = phase * size;
		int ipart = (int)tablepos;
		float fract = tablepos - (float)ipart;
		float frame0 = wave[ipart];
		float frame1 = wave[ipart + 1];
		float w = frame0 * (1.0f - fract) + frame1 * fract;

		duplex[n] = w * duplex[n] * mix + duplex[n] * bmix;

		float inc = freq_buffer[n] * oneslashsr;
		phase += inc;
		if (phase >= 1.0f) {
			phase -= 1.0f;
		}
	}

	rm->phase = phase;
}
