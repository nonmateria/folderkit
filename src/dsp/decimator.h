#pragma once

typedef struct dsp_decimator_t {
	float stored;
	float phase;
	float inc;
	float one_slash_sr;
	int bits;
} dsp_decimator_t;

dsp_decimator_t * dsp_decimator_create(double samplerate);
void dsp_decimator_destroy(dsp_decimator_t * de);

void dsp_decimator_set_bits(dsp_decimator_t * de, int bits);

void dsp_decimator_process(dsp_decimator_t * de, float * duplex, float * freq, unsigned buffersize);
