#pragma once

typedef struct dsp_envelope_t {
	unsigned stage;

	unsigned hold_counter;
	unsigned hold_max;

	float attack_coeff;
	float attack_tco;
	float attack_offset;
	float decay_coeff;
	float decay_offset;
	float release_coeff;
	float release_tco;
	float release_offset;

	float max_atk_ms;
	float max_hold_ms;
	float max_rel_ms;

	float target; // sustain/hold level
	float output;

	int gate;

	float samplerate;

	float * buffer;
} dsp_envelope_t;

#define STAGE_ATTACK 0
#define STAGE_HOLD 1
#define STAGE_RELEASE 2
#define STAGE_DECAY 3
#define STAGE_OFF 4

#define ENV_USE_MULT 0
#define ENV_USE_DB 1
#define ENV_INVERT_DB 2

dsp_envelope_t * dsp_envelope_create(unsigned buffersize, unsigned samplerate, float max_atk_ms, float max_hold_ms, float max_rel_ms);
void dsp_envelope_destroy(dsp_envelope_t * env);

void dsp_envelope_render(dsp_envelope_t * env, unsigned start, unsigned buffersize);

void dsp_env_clear(dsp_envelope_t * env);
void dsp_env_trigger(dsp_envelope_t * env, int a, int h, int r, int dyn, int dbMode, double db_min);
