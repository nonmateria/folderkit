#pragma once

typedef struct dsp_silence_t {
	long counter;
	long max;
	float threshold;
} dsp_silence_t;

dsp_silence_t * dsp_silence_create(double samplerate, double max_time_ms, double db_threshold);

void dsp_silence_destroy(dsp_silence_t * sil);

int dsp_check_silence(dsp_silence_t * sil, float ** buffers, unsigned channels, unsigned start, unsigned buffersize);

void dsp_silence_retrigger(dsp_silence_t * sil);
