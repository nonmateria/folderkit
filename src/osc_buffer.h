#pragma once

#include "atomic.h"
#include <lo/lo.h>

#define FK_OSC_ARGS 35

typedef struct fk_osc_message_t {
	uint64_t timepoint;
	unsigned address;
	unsigned args;
	int values[FK_OSC_ARGS];
	unsigned frame;
} fk_osc_message_t;

typedef struct fk_osc_buffer_t {
	fk_atomic_int_t index;
	unsigned read;
	unsigned size;
	uint64_t old_time;
	uint64_t new_time;
	fk_osc_message_t * data;
} fk_osc_buffer_t;

fk_osc_buffer_t * fk_osc_buffer_create(unsigned size);

void fk_osc_buffer_destroy(fk_osc_buffer_t * osc_buffer);

int fk_osc_handler(const char * path, const char * types, lo_arg ** argv,
                   int argc, void * data, void * user_data);

void fk_osc_error(int num, const char * m, const char * path);

// -------- timing functions --------

void fk_osc_buffer_lap(fk_osc_buffer_t * osc_buffer);

double fk_osc_buffer_delta(fk_osc_buffer_t * osc_buffer, uint64_t timepoint);

void fk_wait(unsigned ms);
