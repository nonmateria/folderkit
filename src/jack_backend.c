#include "jack_backend.h"

#include <jack/jack.h>
#include <stdio.h>
#include <unistd.h>

#include "flags.h"

static jack_port_t * outports[NUMVOICES + 2];
static jack_port_t * inports[orca_z + 1];
static jack_client_t * client;
static unsigned num_outputs = 0;

void _fk_copy_inputs(jack_default_audio_sample_t ** in, fk_synth_t * syn, jack_nframes_t buffersize)
{
	syn->indata.ci += buffersize;
	if (syn->indata.ci >= syn->indata.max) {
		syn->indata.ci = 0;
	}

	for (unsigned i = 0; i < syn->indata.channels; ++i) {
		long ci = syn->indata.ci;
		if (syn->indata.buffers_ref[i] != NULL) {
			for (unsigned n = 0; n < buffersize; ++n) {
				syn->indata.buffers_ref[i][ci + n] = in[i][n];
			}
		} else {
			fprintf(stderr, "something went very wrong, NULL input buffer\n");
		}
	}
}

int _fk_jack_process(jack_nframes_t nframes, void * arg)
{
	fk_synth_t * synthesizer = (fk_synth_t *)arg;

	jack_default_audio_sample_t * out[NUMVOICES + 2];
	jack_default_audio_sample_t * in[36];

	for (unsigned i = 0; i < num_outputs; ++i) {
		out[i] = (jack_default_audio_sample_t *)jack_port_get_buffer(outports[i], nframes);
	}

	if (synthesizer->indata.channels > 0) {
		for (unsigned i = 0; i < synthesizer->indata.channels; ++i) {
			in[i] = (jack_default_audio_sample_t *)jack_port_get_buffer(inports[i], nframes);
		}

		_fk_copy_inputs(in, synthesizer, nframes);
	}

	fk_synth_process(synthesizer, (unsigned int)nframes);

	for (unsigned i = 0; i < num_outputs; ++i) {
		for (jack_nframes_t n = 0; n < nframes; ++n) {
			out[i][n] = synthesizer->outputs[i][n];
		}
	}

	return 0;
}

static void _fk_jack_shutdown(void * arg)
{
	(void)arg;
	printf("jack closed! quitting folderkit...\n");
	exit(1);
}

int fk_jack_backend_open(fk_synth_t * synthesizer, fk_parameters_t * config, fk_osc_buffer_t * osc_buffer, fk_tuning_t * tuning, fk_sample_folder_t ** library, fk_sample_t * impulses)
{

	const char ** ports;
	const char * client_name = "folderkit";
	const char * server_name = NULL;
	jack_options_t options = JackNullOption;
	jack_status_t status;

	/* open a client connection to the JACK server */

	client = jack_client_open(client_name, options, &status, server_name);
	if (client == NULL) {
		fprintf(stderr, "jack_client_open() failed, "
		                "status = 0x%2.0x\n",
		        status);
		if (status & JackServerFailed) {
			fprintf(stderr, "Unable to connect to JACK server\n");
		}
		return 1;
	}
	if (status & JackServerStarted) {
		fprintf(stderr, "JACK server started\n");
	}
	if (status & JackNameNotUnique) {
		client_name = jack_get_client_name(client);
		fprintf(stderr, "unique name `%s' assigned\n", client_name);
	}

	// ------------- init synth --------------------
	config->buffersize = jack_get_buffer_size(client);
	config->samplerate = jack_get_sample_rate(client);

	fk_inputs_create(library, config);

	int rc = fk_synth_prepare_to_play(synthesizer, config, library, impulses, osc_buffer);
	if (rc != 0) {
		return rc;
	}

	// needs voices to be allocated by jack to activate tuning link
	for (unsigned i = 0; i < NUMVOICES; ++i) {
		synthesizer->voices[i]->tuning_link = tuning;
		synthesizer->voices[i]->library_link = library;
		synthesizer->voices[i]->parameters_link = config;
	}
	synthesizer->tuning_link = tuning;

	/* tell the JACK server to call `process()' whenever
	   there is work to be done.
	*/
	jack_set_process_callback(client, _fk_jack_process, synthesizer);

	/* tell the JACK server to call `jack_shutdown()' if
	   it ever shuts down, either entirely, or if it
	   just decides to stop calling us.
	*/

	jack_on_shutdown(client, _fk_jack_shutdown, 0);

	/* create ports */

	outports[0] = jack_port_register(client, "folderkit_main_L",
	                                 JACK_DEFAULT_AUDIO_TYPE,
	                                 JackPortIsOutput, 0);

	outports[1] = jack_port_register(client, "folderkit_main_R",
	                                 JACK_DEFAULT_AUDIO_TYPE,
	                                 JackPortIsOutput, 0);

	num_outputs = config->audio_outputs;
	char portname[21] = "folderkit_group_xx_L";
	unsigned pi = 2;
	for (unsigned i = 0; i < config->num_groups; ++i) {
		if (config->groups[i]->use_jack_channels) {
			portname[16] = (char)('0' + (i / 10));
			portname[17] = (char)('0' + (i % 10));
			portname[19] = 'L';
			outports[pi] = jack_port_register(client, portname,
			                                  JACK_DEFAULT_AUDIO_TYPE,
			                                  JackPortIsOutput, 0);
			portname[19] = 'R';
			outports[pi + 1] = jack_port_register(client, portname,
			                                      JACK_DEFAULT_AUDIO_TYPE,
			                                      JackPortIsOutput, 0);
			pi += 2;
		}
	}

	if (config->num_inputs) {
		char inportname[19] = "folderkit_input_xx";
		for (unsigned i = 0; i < config->num_inputs; ++i) {
			inportname[16] = (char)('0' + (i / 10));
			inportname[17] = (char)('0' + (i % 10));
			inports[i] = jack_port_register(client, inportname,
			                                JACK_DEFAULT_AUDIO_TYPE,
			                                JackPortIsInput, 0);
			if (inports[i] == 0) {
				fprintf(stderr, "cannot get input port %d\n", i);
				config->num_inputs = i;
				fprintf(stderr, "input ports limited to %d\n", i);
			}
		}
	}

	if (config->reverb_use_jack) {
		outports[pi] = jack_port_register(client, "folderkit_reverb_send_L",
		                                  JACK_DEFAULT_AUDIO_TYPE,
		                                  JackPortIsOutput, 0);
		outports[pi + 1] = jack_port_register(client, "folderkit_reverb_send_R",
		                                      JACK_DEFAULT_AUDIO_TYPE,
		                                      JackPortIsOutput, 0);
	}

	/* Tell the JACK server that we are ready to roll.  Our
	 * process() callback will start running now. */

	if (jack_activate(client)) {
		fprintf(stderr, "cannot activate client");
		return 1;
	}

	/* Connect the ports.  You can't do this before the client is
	 * activated, because we can't make connections to clients
	 * that aren't running.  Note the confusing (but necessary)
	 * orientation of the driver backend ports: playback ports are
	 * "input" to the backend, and capture ports are "output" from
	 * it.
	 */
	if (config->jack_autoconnect) {
		ports = jack_get_ports(client, NULL, NULL,
		                       JackPortIsPhysical | JackPortIsInput);
		if (ports == NULL) {
			fprintf(stderr, "no physical playback ports\n");
			return 1;
		}

		// connects only the main outputs by default
		for (unsigned i = 0; i < 2; ++i) {
			if (jack_connect(client, jack_port_name(outports[i]), ports[i])) {
				fprintf(stderr, "cannot connect output port n.%d\n", i);
			}
		}

		jack_free(ports);
	}

	// connect inputs
	if (config->num_inputs > 0) {
		ports = jack_get_ports(client, NULL, NULL,
		                       JackPortIsPhysical | JackPortIsOutput);

		for (unsigned i = 0; i < config->num_inputs; ++i) {
			if (jack_connect(client, ports[i], jack_port_name(inports[i]))) {
				fprintf(stderr, "cannot connect input port n.%d\n", i);
			}
		}

		jack_free(ports);
	}

	return 0;
}

int fk_jack_backend_close(void)
{
	jack_client_close(client);
	return 0;
}
