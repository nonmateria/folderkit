#include "parameters_init.h"
#include "dsp/buffer.h"
#include "dsp/functions.h"
#include "flags.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int fk__parameters_read_file(fk_parameters_t * params, char * stringbuffer);

int fk_parameters_init(fk_parameters_t * params, const char * path)
{
	// default values --------
	params->env_attack_max_ms = 1250;
	params->env_hold_max_ms = 2500;
	params->env_release_max_ms = 2500;
	params->slew_octave_ms = 500;
	params->default_slew_ms = 1;
	params->lfo_slew_freq = 1000.0f;
	params->delay_max_ms = 3000;
	params->delay_lfo_rate_hz = 0.5f;
	params->reference_freq = 440;
	params->samplers_choke_fadeout = 20.0;
	params->samplers_fade_on_offset = 5.0;
	params->audio_outputs = 2;
	params->num_groups = 0;
	params->rev_send_mult = (float)dB(-30.0f);
	params->rev_low_cut = 20.0;
	params->drift_amount = 0.0f;
	params->sine_size = 1024;
	params->max_wave_length = 1024;
	params->master_amp = 1.0f;
	params->master_clip = (float)dB(-2.0);
	params->fm_op_amp = (float)dB(-24.0);
	params->reverb_use_jack = 0;
	params->env_db_min = -30.0;
	params->volume_db_min = -30.0;
	params->silence_threshold_db = -72.0;
	params->jack_autoconnect = 1;

	params->num_inputs = 0;
	params->input_glyph = orca_z + 1;
	params->input_buffers_max_ms = 5000;

	// -----------------------

	// those should be set by the audio backend init
	params->samplerate = 0;
	params->buffersize = 0;

	// set groups defaults
	for (unsigned i = 0; i < NUMVOICES; ++i) {
		params->voice_group[i] = 0;
		params->groups[i] = NULL;
	}

	// open file and update params
	char stringbuffer[FKIT_MAXLINE];
	// path should already be not slashed
	size_t len = strlen(path);
	memmove(stringbuffer, path, len);
	const char filename[] = "/settings.conf";
	memmove(stringbuffer + len, filename, sizeof(filename)); // also copies ending \0

	if (access(stringbuffer, F_OK) != -1) {
		fk__parameters_read_file(params, stringbuffer);
	} else {
		printf("settings.conf not found, using default parameters\n");
	}

	// -------- init group if no group has been created --------
	if (params->num_groups == 0) {
		params->groups[0] = malloc(sizeof(fk_group_t));
		params->groups[0]->amp_input = 1.0f;
		params->groups[0]->clip_threshold = 0.0f;
		params->groups[0]->amp_output = 1.0f;
		params->groups[0]->rev_send = 1.0f;
		params->groups[0]->highpass = 0.0f;
		params->groups[0]->use_jack_channels = 0;
		params->groups[0]->hpf_l = NULL;
		params->groups[0]->hpf_r = NULL;
		params->num_groups++;
	}

	// ----  allocate sine wave ----
	params->sine_wave = dsp_create_buffer(params->sine_size + 1);

	for (unsigned n = 0; n < params->sine_size; ++n) {
		double pct = (double)n / (double)params->sine_size;
		double theta = pct * M_TWO_PI;
		params->sine_wave[n] = (float)sin(theta);
	}
	params->sine_wave[params->sine_size] = 0.0f; // guard point

	for (unsigned i = 0; i < params->num_groups; ++i) {
		if (params->groups[i]->use_jack_channels) {
			params->audio_outputs += 2;
		}
	}

	if (params->reverb_use_jack) {
		params->audio_outputs += 2;
	}

	if (params->num_inputs > 0 && (params->input_glyph > orca_z || params->input_glyph < 0)) {
		printf("[warning] non valid glyph for audio input, setting input channels to 0\n");
		params->num_inputs = 0;
	}

	return 0;
}

void fk_parameters_release(fk_parameters_t * params)
{
	if (params->sine_wave != NULL) {
		dsp_destroy_buffer(params->sine_wave);
	}
	for (unsigned i = 0; i < NUMVOICES; ++i) {
		if (params->groups[i] != NULL) {
			if (params->groups[i]->highpass != 0.0f) {
				dsp_lowcut_destroy(params->groups[i]->hpf_l);
				dsp_lowcut_destroy(params->groups[i]->hpf_r);
			}
			free(params->groups[i]);
		}
	}
}

void fk__create_group(fk_parameters_t * params, char * valuebuffer)
{
	unsigned i = params->num_groups;
	if (i < NUMVOICES - 1) {
		params->groups[i] = malloc(sizeof(fk_group_t));
		if (params->groups[i] == NULL) {
			printf("memory error allocating new voice group\n");
			return;
		}
		params->groups[i]->amp_input = 1.0f;
		params->groups[i]->clip_threshold = 0.0f;
		params->groups[i]->amp_output = 1.0f;
		params->groups[i]->rev_send = 1.0f;
		params->groups[i]->hpf_l = NULL;
		params->groups[i]->hpf_r = NULL;
		for (unsigned k = 0; k < FKIT_MAXLINE; ++k) {
			if (valuebuffer[k] == '\0') {
				break;
			}
			if (valuebuffer[k] >= '0' && valuebuffer[k] <= '9') {
				int v = valuebuffer[k] - '0';
				params->voice_group[v] = i;
			} else if (valuebuffer[k] >= 'a' &&
			           valuebuffer[k] <= ('a' + NUMVOICES - 11)) {
				int v = valuebuffer[k] - 'a' + 10;
				params->voice_group[v] = i;
			}
		}
		params->num_groups++;
	} else {
		printf("WARNING: trying to create more groups than the available voices,"
		       "aborting group creation...\n");
	}
}

static int fk__get_next_line(char * buffer, FILE * file)
{
	for (int i = 0; i < FKIT_MAXLINE; ++i) {
		char c[1];
		fread(c, sizeof(char), 1, file);

		if (ferror(file) != 0) {
			printf("error reading config file lines\n");
			return 2;
		}

		if (!feof(file)) {
			buffer[i] = c[0];

			if (c[0] == '\n') {
				buffer[i] = '\0';
				++i;
				return i;
			}
		} else {
			if (i > 0) {
				buffer[i] = '\0';
				return i + 1;
			} else {
				return EOF;
			}
		}
	}

	printf("line exceeded max buffer space\n");
	return 1;
}

static int fk__match_parameter(const char * line, char * valuebuffer, const char * name)
{
	unsigned mode = 0;
	unsigned i = 0;
	unsigned k = 0;

	while (i < FKIT_MAXLINE) {
		switch (mode) {
		case 0:
			if (line[i] == '\0') {
				return 0; // ended line, name not matching
			}
			if (name[i] == '\0') { // all the name char matched
				mode = 1;
			}
			if (line[i] != name[i]) {
				if (mode == 0) {
					return 0; // name not matched
				} else {
					goto search_equal;
				}
			}
			break;
		case 1:
		search_equal: // check to find equal sign
			if (line[i] == '=') {
				mode = 2;
			} else if (line[i] != ' ') { // allow spaces between name and values
				return 0;
			}
			break;
		case 2: // copy value to return buffer
			if (k >= FKIT_MAXVALUECHARS) {
				return 0;
			}
			valuebuffer[k] = line[i];
			k++;

			if (line[i] == '\0') { // ended line, value copied
				return 1;
			}
			break;
		}
		i++;
	}
	printf("error sentinel in .conf loading\n"); // should never be reached
	return 0;
}

int fk__parse_glyph(char * buffer)
{
	size_t len = strlen(buffer);

	size_t i = 0;
	while (buffer[i] == ' ' && i < len) {
		i++;
	}

	if (i + 1 == len) { // we have just one letter
		int c = buffer[i];

		if (c >= '0' && c <= '9') {
			return c - (int)'0';
		}

		if (c >= 'a' && c <= 'z') {
			return c - (int)'a' + 10;
		}
	}

	return orca_z + 1;
}

static int fk__parameters_read_file(fk_parameters_t * params, char * stringbuffer)
{
	printf("loading settings.conf\n");

	FILE * configfile = fopen(stringbuffer, "r");
	if (configfile == NULL) {
		printf("failed to open setting.conf\n");
		goto error;
	}

	char valuebuffer[FKIT_MAXVALUECHARS];

	// string buffer is now free to use

	while (fk__get_next_line(stringbuffer, configfile) != EOF) {

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "env_attack_max_ms")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->env_attack_max_ms = (float)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "env_hold_max_ms")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->env_hold_max_ms = (float)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "env_release_max_ms")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->env_release_max_ms = (float)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "env_db_min")) {
			double value = (double)atof(valuebuffer);
			if (value < 0.0) {
				params->env_db_min = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "volume_db_min")) {
			double value = (double)atof(valuebuffer);
			if (value < 0.0) {
				params->volume_db_min = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "silence_threshold_db")) {
			double value = (double)atof(valuebuffer);
			if (value < 0.0) {
				params->silence_threshold_db = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "slew_octave_ms")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->slew_octave_ms = (float)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "default_slew_ms")) {
			float value = (float)atof(valuebuffer);
			if (value > 0) {
				params->default_slew_ms = (float)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "lfo_slew_freq")) {
			float value = (float)atof(valuebuffer);
			if (value > 0) {
				params->lfo_slew_freq = (float)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "delay_time_max_ms")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->delay_max_ms = (unsigned)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "delay_lfo_rate_hz")) {
			float value = (float)atof(valuebuffer);
			if (value > 0) {
				params->delay_lfo_rate_hz = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "max_wave_length")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->max_wave_length = (unsigned)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "reference_freq")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->reference_freq = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "fade_on_offset_ms")) {
			double value = (double)atof(valuebuffer);
			if (value >= 0.0) {
				params->samplers_fade_on_offset = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "choke_fadeout_ms")) {
			double value = (double)atof(valuebuffer);
			if (value >= 0.0) {
				params->samplers_choke_fadeout = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "reverb_low_cut")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->rev_low_cut = (double)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "drift_amount")) {
			double value = atof(valuebuffer);
			params->drift_amount = (float)value;
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "sine_resolution")) {
			int value = atoi(valuebuffer);
			if (value > 0) {
				params->sine_size = (unsigned)value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "fm_operators_gain")) {
			double value = atof(valuebuffer);
			params->fm_op_amp = (float)dB(value);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "master_gain")) {
			double value = atof(valuebuffer);
			params->master_amp = (float)dB(value);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "master_clipping")) {
			double value = atof(valuebuffer);
			if (value == 0.0) {
				params->master_clip = 0.0f;
			} else {
				params->master_clip = (float)dB(value);
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "reverb_jack")) {
			int value = atoi(valuebuffer);
			params->reverb_use_jack = (value != 0);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "jack_autoconnect")) {
			int value = atoi(valuebuffer);
			params->jack_autoconnect = (value != 0);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "audio_inputs")) {
			unsigned value = (unsigned)atoi(valuebuffer);
			if (value < orca_z + 1) {
				params->num_inputs = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "input_buffers_ms")) {
			double value = (double)atof(valuebuffer);
			if (value >= 0.0) {
				params->input_buffers_max_ms = value;
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_voices")) {
			fk__create_group(params, valuebuffer);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "input_glyph")) {
			params->input_glyph = fk__parse_glyph(valuebuffer);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_gain")) {
			int g = (int)params->num_groups;
			g--; // last index
			if (g < 0)
				g = 0;
			float value = (float)dB(atof(valuebuffer));
			params->groups[g]->amp_input = value;
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_makeup")) {
			int g = (int)params->num_groups;
			g--; // last index
			if (g < 0)
				g = 0;
			float value = (float)dB(atof(valuebuffer));
			params->groups[g]->amp_output = value;
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_clipping")) {
			int g = (int)params->num_groups;
			g--; // last index
			if (g < 0)
				g = 0;
			double value = atof(valuebuffer);
			if (value == 0.0) {
				params->groups[g]->clip_threshold = 0.0f;
			} else {
				params->groups[g]->clip_threshold = (float)dB(value);
			}
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_send")) {
			int g = (int)params->num_groups;
			g--; // last index
			if (g < 0)
				g = 0;
			double value = atof(valuebuffer);
			params->groups[g]->rev_send = (float)dB(value);
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_jack")) {
			int g = (int)params->num_groups;
			g--; // last index
			if (g < 0)
				g = 0;
			int value = atoi(valuebuffer);
			params->groups[g]->use_jack_channels = value;
		}

		if (fk__match_parameter(stringbuffer, valuebuffer,
		                        "group_highpass")) {
			int g = (int)params->num_groups;
			g--; // last index
			if (g < 0)
				g = 0;
			double value = atof(valuebuffer);
			if (params->groups[g]->highpass == 0.0f && value != 0.0) {
				params->groups[g]->hpf_l = dsp_lowcut_create();
				params->groups[g]->hpf_r = dsp_lowcut_create();
			}
			params->groups[g]->highpass = (float)value;
		}
	}
	fclose(configfile);
	return 0;

error:
	fclose(configfile);
	return 1;
}
