#pragma once

#include "flags.h"

typedef struct fk_tuning_t {
	double reference;
	int degrees;
	double oct[ORCA_GLYPHS_SIZE];
	double xpose[ORCA_GLYPHS_SIZE];
} fk_tuning_t;

void fk_tuning_init(fk_tuning_t * tuning, double reference);
void fk_tuning_split(fk_tuning_t * tuning, int split, int base);

void fk_tuning_mt_default(fk_tuning_t * tuning, int split, int base, int division);
void fk_tuning_mt_indices(fk_tuning_t * tuning, int argc, int * argv);

void fk_tuning_pure_default(fk_tuning_t * tuning, int split, int base, int division);
void fk_tuning_pure_indices(fk_tuning_t * tuning, int argc, int * argv);

void fk_tuning_pure_couples(fk_tuning_t * tuning, int argc, int * argv);

void fk_set_mt_tuning(fk_tuning_t * tuning, unsigned argc, int * argv);
void fk_set_pure_tuning(fk_tuning_t * tuning, unsigned argc, int * argv);
