#pragma once

#include "parameters_init.h"

#define FKIT_MAXFOLDERS 36
#define FKIT_MAXFOLDERS_SQUARED 36 * 36
#define FKIT_NUM_IR 2

#define FKIT_MODE_ONESHOT 0
#define FKIT_MODE_LOOP 1
#define FKIT_MODE_WAVE 2
#define FKIT_MODE_INPUT 3

typedef struct fk_sample_t {
	float * buffers[2];
	unsigned length;
	unsigned channels;
	int mode;
	double samplerate;
} fk_sample_t;

typedef struct fk_sample_folder_t {
	fk_sample_t * samples;
	unsigned size;
	unsigned index;
} fk_sample_folder_t;

fk_sample_folder_t ** fk_library_create(const char * path, unsigned max_wave_length, int input_glyph);
void fk_library_destroy(fk_sample_folder_t ** library);

void fk_report_loaded_samples(void);

fk_sample_t * fk_library_search(fk_sample_folder_t ** library, unsigned folder, unsigned subfolder);

fk_sample_t * fk_ir_create(const char * path, fk_parameters_t * config);

void fk_ir_destroy(fk_sample_t * impulses);

void fk_inputs_create(fk_sample_folder_t ** library, fk_parameters_t * config);
float * fk_get_input_sample(fk_sample_folder_t ** library, fk_parameters_t * config, unsigned i);
