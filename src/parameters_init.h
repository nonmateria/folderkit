#pragma once
#include "dsp/lowcut.h"
#include "flags.h"

typedef struct fk_group_t {
	float rev_send;
	float amp_input;
	float clip_threshold;
	float amp_output;
	float highpass;
	int use_jack_channels;
	dsp_lowcut_t * hpf_l;
	dsp_lowcut_t * hpf_r;
} fk_group_t;

typedef struct fk_parameters_t {
	float env_attack_max_ms;
	float env_hold_max_ms;
	float env_release_max_ms;
	float slew_octave_ms;
	float default_slew_ms;
	float lfo_slew_freq;
	float drift_amount;
	float delay_lfo_rate_hz;
	int reference_freq;
	double samplers_fade_on_offset;
	double samplers_choke_fadeout;
	double rev_low_cut;
	double env_db_min;
	double volume_db_min;
	double silence_threshold_db;
	unsigned delay_max_ms;
	unsigned audio_outputs;
	unsigned max_wave_length;
	int reverb_use_jack;
	int jack_autoconnect;

	unsigned num_inputs;
	int input_glyph;
	double input_buffers_max_ms;

	float master_clip;
	float master_amp;
	float fm_op_amp;
	float rev_send_mult;

	unsigned sine_size;
	unsigned num_groups;

	unsigned samplerate;
	unsigned buffersize;

	// groups data
	unsigned voice_group[NUMVOICES];
	fk_group_t * groups[NUMVOICES];

	// wavetable
	float * sine_wave;
} fk_parameters_t;

int fk_parameters_init(fk_parameters_t * params, const char * path);

void fk_parameters_release(fk_parameters_t * params);
