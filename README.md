# folderkit

`folderkit` is an OSC-controlled sampler I use together with [Orca](https://100r.co/site/orca.html). 

It features 32 voices polyphony, playback of both samples and single cycle waves, samples random robin, a good selection of effects and filters, microtuning and an IR-based reverb.

Designed to make the most out of the very narrow range of values orca can produce (0 to 35), it compresses many options into few arguments. An esoteric sampler for an esoteric language.

The current version still needs some optimization and cleaning, but it's pretty usable. 

## User Guide

For basic usage you don't need to read more than the first three chapters of this guide (four if you want to use wavetables). Read the others later for more advanced stuff.

* [chapter 1](user_guide/ch1_setup.md) : Setup and Hello World
* [chapter 2](user_guide/ch2_osc_basics.md) : OSC Basics
* [chapter 3](user_guide/ch3_files.md) : Files
* [chapter 4](user_guide/ch4_effects.md) : Effects
* [chapter 5](user_guide/ch5_settings.md) : `settings.conf`
* [chapter 6](user_guide/ch6_tuning.md) : Microtuning and Timings
* [chapter 7](user_guide/ch7_hints.md) : Hints and Tricks 

## Thanks to 

* Hundred Rabbits and cancel for making the [ORCA](https://github.com/hundredrabbits/orca-c) c version.
* Andre Weissflog for [sokol](https://github.com/floooh/sokol) time header lib
* David Reid for [dr_wav](https://github.com/mackron/dr_libs)
* Mattias Gustavsson for [rnd.h](https://github.com/mattiasgustavsson/libs)
* Randy Gaul for [cute_files](https://github.com/RandyGaul/cute_headers)

## License 

Nicola Pisanti, MIT License 2020-2021
