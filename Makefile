
# this is a lazy generic makefile 
# mostly from : 
# https://spin.atomicobject.com/2016/08/26/makefile-c-projects/

TARGET ?= bin/$(shell basename $(CURDIR))
SRC_DIRS ?= ./src
DESTDIR?=/usr/local/bin

CC ?= gcc

CFLAGS += -std=c11 -pipe -finput-charset=UTF-8  -g -D_POSIX_C_SOURCE=200809L
CFLAGS += -Wall -Wpedantic -Wextra -Wwrite-strings 
CFLAGS += -Wconversion -Wshadow -Wstrict-prototypes
CFLAGS += -Werror=implicit-function-declaration -Werror=implicit-int
CFLAGS += -Werror=incompatible-pointer-types -Werror=int-conversion

# links for system libraries, for example liblo
LDLIBS = -llo -ljack -lm -lfftw3f

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(addsuffix .o,$(basename $(SRCS)))
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CFLAGS += $(INC_FLAGS) -MMD -MP

all: release

release: CFLAGS+=-O3 -flto=auto -DNDEBUG -march=native -ffast-math
release: $(TARGET)

debug: CFLAGS+=-DDEBUG
debug:  $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) -o $@ $(LOADLIBES) $(LDLIBS)

-include $(DEPS)

install: all
	install -d $(DESTDIR)
	install $(TARGET) $(DESTDIR)

clean:
	$(RM) $(TARGET) $(OBJS) $(DEPS)
	
format:
	clang-format -i src/main.c src/synthesizer.h src/synthesizer.c src/jack_backend.h src/jack_backend.c src/dsp/*.c src/dsp/*.h src/osc_buffer.h src/osc_buffer.c src/atomic.h src/voice.h src/voice.c src/flags.h src/sample_library.h src/sample_library.c src/parameters_init.h src/parameters_init.c src/tuning.h src/tuning.c
	