
JACK leaks two blocks at exit:

```console
==12961== 
==12961== HEAP SUMMARY:
==12961==     in use at exit: 96 bytes in 2 blocks
==12961==   total heap usage: 59 allocs, 57 frees, 140,638 bytes allocated
==12961== 
==12961== 48 bytes in 1 blocks are still reachable in loss record 1 of 2
==12961==    at 0x4835DEF: operator new(unsigned long) (vg_replace_malloc.c:334)
==12961==    by 0x4891B89: ??? (in /usr/lib/x86_64-linux-gnu/libjack.so.0.1.0)
==12961==    by 0x400F379: call_init.part.0 (dl-init.c:72)
==12961==    by 0x400F475: call_init (dl-init.c:118)
==12961==    by 0x400F475: _dl_init (dl-init.c:119)
==12961==    by 0x40010C9: ??? (in /lib/x86_64-linux-gnu/ld-2.28.so)
==12961== 
==12961== 48 bytes in 1 blocks are still reachable in loss record 2 of 2
==12961==    at 0x4835DEF: operator new(unsigned long) (vg_replace_malloc.c:334)
==12961==    by 0x4891BA7: ??? (in /usr/lib/x86_64-linux-gnu/libjack.so.0.1.0)
==12961==    by 0x400F379: call_init.part.0 (dl-init.c:72)
==12961==    by 0x400F475: call_init (dl-init.c:118)
==12961==    by 0x400F475: _dl_init (dl-init.c:119)
==12961==    by 0x40010C9: ??? (in /lib/x86_64-linux-gnu/ld-2.28.so)
==12961== 
==12961== LEAK SUMMARY:
==12961==    definitely lost: 0 bytes in 0 blocks
==12961==    indirectly lost: 0 bytes in 0 blocks
==12961==      possibly lost: 0 bytes in 0 blocks
==12961==    still reachable: 96 bytes in 2 blocks
==12961==         suppressed: 0 bytes in 0 blocks
==12961== 
==12961== For counts of detected and suppressed errors, rerun with: -v
==12961== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)

```